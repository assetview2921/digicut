// server.js
process.env.TZ = "America/New_York";
// set up ======================================================================
// get all the tools we need
var express = require('express');
var app = express();
var router = express.Router();
var mainpath = __dirname;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
console.log('env', process.env)
var port = process.env.PORT || 3000;
// Requires multiparty
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty();
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

app.use("/bower", express.static(__dirname + "/bower_components"));
app.use("/js", express.static(__dirname + "/public/js"));
app.use("/uploads", express.static(__dirname + "/uploads"));
app.use("/css", express.static(__dirname + "/public/css"));
app.use("/img", express.static(__dirname + "/public/img"));
app.use("/images", express.static(__dirname + "/public/images"));
app.use("/templates", express.static(__dirname + "/public/views/templates"));
app.get("/test", function(req, res) {res.send("hello 123")});
// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({secret: 'susdcsdchsdvcsdjcakdaguducsdjciyw87hibwjhjhbcyudwgcuy987r982webcjhw'})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

app.use(function (req, res, next) {
    var config = require("./config/config");
    res.locals.title = config.globals.title;
    res.locals.siteurl = config.globals.siteurl;
    res.locals.weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    res.locals.yearmonths = ['January', 'Feburary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    //if(config.globals.siteurl.indexOf(req.headers.host)<0){
    //    res.send({
    //        error:true,
    //        message:'Cros Domain access is not allowed'
    //    })
    //    return;
    //}
    next();

});


// create reusable transporter object using SMTP transport
// routes ======================================================================
//require('./routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
var indexRouter = require("./routes/index");
var userRouter = require('./routes/users');
var barbersRouter = require('./routes/barber');
var profileRouter = require("./routes/profile");
var accountRouter = require("./routes/account");
var dashboardRouter = require("./routes/dashboard");
var uploadRouter = require("./routes/uploads");
var bookRouter = require("./routes/book");
var orderRouter = require("./routes/order");
var appointmentRouter = require("./routes/appointments");
var selectTimeRouter = require("./routes/selecttime");
var paymentRouter = require("./routes/payment");
var autocompleteRouter = require("./routes/autocomplete");
var servicesRouter = require("./routes/services");
var subscriptionRouter = require("./routes/subscriptions");
var reviewsRouter = require("./routes/reviews");
var adminRouter = require("./routes/admin/index");
var veiwRouter = require("./routes/view");
var adminBillingPlansRouter = require("./routes/admin/billingPlans");
var sitemapCtrl = require('./routes/sitemapCtrl');

var CronJob = require('cron').CronJob;
var robots = require('robots.txt');

app.use("/",indexRouter);
app.use('/users', userRouter);
app.use('/profile', profileRouter);
app.use('/account', accountRouter);
app.use('/barber', barbersRouter);
app.use("/dashboard", dashboardRouter);
app.use("/upload", uploadRouter);
app.use("/book", bookRouter);
app.use("/order", orderRouter);
app.use("/appointments", appointmentRouter);
app.use("/select-date", selectTimeRouter);
app.use("/cron", paymentRouter);
app.use("/autocomplete", autocompleteRouter);
app.use("/subscriptions", subscriptionRouter);
app.use("/reviews", reviewsRouter);
app.use("/services", servicesRouter);
app.use("/admin", adminRouter);
app.use("/view", veiwRouter);
app.use("/admin/billingPlans", adminBillingPlansRouter);
app.use(robots(__dirname + '/robots.txt'));

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port+ "--->>>"+new Date());

sitemapCtrl.setSitemap();


new CronJob({
    cronTime:'00 00 9 * * 5',
    //cronTime:'00 00 11 * * 3',
    onTick: function () {
        var config = require("./config/config");
        var request = require('request');
        request(config.globals.siteurl+"cron/payments",function(err,response,body){
            console.log(response.body);
        })
    },
    start: true,
    timezone: 'America/Los_Angeles'
});

new CronJob({
    //cronTime:'00 00 11 * * 2',
    cronTime:'00 00 17 * * 4',
    onTick:function () {
        var config = require("./config/config");
        var request = require('request');
        request(config.globals.siteurl+"cron/admin-mail",function(err,response,body){
            console.log(response.body);
        })
    },
    start: true,
    timezone:'America/Los_Angeles'
});

new CronJob({
    //update barbers paypal email (set new paypalemail to barber document as default)
    //cronTime:'00 00 11 * * 2',
    cronTime:'00 00 17 * * 5',
    onTick:function () {
        var config = require("./config/config");
        var request = require('request');
        request(config.globals.siteurl+"cron/update-emails",function(err,response,body){
            console.log(response.body);
        })
    },
    start: true,
    timezone:'America/Los_Angeles'
});


//new CronJob({
//    cronTime:'00 */1 2-5 * * *',
//    onTick:function () {
//        console.log('===>>>', new Date())
//      var config = require("./config/config");
//      var request = require('request');
//      request(config.globals.siteurl+"autocomplete",function(err,response,body){
//        console.log('cronjob autocomplete');
//      })
//    },
//    start: true,
//    timezone:'America/Los_Angeles'
//});


new CronJob({
    //cronTime:'00 */5 12-23 * * *',
    cronTime:'00 */30 12-23 * * *',
    onTick:function () {
        console.log('===>>>', new Date())
      var config = require("./config/config");
      var request = require('request');
      request(config.globals.siteurl+"autocomplete",function(err,response,body){
       console.log('cronjob autocomplete');
      })
    },
    start: true,
    timezone:'America/Los_Angeles'
});
new CronJob({
    //cronTime:'00 00 11 * * 2',
    cronTime:'00 */30 0-1 * * *',
    onTick:function () {
      console.log('===>>>', new Date())
      var config = require("./config/config");
      var request = require('request');
      request(config.globals.siteurl+"autocomplete",function(err,response,body){
      console.log('cronjob autocomplete');
      })
    },
    start: true,
    timezone:'America/Los_Angeles'
});
