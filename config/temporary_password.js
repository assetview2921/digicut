/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = {
    'isTemporaryPassword': function (req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.user.isTemporaryPassword==true) {
            return next();
        }
        req.flash('error', true);
        req.flash('message', "Please Reset your password");
        // if they aren't redirect them to the home page
        res.redirect('/users/change-password');
    }
    
}

