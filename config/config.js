/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var env =  process.env.NODE_ENV || 'development'
var config = {
    production:{
        globals: {
            //keywords:"<meta name='keywords' content='barber, barbers, barbershop, barbershops, haircut, black barber, black barbers, black barbershop'>",
            siteurl: 'http://filnu.com/',
            title: 'Filnu',
            adminmain: 'mansour@filnu.com, ace@filnu.com',
            //adminmain:'filnu@filnu.com',
            transaction_price: 15,
            currency: '$',
            fee: {
                customer: {
                    setup: 30,
                    monthly: 80
                },
                barber: {
                    setup: 30,
                    monthly: 20
                },
                customer_changed_card:{
                    setup: 0,
                    monthly: 80
                },
                customer_changed_card_2_cuts:{
                  setup: 0,
                  monthly: 40
                },
                barber_changed_card:{
                    setup: 0,
                    monthly: 20
                }
            }
        },
        twillio: {
            //accountID:'AC4acf8253b7ca1d86945c788c0ee7fa37',
            //authToken:'9d2b157574d263acfe29c0d308c16854'
            accountID: 'AC90f51d87de06fd14658e2d9f780d44b6',
            authToken: '9a1cd1ce9d7b316611d07982c8c121e9',
            number: "+13473636600"

        },
        aws:{
            key:'AKIAJTOFDRJZQWI3BCWA',
            secretKey:'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',
            bucket: 'digicut-public',
            folder: ''
        },
        paypal: {
            'mode': 'live',
            'schema': 'https',
            'host': 'api.paypal.com',
            'port': '',
            'openid_connect_schema': 'https',
            'openid_connect_host': 'api.paypal.com',
            'openid_connect_port': '',
            'client_id': "AWVUVSGVGCd332bgIyZ83MXFr7GoPygBbJZmLyDY-yCRCdPNx_bbTX6UOZ2gj1d4RKwzHovyPWYBmoOt",
            'client_secret': "EPrKEHr69Yz5XYpf0rv1b4K8OZn0h8mdlI1NBuSOvdYTkB8sFSiBxHUHLm9frHwiQoio8F6NaVyKiaYs",
            'authorize_url': 'https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/authorize',
            'logout_url': 'https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/endsession',
            //to test with low amounts uncomment below one
            /*billingPlans: {
             customer: "P-4NA0685523878620SIGBZVUI",
             barber: 'P-06R9438297934241MIGCFKSI'
             }*/
            //when testing please comment below
            billingPlans:{
                barber:"P-7EX12329K9880135PS4LDC7Y",
                customer:"P-61R2996739755294WS4LYF3I",
                barber_changed_card: "P-5835117455055174FZBHMYPY",
                customer_changed_card: "P-7DK40603JV2436722ZBHJSPI",
                customer_changed_card_2_cuts:'P-1K800404NB518525SWXI76BQ'
            }
        },
      services:[{service:'CUT', price:25},{service:'LINE-UP', price:15},{service:'SHAVE', price:10}]
    },
    development:{
        globals: {
            siteurl: 'http://digicut-dev-3.elasticbeanstalk.com/',
            title: 'Filnu',
            //adminmain:'mansour.cortes@gmail.com'
            adminmain: 'alina.petrova.dunice@gmail.com',
            transaction_price: 15,
            currency: '$',
            fee: {
                customer: {
                    setup: 30,
                    monthly: 80
                },
                barber: {
                    setup: 30,
                    monthly: 20
                },
                customer_changed_card:{
                    setup: 0,
                    monthly: 80
                },
                customer_changed_card_2_cuts:{
                  setup: 0,
                  monthly: 40
                },
                barber_changed_card:{
                    setup: 0,
                    monthly: 20
                }

            }

        },
        twillio: {
            //accountID:'AC4acf8253b7ca1d86945c788c0ee7fa37',
            //authToken:'9d2b157574d263acfe29c0d308c16854'
            accountID: 'AC4b891236c3f388a24826aef9f7cc64b7',
            authToken: 'c2f3bc29dc2a142930fba36b1b9fcf92',
            number: "+18327424972"

        },
        paypal: {
            'mode': 'sandbox', //sandbox or live
            'client_id': 'AdnBlwJyYkH1VpCtRaUjhoBXlETwqXGyhlhh2ynmSzpe502VcrBIJtTOT4awy4FNR-RFGcptj_U4QtlT',
            'client_secret': 'EF-0ec-9ZMro-AIAvT1fuMZOlwWJHBL78OttKisZoHHJIRwyzOh5S4nVJp1JqNY8ukM_9ApTzpNd16HE',
            billingPlans: {
                customer: "P-92731707YM035590ESILK27I",
                barber: "P-73209022KK1257228SIM3UCI",
                barber_changed_card: "P-2WB2013524267421RJVD2WCQ",
                customer_changed_card: "P-8G8675242S128823JJVD7LTQ",
                customer_changed_card_2_cuts:"P-0KE2415577629020EQ42ABDY"
            }
        },
        aws:{
            key:'AKIAJTOFDRJZQWI3BCWA',
            secretKey:'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',
            bucket: 'digicut-public',
            folder: 'development_'
        },
      services:[{service:'CUT', price:25},{service:'LINE-UP', price:15},{service:'SHAVE', price:10}]
    },
    test:{
        globals: {
            siteurl: 'http://localhost:3000/',
            title: 'Filnu',
            //adminmain:'mansour.cortes@gmail.com'
            adminmain: 'papko.ts@yandex.ru, tatyana.papko@gmail.com',
            transaction_price: 15,
            currency: '$',
            fee: {
                customer: {
                    setup: 30,
                    monthly: 80
                },
                customer_2_cuts: {
                    setup: 30,
                    monthly: 40
                },
                barber: {
                    setup: 30,
                    monthly: 20
                },
                customer_changed_card:{
                    setup: 0,
                    monthly: 80
                },
                customer_changed_card_2_cuts:{
                    setup: 0,
                    monthly: 40
                },
                barber_changed_card:{
                    setup: 0,
                    monthly: 20
                }
            }

        },
        twillio: {
            //accountID:'AC4acf8253b7ca1d86945c788c0ee7fa37',
            //authToken:'9d2b157574d263acfe29c0d308c16854'
            accountID: 'AC4b891236c3f388a24826aef9f7cc64b7',
            authToken: 'c2f3bc29dc2a142930fba36b1b9fcf92',
            number: "+18327424972"

        },
        paypal: {
            'mode': 'sandbox', //sandbox or live
            'client_id': 'AQvgp0Ro0J3eKbYBU-8BM2PZ84t5B6ro94yOLx7xDWMOH3ldkvjHc4Hh67UjW-W60HbpmqEln1dsn5Ja',
            'client_secret': 'EHbIYZSl0E4e5DfeQ7ocSeDpo495qys0Hd3u2fnBq6NmsN144MTqYAwDhb53YC6ornfOaE1L-wt-osgh',
            //digicut dev
            //'client_id': 'AZ6YsN0azw2VBGOJo61tQSnSqFzoHtRG6pZJH6xi9ETRu9MjA3Zjlj2YwsddOSG5Vrl_vMfeCYbGa_0M',
            //'client_secret': 'EKFrNogbYmxUgV9uimrbFy77STxjXEedUUgUe4_zs85DdAB9HrREfIWTp9H498lzbsa8H-xOBifi38iI',
            billingPlans: {
                customer: "P-17V75826B919365192YHWKLQ",
                customer_2_cuts: "P-5KE445836H341141DLWIP6VI",
                barber: "P-0LM1301944604305C2YHYDFY",
                barber_changed_card: "P-0A4962207M760591DI3IDGCA",
                customer_changed_card: "P-38C0961350628740SI3H7R5I",
                customer_changed_card_2_cuts: "P-15159601GP643033WLWJFPUI"
                //digicut dev
                //customer: "P-0RP779454E60465176Q4SDJA",
                //barber: "P-16456613DR566044A6Q4Z3QA"
            }
        },
        aws:{
            key:'AKIAJTOFDRJZQWI3BCWA',
            secretKey:'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',
            bucket: 'digicut-public',
            folder: 'test_'
        },
      services:[{service:'CUT', price:25},{service:'LINE-UP', price:15},{service:'SHAVE', price:10}]
    }
}

module.exports = config[env];
