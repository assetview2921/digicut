// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy,
    fs = require('fs'),
    sitemapCtrl = require('../routes/sitemapCtrl'),
    expressmailer = require('../config/expressmailer'),
// load up the user model
    User = require('../models/user'),
    reg_password = /^[^\s]{5,}$/,
    reg_username = /^[a-zA-Z0-9_]{1,14}$/,
    Availability = require('../models/availability'),
    availability = [
    {
      "sat" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ],
      "fri" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ],
      "thu" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ],
      "wed" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ],
      "tue" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ],
      "mon" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ],
      "sun" : [
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true"
      ]
    }
  ],
    config = require("./config"),
    knox = knox = require("knox").createClient({
        key: config.aws.key
        , secret: config.aws.secretKey
        , bucket: config.aws.bucket
    }),
    PayPal = require('paypal-rest-sdk');

PayPal.configure({
  'mode': config.paypal.mode, //sandbox or live
  'client_id': config.paypal.client_id,
  'client_secret': config.paypal.client_secret
});

var FacebookStrategy = require('passport-facebook').Strategy;

// expose this function to our app using module.exports
module.exports = function (passport) {
    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },


        function (req, username, password, done) {
            // asynchronous
            // User.findOne wont fire unless data is sent back
            process.nextTick(function () {

                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.findOne({$or: [{'username': new RegExp('^'+req.body.username+'$', 'i')}, {'email': req.body.email}, {'phone': req.body.phone}]}, function (err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        req.flash('error', true);
                        if (user.email == req.body.email) {
                            message = user.email + " is already taken";
                        } else if (user.phone == req.body.phone) {
                            message = "User with phone no: " + user.phone + " is already registered";
                        } else {
                            message = username + " is already taken";
                        }
                        done(null, false, req.flash('popupmessage', message));
                        req.flash('popupmessage', '');
                      //req.flash('error', false);
                        return
                    } else {

                      // check password - should contain at least 5 digits
                      if(!reg_password.test(password)){
                        var message = "Password should be at least 5 characters (without spaces)";
                        done(null, false, req.flash('popupmessage', message));
                        req.flash('popupmessage', '');
                        return;
                      }else{


                        // if there is no user with that email
                        // create the user
                        var newUser = new User();
                        var vcode = newUser.generatePassword(6);
                        // set the user's credentials
                        newUser.email = req.body.email;
                        newUser.phone = req.body.phone;
                        newUser.username = req.body.username;
                        newUser.password = newUser.generateHash(password);
                        newUser.first = req.body.first;
                        newUser.last = req.body.last;
                        newUser.type = req.body.type;
                        newUser.active = 0;
                        newUser.emailVerified = 0;
                        newUser.birthday = req.body.birthday;
                        newUser.verification_code = vcode;

                        //check if pic is available
                        if (typeof req.files.profile_pic != "undefined") {
                          //rename pic
                          if (req.files.profile_pic.name) {
                            picname = config.aws.folder + (new Date().getTime()) + req.files.profile_pic.name;
                            newUser.profile_pic = picname;
                            //read and upload to folder
                            var stream = fs.createReadStream(req.files.profile_pic.path);
                            awsResult = knox.putStream(stream, picname,
                              {
                                'Content-Type': req.files.profile_pic.type,
                                //'Cache-Control': 'max-age=604800',
                                'x-amz-acl': 'public-read',
                                'Content-Length': req.files.profile_pic.size
                              },
                              function (err, result) {
                                //console.log('putStream result >>',result);
                              }
                            );
                          }

                        }
                        //check user type and if its barber
                        if (req.body.type == 1) {
                          //add address fields
                          which = 'barber';
                          newUser.barber.working_hours = true;
                          //newUser.aka = req.body.aka;
                          newUser[which].paypalemail = req.body.paypalemail;
                          newUser.services = config.services;
                        } else if (req.body.type == 0) {
                          which = 'customer';
                        }
                        if (req.body.type == 1 || req.body.type == 0) {
                          var request = require('request');
                          request("https://maps.googleapis.com/maps/api/geocode/json?address=" + req.body.address + ", " + req.body.city + ", " + req.body.state + ", " + req.body.zip, function (err, response, body) {
                            responseBody = JSON.parse(body);
                            try {
                              results = responseBody.results;

                              newUser.address.details = req.body.address + ", " + req.body.city + ", " + req.body.state + ", " + req.body.zip;
                              var address_addr = '';
                              for (addr in results[0].address_components) {
                                address = results[0].address_components[addr];
                                //console.log(results[0].address_components[addr],addr);
                                console.log(address.types);
                                if (address.types.indexOf('street_number') >= 0) {
                                  address_addr += (address_addr !== '')?", "+address.short_name:address.short_name;
                                }
                                if (address.types.indexOf('route') >= 0) {
                                  address_addr +=  (address_addr !== '')?", "+address.short_name:address.short_name;
                                }
                                if (address.types.indexOf('postal_code') >= 0) {
                                  newUser.address.zipcode = (address.long_name)
                                }
                                if (address.types.indexOf('locality') >= 0) {
                                  newUser.address.city = (address.long_name)
                                }
                                if (address.types.indexOf('administrative_area_level_1') >= 0) {
                                  newUser.address.state = (address.long_name)
                                }
                                if (address.types.indexOf('country') >= 0) {
                                  newUser.address.country = (address.long_name)
                                }
                              }

                              if (results.length > 0) {
                                newUser.address.address = address_addr;
                                lat = results[0].geometry.location.lat;
                                lng = results[0].geometry.location.lng;
                                newUser.address.geo = [Number(lat), Number(lng)];
                              }
                            } catch (exp) {
                              console.log(exp);
                            }
                            newUser.save(function (err, registredUser) {
                              console.log(err);
                              if (err) {
                                req.flash('error', true);
                                return done(false, req.flash("popupmessage", err.errors.first.message));
                              } else if (registredUser.type == 1) {
                                  sitemapCtrl.addToSitemap(req.body.username);

                                var barberAvailability = new Availability({
                                  user_id: registredUser._id,
                                  availability: availability
                                });
                                barberAvailability.save(function (err, result) {
                                  console.log('barberAvailability', err, result);
                                })
                              }


                              var config = require("./config");
  //send sms notification


                              //send confirmation mail
                              var expressmailer = require('./expressmailer');
                              expressmailer.mailer.mailer.send("../public/emails/new_registration", {
                                to: req.body.email,
                                subject: "User Registration",
                                data: newUser,
                                siteurl: config.globals.siteurl,
                                vcode: vcode,
                                pwd: password,
                              }, function (err) {
                                req.session.vemail = req.body.email;
                                req.flash('show_verfication', true);
                                expressmailer.mailer.mailer.send("../public/emails/admin_new_registration", {
                                  to: config.globals.adminmain,
                                  subject: "New " + which + " Registration",
                                  data: newUser,
                                  siteurl: config.globals.siteurl,
                                  pwd: password,
                                }, function (err) {
                                  console.log(err);


                                })
                                if (err) {
                                  req.flash('error', false);
                                  req.flash('show_verfication', true);
                                  req.flash("popupmessage", "Thanks for registration, Please verify your email address by clicking on link we sent on your email!");
                                  return done(null, newUser);
                                } else {
                                  req.flash('error', false);
                                  req.flash('show_verfication', true);
                                  req.flash("popupmessage", "Thanks for registration, Please verify your email address by clicking on link we sent on your email!");
                                  return done(null, newUser);
                                }

                                console.log(err);
                              })
                            });
                          })

                        } else {
                          usr = new User();

                          newUser.save(function (err) {
                            if (err) {
                              req.flash('error', true);
                              return done( false, req.flash("popupmessage", err.errors.first.message));
                            }
                            req.session.vemail = req.body.email;


                            var config = require("./config");
  //send sms notification

                            //send confirmation mail
                            var expressmailer = require('./expressmailer');
                            expressmailer.mailer.mailer.send("../public/emails/new_registration", {
                              to: req.body.email,
                              subject: "User Registration",
                              data: newUser,
                              siteurl: config.globals.siteurl,
                              pwd: password,
                              vcode: vcode
                            }, function (err) {
                              expressmailer.mailer.mailer.send("../public/emails/admin_new_registration", {
                                to: config.globals.adminmain,
                                subject: "New  Registration",
                                data: newUser,
                                siteurl: config.globals.siteurl,
                                pwd: password,
                              }, function (err) {
                                console.log(err);


                              })
                              req.session.vemail = req.body.email;
                              req.flash('show_verfication', true);


                              if (err) {
                                req.flash('error', false);
                                req.flash('show_verfication', true);
                                req.flash("popupmessage", "Thanks for registration, Please verify your email address by clicking on link we sent on your email!");
                                return done(null, newUser);
                              } else {

                                req.flash('error', false);
                                req.session.vemail = req.body.email;
                                req.flash('show_verfication', true);
                                req.flash('verificationlink', true)
                                req.flash("popupmessage", "Thanks for registration, Please verify your email address by clicking on link we sent on your email!");
                                return done(null, newUser);
                              }

                              console.log(err);
                            })
                          });


                        }

                      }
                    }

                });

            });

        }));

    passport.use('local-login', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, username, password, done) { // callback with email and password from our form
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({$or: [{'username': new RegExp('^'+username+'$', 'i')}, {'email': username}]}).exec(function (err, user) {
                req.session.usernameLogin = username;
                // if there are any errors, return the error before anything else
                if (err) {
                    req.flash('error', true);
                    return done(err);
                }

                // if no user is found, return the popupmessage
                if (!user) {
                    req.flash('error', true);
                    return done(null, false, req.flash('popupmessage', 'Invalid Username/Password')); // req.flash is the way to set flashdata using connect-flash
                }
                // if the user is found but the password is wrong
                if (!user.validPassword(password)) {
                    req.flash('error', true);
                    return done(null, false, req.flash('popupmessage', 'Invalid Username/Password')); // create the loginMessage and save it to session as flashdata
                }
                // if user is inactive
                if (user.emailVerified !== 1) {
                    req.flash('error', true);
                    req.session.vemail = req.body.username;
                    req.flash('show_verfication', true);
                    return done(null, {error:'Your account is disabled, Please activate your account by clicking on link sent in your mail'}); // req.flash is the way to set flashdata using connect-flash
                }

                //if (user.active == 1) {
                    var Subscription = require("../models/subscriptions");
                    Subscription.findOne({user_id: user._id}, function (err, subscription) {
                      if(subscription){
                      //  PayPal.billing_agreement.get(subscription.subscription_id, function(err, data){
                      //    if(data.state === "Active"){
                            req.session.subscription = subscription;
                            return done(null, user);
                        //  }else{
                        //    req.session.subscription = null;
                        //    return done(null, user);
                        //  };
                        //});
                      }else{
                        req.session.subscription = null;
                        return done(null, user);
                      };
                    }).sort({created:-1});

                //} else {
                //    return done(null, user);
                //};
                // all is well, return successful user

            });

        }));
    passport.use('local-deactivate', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, username, password, done) { // callback with email and password from our form
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({$or: [{'username': username}, {'email': username}]}, function (err, user) {

                // if there are any errors, return the error before anything else
                if (err) {
                    req.flash('error', true);
                    return done(err);
                }

                // if no user is found, return the popupmessage
                if (!user) {
                    req.flash('error', true);
                    return done(null, false, req.flash('popupmessage', 'Invalid Username/Password')); // req.flash is the way to set flashdata using connect-flash
                }
                // if the user is found but the password is wrong
                if (!user.validPassword(password)) {
                    req.flash('error', true);
                    return done(null, false, req.flash('popupmessage', 'Invalid Username/Password')); // create the loginMessage and save it to session as flashdata
                }

                var suspend_note = {
                    "note": "Suspending the agreement"
                };
                var config = require("./config");
                var PayPal = require('paypal-rest-sdk');
                PayPal.configure({
                    'mode': config.paypal.mode, //sandbox or live
                    'client_id': config.paypal.client_id,
                    'client_secret': config.paypal.client_secret
                });
              if(req.session.subscription){
                PayPal.billing_agreement.cancel(req.session.subscription.subscription_id, suspend_note, function (err, response) {
                  console.log("PAYPAL - cancel billing agreements",err, response)
                  user.active = 0;
                  user.save(function (usr) {
                    req.session.subscription = {};
                    req.user = usr;

                    expressmailer.mailer.mailer.send("../public/emails/admin_user_deactivated", {
                      to: config.globals.adminmain,
                      subject: "Account deactivated",
                      data:user
                    }, function (err) {
                      console.log(err)
                    });

                    req.flash("error", true);
                    done(null, false, req.flash('popupmessage', 'Account deactivated successfully')); // req.flash is the way to set flashdata using connect-flash
                    req.flash('popupmessage', '');
                    return
                    // all is well, return successful user

                  })

                })
              }else{
                //if(user.type == 1){
                  user.active = 0;
                  user.save(function (usr) {
                    req.session.subscription = {};
                    req.user = usr;

                    expressmailer.mailer.mailer.send("../public/emails/admin_user_deactivated", {
                      to: config.globals.adminmain,
                      subject: "Account deactivated",
                      data:user
                    }, function (err) {
                      console.log(err)
                    });

                    req.flash("error", true);
                    done(null, false, req.flash('popupmessage', 'Account deactivated successfully')); // req.flash is the way to set flashdata using connect-flash
                    req.flash('popupmessage', '');
                    return
                  })
                //}else{
                //  req.flash("error", true);
                //  done(null, false, req.flash('popupmessage', 'Account deactivated successfully')); // req.flash is the way to set flashdata using connect-flash
                //  req.flash('popupmessage', '');
                //  return
                //}

                  // all is well, return successful user



              }



            });

        }));
    //facebook
    passport.use('facebook', new FacebookStrategy({
        clientID: 1396072827377293,
        clientSecret: "b54f459ea79b19a62aa00ea7aa9891e5",
        callbackURL: "http://localhost:3000/facebook/callback"
    }, function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            if (profile) {
                User.findOne({'fbid': profile.id}, function (err, user) {
                    if (user) {
                        return done(null, user);
                    }
                    var localStorage = require('localStorage')
                    localStorage.setItem('user', profile);
                    return done(null, profile);
                });

            } else {
                return done(null, false);
            }
        })

    }))
};
