/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Subscriptions = require("../models/subscriptions"),
    moment = require('moment'),
    config = require("./config"),
    PayPal = require('paypal-rest-sdk');

PayPal.configure({
  'mode': config.paypal.mode, //sandbox or live
  'client_id': config.paypal.client_id,
  'client_secret': config.paypal.client_secret
});

module.exports = {
    'isSubscribed': function (req, res, next) {
         // if user is authenticated in the session, carry on
        //if (req.user.active==1){
            return next();
        //}
        //else{
        //  if(req.user.type == 1){
        //    return next();
        //  }else{
        //    Subscriptions.findOne({user_id:req.user._id}, function(err, subscription){
        //      if(!err && subscription){
        //        req.flash('popupmessage', "Your account deactivated, please activate it.");
        //        // if they aren't redirect them to the home page
        //        res.redirect('/dashboard');
        //        req.flash('popupmessage', '');
        //        return;
        //      }else{
        //        req.flash('error', true);
        //        req.flash('popupmessage', "Please Subscribe to Plan to view that area");
        //        // if they aren't redirect them to the home page
        //        res.redirect('/account/subscribe');
        //        req.flash('error', true);
        //        return;
        //      }
        //    })
        //  }
        //
        //}
    },
    'isBarber': function (req, res, next) {
        // if user is authenticated in the session, carry on
        if (req.user.type==1) {
            return next();
        }
        req.flash('error', true);
        req.flash('popupmessage', "Only barbers are allowed to access that area");
        // if they aren't redirect them to the home page
        res.redirect('/dashboard');
    },
    'isCustomer': function (req, res, next) {

        // if user is authenticated in the session, carry on
        if(req.user.type==0){
            return next();
        }
        req.flash('error', true);
        req.flash('popupmessage', "Only Customers are allowd to access that area");
        res.redirect('/dashboard');
    },
    'checkSubscriptionDate': function(req, res, next){
        if(req.session.subscription){
            if(req.session.subscription.created){
                checkStartDate(req, res, next);
            }else{
                getSubscription(req, res, next);
            }
        }else{
            getSubscription(req, res, next);
        };
    },
    'checkSubscription': function(req, res, next){
      if(req.user.type == 0){
        if(req.user.active == 1){
          //next();
          if(req.session.subscription){
            if(req.session.subscription.created){
              checkStartDate_simple(req, res, next);
            }else{
              getSubscription_simple(req, res, next);
            }
          }else{
            getSubscription_simple(req, res, next);
          };
        }else{
          req.notSubscribed = true;
          next();
        }
      }else{
        next();
      }
    }
}
function getSubscription(req, res, next){
    Subscriptions.findOne({user_id:req.user._id}, function (err, subscription) {
      if(subscription){
        PayPal.billing_agreement.get(subscription.subscription_id, function(err, data){
          if(data.state === "Active"){
            req.session.subscription = subscription;
            checkStartDate(req, res, next);
          }else{
            req.notSubscribed = true;
            next();
          };
        });
      }else{
        req.notSubscribed = true;
        next();
      };
    }).sort({created:-1})
}

function getSubscription_simple(req, res, next){
    Subscriptions.findOne({user_id:req.user._id}, function (err, subscription) {
      if(subscription){
        req.session.subscription = subscription;
        checkStartDate_simple(req, res, next);
      }else{
        req.notSubscribed = true;
        next();
      };
    }).sort({created:-1})
}

function checkStartDate(req, res, next){
    if(!req.session.subscription.start){
        req.start = req.session.subscription.created;
        resetStart(req, res, next);
    }else{
        var current = moment(),

          _count = Math.ceil(current.diff(moment(new Date(req.session.subscription.start)).add(1, 'month'), 'months', true)),
          count = _count?_count:Math.ceil(current.diff(moment(new Date(req.session.subscription.created)).add(1, 'month'), 'months', true))+1,
          end = moment(new Date(req.session.subscription.created)).add(count, 'month');
        if(end.unix() < current.unix()){
            req.start = moment(new Date(end)).format();
            resetStart(req, res, next);
        }else{
            next();
        };
    };

}

function checkStartDate_simple(req, res, next){
    if(!req.session.subscription.start){
        req.start = req.session.subscription.created;
        resetStart_simple(req, res, next);
    }else{
        var current = moment(),
            //end = moment(new Date(req.session.subscription.start)).add(30, 'days');
      //Math.ceil(b.diff(a, 'months', true))
          main_diff = Math.ceil(current.diff(moment(new Date(req.session.subscription.created)), 'months', true)),
          _count = Math.ceil(current.diff(moment(new Date(req.session.subscription.start)).add(1, 'month'), 'months', true)),
          count = (_count>0)?main_diff - _count:main_diff,
          //difference = ((_count>0)&&(main_diff !== _count))?'':'',
      end = moment(new Date(req.session.subscription.created)).add(count, 'month');
console.log('checkStartDate_simple',main_diff, _count,count, end)
      if(end.unix() < current.unix()){
            req.start = moment(new Date(end)).format();
            resetStart_simple(req, res, next);
        }else{
            next();
        };
    };
}

function resetStart(req, res, next){
    console.log(req.start);
    Subscriptions.update({_id:req.session.subscription._id}, {$set:{start:req.start}})
        .exec(function(err, subscription){
            if(!err && subscription){
                Subscriptions.findOne({user_id:req.user._id}, function (err, subscription) {
                    req.session.subscription = subscription;
                    checkStartDate(req, res, next);
                }).sort({created:-1})
            }else{
                resetStart(req, res, next);
            }
        })
}

function resetStart_simple(req, res, next){
    console.log(req.start);
    Subscriptions.update({_id:req.session.subscription._id}, {$set:{start:req.start}})
        .exec(function(err, subscription){
            if(!err && subscription){
                Subscriptions.findOne({user_id:req.user._id}, function (err, subscription) {
                  req.session.subscription = subscription;
                  checkStartDate_simple(req, res, next);
                }).sort({created:-1})
            }else{
                resetStart(req, res, next);
            }
        })
}


