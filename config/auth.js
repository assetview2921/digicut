/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = {
    'isLoggedIn': function (req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated()) {
            return next();


        }
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        req.flash('error', true);
        req.flash('message', "Please Login to continue");
        // if they aren't redirect them to the home page
        res.redirect('/login');
    },'isLoggedInToBook': function (req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated()) {
            return next();


        }
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        req.flash('error', true);
        req.flash('message', "Please Login to continue");
        // if they aren't redirect them to the home page
        req.session.barber = req.params.username;
        res.redirect('/login');
    },
    isAdmin: function(req, res, next){
        if(req.user.admin === true){
            next();
        } else {
            res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
            res.header('Expires', '-1');
            res.header('Pragma', 'no-cache');
            req.flash('error', true);
            req.flash('message', "Please Login to continue");
            // if they aren't redirect them to the home page
            res.redirect('/login');
        }
    },
    isClient: function(req, res, next){
        if(req.user.admin !== true){
            next();
        } else {
            res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
            res.header('Expires', '-1');
            res.header('Pragma', 'no-cache');
            req.flash('error', true);
            req.flash('message', "Only Clients have access to this page");
            // if they aren't redirect them to the home page
            res.redirect('/login');
        }
    }

}

