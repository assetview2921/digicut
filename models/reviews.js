/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var reviewsSchema = mongoose.Schema({
    user_id:{type:"ObjectId",ref:'User'},
    barber_id:{type:"ObjectId",ref:'User'},
    appointment_id:{type:"ObjectId",ref:'User'},
    comment:{type:String},
    rating:{type:Number,default:1},
    created:{type:Date,default:Date.now}
})
module.exports = mongoose.model('Reviews', reviewsSchema);
