/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var subscriptionSchema = mongoose.Schema({
    user_id:{type:"ObjectId",ref:'User'},
    subscription_id:String,
    status:Boolean,
    created:{type:Date,default:Date.now},
    start:{type:Date}
})
module.exports = mongoose.model('Subscriptions', subscriptionSchema);
