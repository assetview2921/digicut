/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var appointmentSchema = mongoose.Schema({
    user_id: {type:"ObjectId",ref:'User'},
    barber_id: {type:"ObjectId",ref:'User'},
    date: Date,
    service:{type:'String'},
    manually:{type:Boolean, default:false},
    client_name:{type:String},
    confirmation_code: String,
    confirmation_status: {type: Boolean, default: true},
    review_status:{type:Boolean,default:false},
    payed_to_barber: {type: Boolean, default: false},
    client_cancel: {type: Boolean, default: false},
    created: {type: Date, default: Date.now},
    status: {type: Boolean, default: false},
    payDate:{type:Date},
    service:{type:String},
    price:{type: Number},
    action:{type:"ObjectId", ref:'Action'}

});
module.exports = mongoose.model('Appointment', appointmentSchema);
