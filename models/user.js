// models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var userMinLenght = [3, 'The value of path `{PATH}` (`{VALUE}`) exceeds the maximum allowed length ({MINLENGTH}).'];
var moment = require('moment'),
    reg_name = /^[a-zA-Z\s]+$/,
    reg_username = /^[a-zA-Z0-9_]{3,}$/,
    reg_email = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
    reg_phone = /[\(]\d{3}[\)]\s\d{3}[\-]\d{4}/;

var userSchema = mongoose.Schema({
    fbid: {type: String},
    admin: {type:Boolean, default:false},
    email: {type: String, required: true, unique: [true,'(`{VALUE}`) is already registered']},
    password: {type: String, required: true},
    phone: {type: String, required: true, default: null,unique:[true,'(`{VALUE}`) is already registered']},
    username: {type: String, required: true, unique:[true,'(`{VALUE}`) is already registered'],min:userMinLenght},
    first: {type: String, required: true},
    last: {type: String, required: true},
    aka:{type:String},
    verification_code:String,
    type: {type: Number, require: true, default: 0},
    cuts: Number,
    active: {type: Number, default: 0},
    emailVerified: {type: Number, default: 0},
    profile_pic: {type: String},
    updated: {type: Date, default: Date.now},
    created: {type: Date, default: Date.now},
    isTemporaryUsername: {type: Boolean, default: false},
    isTemporaryPassword: {type: Boolean, default: false},
    isTemporaryEmail: {type: Boolean, default: false},
    tmp_email: {type: String},
    tmp_username: {type: String},
    birthday:{type:String, required: true},
    address: {
        address: {type: String},
        city: {type: String, required: true},
        state: {type: String, required: true},
        country: {type: String, required: true},
        details: {type: String, required: true},
        zipcode:{type:String, required: true},
        geo: {}
    },
    barber: {
        working_hours:{type:Boolean,default:false},
        paypalemail: {type: String, default: null},
        newpaypalemail: {type: String, default: null}
    },
    card:{
        digits: {type: String},
        first: {type: String},
        last: {type: String}
    },
    services:{}
});
userSchema.pre('save', function (next) {
  if(this.type === 1){
    if(!this.barber.paypalemail.length){
      var err = new Error();
      err.errors = {first:{message:"Paypal email is required"}}
      next(err);
    }else if(!reg_email.test(this.barber.paypalemail)) {
      var err = new Error();
      err.errors = {first:{message:"Please enter a valid Paypal email"}}
      next(err);
    }
    if(this.newpaypalemail){
      if(!reg_email.test(this.barber.newpaypalemail)) {
        var err = new Error();
        err.errors = {first:{message:"Please enter a valid Paypal email"}}
        next(err);
      }
    }
  }
  if(!reg_phone.test(this.phone)){
    var err = new Error();
    err.errors = {first:{message:"Mobile number must be: (xxx) xxx-xxxx"}}
    next(err);
  }else if(!reg_email.test(this.email)){
    var err = new Error();
    err.errors = {first:{message:"Please enter a valid email"}}
    next(err);
  }else if(!reg_name.test(this.first)) {
    var err = new Error();
    err.errors = {first:{message:"First Name can contain only letters"}}
    next(err);
  }else if(!reg_name.test(this.last)) {
    var err = new Error();
    err.errors = {first:{message:"Last Name can contain only letters"}}
    next(err);
  }else if(!reg_username.test(this.username)) {
    var err = new Error();
    err.errors = {first:{message:"Username should be 3-14 characters (letters, digits and underscore)"}}
    next(err);
  }else if(!moment(this.birthday).isValid()){
    var err = new Error();
    err.errors = {first:{message:"Please enter a valid Birthday"}}
    next(err);
  }else{
    console.log('this ', this)
    next();
  }
});
userSchema.index( { "address.geo": "geoHaystack", type : 1 } ,
                       { bucketSize : 1 } );
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};
userSchema.methods.generatePassword = function (len) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
userSchema.methods.sendMail = function (options) {
    var mailConfig = require("../config/mail.js");
    mailConfig.transporter.sendMail(options);
}
userSchema.methods.exists = function (options) {
    console.log(options);

}

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
var User = require('../models/user');
