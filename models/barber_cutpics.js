/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var mongoose = require('mongoose');
var barbercutpicsSchema = mongoose.Schema({
    user_id:{type:"ObjectId",ref:'User'},
    pic:String,
    default:Boolean,
    created:{type:Date,default:Date.now}
})
module.exports = mongoose.model('BarberCutPics', barbercutpicsSchema);
