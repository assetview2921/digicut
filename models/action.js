var mongoose = require('mongoose');
var actionSchema = mongoose.Schema({
  active: {type: Boolean, default: false},
  description:{type: String},
  button:{type:String},
  price:{type: Number},
  client_price:{type: Number},
  type:{type:String}
});

module.exports = mongoose.model('Action', actionSchema);
