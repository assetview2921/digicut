/**
 * Created by user on 02.02.16.
 */
$(document).ready(function () {
  $.validator.addMethod("username", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9_]{3,14}$/.test(value);
  }, 'Username should be 3-14 characters (letters, digits and underscore)');

  $.validator.addMethod("email", function (value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
  }, 'Please enter a valid email');
  $.validator.addMethod("paypalemail", function (value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
  }, 'Please enter a valid Paypal email');

  $.validator.addMethod("password", function (value, element) {
    return this.optional(element) || /^[^\s]{5,}$/.test(value);
  }, 'Password should be at least 5 characters (without spaces)');

  $.validator.addMethod("first", function (value, element) {
    return this.optional(element) || /^[a-zA-Z]+$/.test(value);
  }, 'First Name can contain only letters');

  $.validator.addMethod("last", function (value, element) {
    return this.optional(element) || /^[a-zA-Z]+$/.test(value);
  }, 'Last Name can contain only letters');

  $.validator.addMethod("phone", function (value, element) {
    return this.optional(element) || /[\(]\d{3}[\)]\s\d{3}[\-]\d{4}/.test(value);
  }, 'Phone number must be: (xxx) xxx-xxxx');
});
