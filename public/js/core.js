/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
doSubmittion = false;

var users = {
    unique: function (e) {
        var params = $(e).prop('name') + "=" + $(e).val();
        var name = $(e).prop('name');
        $.ajax("/users/unique", {
            type: 'POST',
            dataType: 'JSON',
            data: params,
            success: function (response) {

                if (response.error == true) {
                    alert(name + " already exists");
                    e.value = "";
                }
            }
        })

    }
}

//document ready functions and event bindings

$(document).ready(function () {
    //toggle availability on click of buttons and change value of input box accordingly
    $("#avail-cal span.time-available,#avail-cal span.time-unavailable").click(function () {

        if ($(this).hasClass('time-unavailable')) {
            $(this).removeClass('time-unavailable').addClass('time-available');
            status = true;
        } else {
            $(this).removeClass('time-available').addClass('time-unavailable');
            status = false;
        }
        $(this).next("input").val(status);
    });
    //go back
    $(".goback-icon-2").click(function () {
        window.location.href = "/dashboard";
    });

    $("#showLeft").click(function () {
        if ($("#cbp-spmenu-s1").css("left") != '0px')
            $("#cbp-spmenu-s1").animate({left: "0"}, 300);
        else
            $("#cbp-spmenu-s1").animate({left: "-65%"}, 300);
    })
    $(".container,.wrapper").click(function () {
        if ($("#cbp-spmenu-s1").css("left") == "0px") {
            $("#cbp-spmenu-s1").css("left", "-65%");
        }
    })

})


function setLocation() {
    navigator.geolocation.getCurrentPosition(function (position) {
        $("#lat").val(position.coords.latitude);
        $("#lng").val(position.coords.longitude);
        $.ajax('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + "," + position.coords.longitude, {
            success: function (response) {
                if (typeof response.status != 'undefined') {
                    if (response.status == 'OK') {
                        addr = response.results[0];
                        $("#nearest_location").val(addr.formatted_address);
                        addrcompo = addr.address_components;
                        var address = '';
                        for (adr in addrcompo) {
                            if (addrcompo[adr].types.indexOf('administrative_area_level_2') >= 0) {
                                $("#city").val(addrcompo[adr].long_name);
                            }
                            if (addrcompo[adr].types.indexOf('street_number') >= 0) {
                               address += (address !== '')?", "+addrcompo[adr].short_name:addrcompo[adr].short_name;
                            }
                            if (addrcompo[adr].types.indexOf('route') >= 0) {
                                address +=  (address !== '')?", "+addrcompo[adr].short_name:addrcompo[adr].short_name;
                            }
                            if (addrcompo[adr].types.indexOf('administrative_area_level_1') >= 0) {
                                $("#state").val(addrcompo[adr].long_name);
                            }
                            if (addrcompo[adr].types.indexOf('country') >= 0) {
                                $("#country").val(addrcompo[adr].long_name);
                            }
                            if (addrcompo[adr].types.indexOf('postal_code') >= 0) {
                                $("#zip").val(addrcompo[adr].long_name);
                            }
                        }
                        $('#address').val(address);
                    }
                }
            }
        });
    })
}
function changeLocation(result) {
    addr = result;
    $("#lat").val(addr.geometry.location.lat);
    $("#lng").val(addr.geometry.location.lng);
    $("#nearest_location").val(result.formatted_address);
    addrcompo = addr.address_components;
    console.log(addrcompo);
    for (adr in addrcompo) {
        if (addrcompo[adr].types.indexOf('administrative_area_level_2') >= 0) {
            $("#city").val(addrcompo[adr].long_name);
        }
        if (addrcompo[adr].types.indexOf('administrative_area_level_1') >= 0) {
            $("#state").val(addrcompo[adr].long_name);
        }
        if (addrcompo[adr].types.indexOf('country') >= 0) {
            $("#country").val(addrcompo[adr].long_name);
        }
    }
}

function getLocation() {
    navigator.geolocation.getCurrentPosition(function (position) {
        $("#lat").val(position.coords.latitude);
        $("#lng").val(position.coords.longitude);
        $.ajax('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + "," + position.coords.longitude, {
            success: function (response) {
                if (typeof response.status != 'undefined') {
                    if (response.status == 'OK') {

                    }
                }
            }
        });
    })
}
var geo = {
    getAddress: function (options) {
        var url;
        var eflag = false;
        if (typeof options.element != 'undefined') {
            address = $(options.element).val();
            console.log(address);
            url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address;
            eflag = true;
        }
        navigator.geolocation.getCurrentPosition(function (position) {
            if (eflag == false) {
                url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + "," + position.coords.longitude;
                $("#lat").val(position.coords.latitude);
                $("#lng").val(position.coords.longitude);
            }
            $.ajax(url, {
                success: function (response) {
                    if (typeof response.status != 'undefined') {
                        if (response.status == 'OK') {
                            if (typeof options.callback !== 'undefined') {
                                for (i = 0; i < options.callback.length; i++) {
                                    if (options.callback[i] == 'doSubmittion') {
                                        continue;
                                    }
                                    window[options.callback[i]](response.results[0]);
                                }
                            }
                        }
                    }
                }
            }).promise().done(function () {
                for (i = 0; i < options.callback.length; i++) {
                    if (options.callback[i] == 'doSubmittion') {
                        window[options.callback[i]]();
                    }

                }

            });
        })
    },
    searchBarbers: function (options) {
        if (typeof options.form == 'undefined' || typeof options.searchurl == 'undefined') {
            return;
        }
        $.ajax(options.searchurl, {
            method: "POST",
            data: $(options.form).serialize(),
            dataType: "JSON",
            success: function () {
                for (i = 0; i < options.callback.length; i++) {
                    window[options.callback[i]]();
                }
            }
        })
    }
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d.toFixed(2);
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

function json2map(result) {
    if (typeof result.count !== 'undefined') {
        if (result.count == 0) {
            //window.location.href = "/users/invitenow/"
        } else {
            //window.location.href = "/users/joinnow/"
        }

    } else {
        if (result.error == null && result.length > 0) {
            coords = new Array();
            for (i in result) {
                lat2 = result[i][0].address.geo[0];
                lng2 = result[i][0].address.geo[1];
                result[i].distance = getDistanceFromLatLonInKm(lat, lng, lat2, lng2);
                coords.push(result[i]);
            }
            initialize(coords);
        }
    }

}

function initialize(coords) {

    console.log(lat, lng);
    var myCenter = new google.maps.LatLng(lat, lng);

    var mapProp = {
        center: myCenter,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var marker = [];
    var infowindow = [];
    var map = new google.maps.Map(document.getElementById("map"), mapProp);
    for (var i = 0; i < coords.length; i++) {
        var content = '';
        for(var j = 0; j < coords[i].length; j++){
            content += coords[i][j].first + ' ' + coords[i][j].last + " a.k.a " + coords[i][j].username +
                "<br/><a href='/barber/" + coords[i][j].username + "'>Book an appoinment</a></br></br>"

        }
        var position = new google.maps.LatLng(coords[i][0].address.geo[0], coords[i][0].address.geo[1]);
        marker[i] = new google.maps.Marker({
            position: position,
            title: 'Click to view'
        });

        infowindow[i] = new google.maps.InfoWindow({
            content: content
        });
        marker[i].setMap(map);
// Zoom to 9 when clicking on marker
        google.maps.event.addListener(marker[i], 'click', function (innerKey) {
            return function () {
                infowindow[innerKey].open(map, marker[innerKey]);
            }
        }(i));




    }
    $("#searchwrapper").hide();
    $("#mapwrapper").show();
}

