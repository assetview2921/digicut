/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
   $("#birthdaypicker").datetimepicker({
            icons: {
                previous: 'fa fa-angle-double-left',
                next: 'fa fa-angle-double-right'
//                            minDate:'12 31 1949'
//                    time: "fa fa-clock-o",
//                            date: "fa fa-calendar",
//                            up: "fa fa-arrow-up",
//                            down: "fa fa-arrow-down"
            },
            viewMode: 'years',
            format: 'MM/DD/YYYY'
        }
    );
    if($("#editprofile").size()<=0)
        setLocation();
    $("#username,#email,#phone").blur(function () {
        if($.trim(this.value)!='')
        users.unique(this);
    })
    $("form").submit(function (e) {
        if ($(this).find(".has-error").size() > 0) {
            $("#global").addClass('alert alert-danger').html("Oops! Something is wrong, Please fix all errors.");
            $(window).scrollTop(0);
            return false;
        }
    })



})
