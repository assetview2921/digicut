var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    async = require('async'),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    mongoose = require('mongoose'),
    ObjectID = mongoose.Types.ObjectId,
    moment = require('moment'),
    formidable = require('formidable'),
    accountSid = config.twillio.accountID,
    authToken = config.twillio.authToken,
    client = require('twilio')(accountSid, authToken);

router.get("/", auth.isLoggedIn, auth.isClient, subscriber.checkSubscription, function (req, res) {
    res.locals.title = 'Appointments';
    if (req.user.type == 0) {
        async.parallel([
            function(callback){
                Appointment.find({"user_id": req.user._id, confirmation_status:{$ne:false}}).populate('barber_id').exec(function (err, appointments) {
                    callback(err, appointments);
                });
            },
            function(callback){
                if(req.notSubscribed || req.user.cuts == 0){
                    callback(null, "");
                }else{
                    getCountAppointmentsPerMonth(req, function(err, count){
                        //var date = moment(new Date(moment(req.session.subscription.start).add(1, 'month')).toISOString()).format('MMM. DD');
                        var date = moment(new Date(moment(new Date(req.session.subscription.created))
                          .add(Math.ceil(moment().diff(moment(new Date(req.session.subscription.created)), 'months', true)), 'month')).toISOString()).format('MMM. DD');
                        callback(err, "You have "+count+" transactions remaining this cycle. Next cycle begins on "+date);
                    });

                };
            }
        ], function(err, result){
            //Appointment.find({"user_id": req.user._id}).populate('barber_id').exec(function (err, appointments) {
                res.locals.title = "History";
                res.render("../public/views/users/appointments", {
                    user: req.user,
                    appointments: result[0],
                    remainingMessage: result[1],
                    popupmessage: req.flash('popupmessage'),
                    error: req.flash('error'),
                    moment: moment
                });
            return;
            //});
        })

    } else if (req.user.type == 1) {
        Appointment.find({"barber_id": req.user._id, confirmation_status:{$ne:false}}).populate('user_id').exec(function (err, appointments) {
            res.locals.title = "Barber History";
            res.render("../public/views/users/appointments", {
                user: req.user,
                appointments: appointments,
                popupmessage: req.flash('popupmessage'),
                error: req.flash('error'),
                moment: moment
            });
        });
    }

})
router.get("/barber/:_id",  function (req, res) {
    res.locals.title = 'Appointments';
  if(req.isAuthenticated()){
    if(req.user.admin === true){
      getAppointments(req, res, false);
    } else {
      getAppointments(req, res, true)
    }
  }else{
    getAppointments(req, res, false)
  }

})
router.get("/:_id/review", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, function (req, res) {
    res.locals.title = 'Reviews';
    if (typeof req.params._id !== 'undefined') {
        Reviews.count({user_id: req.user._id, appointment_id: req.params._id}, function (err, count) {
            console.log("-----test-----");
            console.log(count);
            if (count > 1) {
                req.flash('popupmessage', 'Can not review on same appointment again and again.');
                res.redirect("/appointments");
                return;
            } else {
                Appointment.findOne({_id: req.params._id}, {barber_id: 1}).populate('barber_id').exec(function (err, appointment) {
                    res.render("../public/views/users/review", {
                        _id: req.params._id,
                        user: req.user,
                        error: false,
                        appointment: appointment,
                        popupmessage: req.flash('popupmessage')
                    });
                })
            }
        })
    } else {
        req.flash('popupmessage', 'Invalid Request');
        res.redirect("/appointments");
    }
})
router.post("/:_id/review", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        req.body = fields;
        if (typeof req.params._id !== 'undefined') {
            var rev = new Reviews();
            rev.barber_id = req.body.barber_id;
            rev.user_id = req.user._id;
            rev.appointment_id = req.body.appointment_id;
            rev.comment = req.body.comment;
            rev.rating = req.body.rating;
            rev.save(function (err1, rev) {
                Appointment.findOne({_id: req.params._id}, {_id: true}, function (err, app) {
                    app.review_status = true;
                    app.save(function () {
                        req.flash('popupmessage', 'Thanks for review!');
                        res.send(200, "/appointments")
                    })
                })
            })
        } else {
            req.flash('popupmessage', 'Invalid Request');
            res.send(200, "/appointments")
        }
    })
})
router.get('/complete', auth.isLoggedIn, auth.isClient, function (req, res) {
    res.locals.title = 'Enter Code';
    req.flash('popupmessage', 'Invalid Request');
    res.redirect("/appointments");
});

router.get('/cancel', auth.isLoggedIn, auth.isClient, function (req, res) {
    res.locals.title = 'Cancel Appointments';
    req.flash('popupmessage', 'Invalid Request');
    res.redirect("/appointments");
});
router.get('/create', auth.isLoggedIn, auth.isClient, subscriber.isBarber, function(req, res){
    res.render('../public/views/users/appointments',{
            user: req.user,
            popupmessage: req.flash('popupmessage'),
            error: req.flash('error')
        }
    )
})

router.get('/complete/:id', auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    res.locals.title = 'Enter Code';
    Appointment.findOne({_id: req.params.id, status: false}).populate("user_id").exec(function (err, appointment) {
        if (err || appointment == null) {
            req.flash('popupmessage', 'No appointment found');
            res.redirect("/appointments");
            return;
        }
        res.render("../public/views/users/barbers/appointment_complete", {
            appointment: appointment,
            _id: req.params.id,
            user: req.user,
            error: false,
            popupmessage: req.flash('popupmessage')
        });
    })
});

router.get('/cancel/:id', auth.isLoggedIn, auth.isClient, function (req, res) {
    res.locals.title = 'Cancel This Appointment';
    res.render("../public/views/users/barbers/appointment_cancle", {
        _id: req.params.id,
        user: req.user,
        error: false,
        popupmessage: req.flash('popupmessage')
    });
});

router.post('/cancle', auth.isLoggedIn, auth.isClient, function (req, res) {
    Appointment.findOne({_id: req.body.id})
        .populate('user_id', {'email': true, 'username': true, 'first': true, 'last': true, 'phone':true})
        .populate('barber_id', {'email': true, 'username': true, 'first': true, 'last': true, 'phone':true, address:true})
        .populate('action')
        .exec(function (err, app) {
            if (err || app == null) {
                req.flash('popupmessage', 'Invalid Request');
                res.redirect("/appointments");
                return;
            }
            if(req.user.type == 0 ||(req.user.username === app.user_id.username)){
                clientCancelAppointment(req, res, app);
            } else {
                barberCancelAppointment(req, res, app);
            }
        })
});
router.post("/bydate", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    var val = mongoose.Types.ObjectId(req.user._id);
    Appointment.aggregate([
        {$match: {'barber_id': val, confirmation_status:true}},
        {
            $group: {
                _id: '$date',
                total: {$sum: 1}
            }
        }
    ]).exec(function (e, d) {
        res.send(d);
    });
})
router.post('/complete', auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    Appointment.findOne({
        _id: req.body.id
    }).populate('user_id').exec(function (err, app) {
        if (err || app == null || ( app.confirmation_code!== req.body.code && app.manually !== true )) {
            req.flash('popupmessage', 'Invalid Code\nPlease check and try again.');
            res.redirect("/appointments/complete/" + req.body.id);
            return;
        }
        app.status = true;
        app.completedAt = moment();
        app.save(function (error) {
            if (error) {
                req.flash('popupmessage', 'Unable to complete appointment, please try again.');
                res.redirect("/appointments/complete/" + app._id);
                return;
            }
            //notify to admin
            if(app.manually !== true){
                expressmailer.mailer.mailer.send("../public/emails/admin_appointment", {
                    to: config.globals.adminmain,
                    subject: "Appointment completed",
                    action: 'completed',
                    barber: req.user,
                    siteurl: config.globals.siteurl,
                    customer: app.user_id,
                    client_name: app.client_name,
                    client_cancel: app.client_cancel,
                    date: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A')
                }, function (err) {
                    console.log(err)
                });
                //notify to customer
                expressmailer.mailer.mailer.send("../public/emails/customer_appointment_completed", {
                    to: app.user_id.email,
                    subject: "Appointment completed",
                    data: req.user,
                    manually:app.manually,
                    customer: app.user_id,
                    client_name: app.client_name,
                    app_date:moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
                    service:app.service,
                    date: moment(new Date().toISOString()).format('ddd MMM DD YYYY hh:mm A'),
                    siteurl: config.globals.siteurl
                }, function (err) {
                    req.flash('popupmessage', 'Appointmet has been completed successfully');
                    res.redirect("/appointments/");
                    return;
                });
            }else{
                req.flash('popupmessage', 'Appointmet has been completed successfully');
                res.redirect("/appointments/");
                return;
            }
        })
    })
});
router.get('/:id', auth.isLoggedIn, auth.isClient, function (req, res) {
    if (req.params.id == "undefined") {
        req.flash('popupmessage', 'Invalid Request');
        res.redirect("/appointments");
        return;
    }
    Reviews.count({user_id: req.user._id, appointment_id: req.params.id}, function (err, count) {
        console.log(count);
        if (count > 1) {
            req.flash('popupmessage', 'Can not review on same appointment again and again.');
            res.redirect("/appointments");
            return;
        }
    })
    var query = {
        "date" :1,
        "service" : 1,
        "barber_id" : 1,
        "user_id" : 1,
        "status" : 1,
        "created" : 1,
        "payed_to_barber" : 1,
        "review_status" : 1,
        "confirmation_status" : 1
    }
    if(req.user.type == 0) query.confirmation_code = 1;
    Appointment.findOne({
        _id: req.params.id,
        confirmation_status: true
    }).populate("barber_id").exec(function (err, appointment) {
        console.log(err, appointment);
        if (err || appointment == null) {
            req.flash('popupmessage', 'No appointment found');
            res.redirect("/appointments");
            return;
        }
        var ID = new ObjectID(req.user._id);
        var ID2 = new ObjectID(appointment.user_id);
        if(!(ID.equals(ID2))) {
            appointment.confirmation_code = false;
        }
        res.locals.title = 'Appointment Details';
        res.render("../public/views/users/appointment_details", {
            user: req.user,
            moment:moment,
            appointment: appointment,
            error: false,
            popupmessage: req.flash('popupmessage')
        });
    })
});
router.get("/bydate/:date", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    var day = moment(req.params.date, "MM-DD-YYYY"),
        time =day.toDate().getTime(),
        tomorrow = moment(day).add(1, 'days');
    console.log(day.toDate(), tomorrow.toDate());
  if(req.user.barber.working_hours === false){
    req.flash('error', true);
    req.flash('popupmessage', "You didn't set working hours");
    res.redirect("/barber/calendar");
    return;
  }
    Appointment.find({
        barber_id: req.user._id,
        date: {$gte: day.toDate(), $lt: tomorrow.toDate()}
    }).populate("user_id").exec(function (err, appointments) {
        res.locals.title = 'Calendar';
        if (err || appointments == null || appointments.length == 0) {
            res.render("../public/views/users/barbers/calendar_event", {
                user: req.user,
                appointments: appointments,
                error: false,
                moment:moment,
                time:time,
                barber:req.user.username,
                popupmessage: req.flash('popupmessage')
            });
            return;
        }
        res.render("../public/views/users/barbers/calendar_event", {
            user: req.user,
            appointments: appointments,
            error: false,
            moment:moment,
            time:time,
            barber:req.user.username,
            popupmessage: req.flash('popupmessage')
        });
    })
})
function barberCancelAppointment(req, res, app){
    app.confirmation_status = false;
    app.completedAt = moment();
    app.save(function (error) {
        if (error) {
            req.flash('popupmessage', 'Unable to cancle appointment, please try again.');
            res.redirect("/appointments/cancle/" + app._id);
            return;
        }
        app.date = moment(new Date(app.date)).format('ddd MMM DD YYYY HH:mm ');
      var title = (app.price && (app.price !== 'undefined'))?"Appointment 'pay as you go' canceled":"Appointment canceled";
        //notify admin
        expressmailer.mailer.mailer.send("../public/emails/admin_appointment", {
            to: config.globals.adminmain,
            subject: title,
            action: 'canceled',
            barber: app.barber_id,
            price: app.price,
            campaign:app.action,
            siteurl: config.globals.siteurl,
            customer: app.user_id,
            date: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
            client_cancel: app.client_cancel
        }, function (err) {
            console.log(err)
        });
        //notify to client on sms
        temp1 = app.user_id.phone.split(/[()\s\t-]/);
        app.user_id.phone = temp1.join("");
        client.messages.create({
            body: "Sorry "+app.user_id.username+" but your barber had to cancel your appointment. Please book a new appointment for a different time or a different barber. Sorry for the inconvenience.\n\nFILNU",
            to: "+1" + (app.user_id.phone),
            from: config.twillio.number
        }, function (err, message) {
            console.log(err, message);
        });

        expressmailer.mailer.mailer.send("../public/emails/appointment_cancled", {
            to: app.user_id.email,
            subject: "Appointment canceled",
            appointment: app,
            date: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
            siteurl: config.globals.siteurl
        }, function (err) {
            req.flash('popupmessage', 'Appointment has been canceled successfully');
            res.redirect("/appointments/");
            return;
        });

    })
}
function clientCancelAppointment(req, res, app){
    console.log('CANCEL CLIENT !!!!!!!!')
    app.confirmation_status = false;
    app.client_cancel = true;
    app.completedAt = moment();
    app.save(function (error) {
        if (error) {
            req.flash('popupmessage', 'Unable to cancel appointment, please try again.');
            res.redirect("/appointments/cancle/" + app._id);
            return;
        }
        //notify admin

        //notify to barber on sms
        var temp1 = app.barber_id.phone.split(/[()\s\t-]/);
        app.barber_id.phone = temp1.join("");
        //app.date =  moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A') +" GMT -0400 (EDT)"
        console.log('!!!!!!!!!!!!!', "You cancel appointment for "+ moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A') +". \nKeep doing your thing, boss!\n\n  FILNU")
        if(app.manually == true){
                req.flash('popupmessage', 'Appointment has been canceled successfully');
                res.redirect("/appointments/");
                return;
        }else{
          var title = (app.price && (app.price !== 'undefined'))?"Appointment 'pay as you go' canceled(by client) ":"Appointment canceled(by client)";
            expressmailer.mailer.mailer.send("../public/emails/admin_appointment", {
                to: config.globals.adminmain,
                subject: title,
                action: 'canceled',
                barber: app.barber_id,
                siteurl: config.globals.siteurl,
                customer: app.user_id,
                price: app.price,
                campaign:app.action,
                date: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
                client_name: app.client_name,
                client_cancel: app.client_cancel
            }, function (err) {
                console.log(err)
            });
            client.messages.create({
                body: "We are sorry to inform you that "+ app.user_id.username+" has cancelled the appointment for "+ moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A') +". \nKeep doing your thing, boss!\n\n  FILNU",
                to: "+1" + (app.barber_id.phone),
                from: config.twillio.number
            }, function (err, message) {
                console.log(err, message);
            });
            expressmailer.mailer.mailer.send("../public/emails/appointment_canceled_by_user", {
                to: app.barber_id.email,
                subject: "Appointment canceled",
                appointment: app,
                date: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
                siteurl: config.globals.siteurl
            }, function (err) {
                req.flash('popupmessage', 'Appointment has been canceled successfully');
                res.redirect("/appointments/");
                return;
            });
        }


    })
}
function getCountAppointmentsPerMonth(req, callback){
    var subscriptionStartDate = moment(new Date(req.session.subscription.start)),
        //subscriptionEndDate = moment(new Date(req.session.subscription.start)).add(1, 'month'),
        subscriptionEndDate = moment(new Date(req.session.subscription.created))
          .add(Math.ceil(moment().diff(moment(new Date(req.session.subscription.created)), 'months', true)), 'month'),

        val = mongoose.Types.ObjectId(req.user._id);
    Appointment.aggregate([
        {
            $match: {
                user_id: val,
                created: {$gte: subscriptionStartDate.toDate(), $lte: subscriptionEndDate.toDate()},
                confirmation_status: true
            }
        },
        {
            $group: {
                '_id': '$status',
                total: {$sum: 1}
            }
        }
    ]).exec(function (err, docs) {
        console.log("---Aggregate---");
        console.log(docs, docs[0])
        var count = 0,
          cuts = req.user.cuts||4;
        if (docs.length > 0) {
            if (typeof docs[0] != 'undefined') {
                count += docs[0].total;
            }
            if (typeof docs[1] != 'undefined') {
                count += docs[1].total;
            }
            callback(null, cuts-count);
        }else{
            callback(null, cuts)
        }

    })
}
function getAppointments(req, res, usr){
  Appointment.find({"barber_id": req.params._id, confirmation_status:{$ne:false}}).populate('user_id').exec(function (err, appointments) {
    User.findOne({_id:req.params._id}, function(err, user){
      res.locals.title = "Barber History";
      if(usr){
        var backUrl = (req.user.type == 1)?'/profile':'/barber/'+user.username;
        req.user.type = 1;
      }else{
        var backUrl = '/barber/'+user.username
        user.type = 1;
      }

      res.render("../public/views/users/appointments", {
        user: (usr)?req.user:user,
        appointments: appointments,
        popupmessage: req.flash('popupmessage'),
        error: req.flash('error'),
        moment: moment,
        backUrl:backUrl
      });
    })

  });
}
module.exports = router;
