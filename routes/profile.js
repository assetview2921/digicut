var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    moment = require('moment'),
    Reviews = require("../models/reviews"),
    reg_password = /^[^\s]{5,}$/,
    async = require("async"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    User = require('../models/user'),
    BarberCutPic = require("../models/barber_cutpics"),
    multiparty = require('connect-multiparty');
multipartyMiddleware = multiparty();
// define the home page route
router.get('/', auth.isLoggedIn, auth.isClient, function (req, res) {
    if(req.user.type==1){
        User.findOne({
            _id:req.user._id
        }).exec(function(err,barber){
            if(err==null){
                BarberCutPic.find({
                    user_id:barber._id
                },function(err,barbercutpics){
                    if(!err){
                        Appointment.count({barber_id:barber._id,status:true},function(err,count){
                            Reviews.find({barber_id:barber._id},function(cerr,reviewCount){
                              async.parallel({
                                  main_pic: function(callback){
                                    BarberCutPic.find({user_id:barber._id, default:true}, function(err, cutPic){
                                      if(!err && cutPic){
                                        callback(err, cutPic[0])
                                      }else{
                                        callback(err, null)
                                      }

                                    }).sort({created:-1})
                                  },
                                  cut_pic: function(callback){
                                    BarberCutPic.findOne({user_id:barber._id}, function(err, cutPic){
                                      callback(err, cutPic)
                                    })
                                  }
                                },
                                function(err, results) {
                                  var barberCutPic = (results.main_pic)?results.main_pic:results.cut_pic;
                                  if(barberCutPic){
                                    barberCutPic = "https://digicut-public.s3.amazonaws.com/"+barberCutPic.pic;
                                  }else{
                                    if(barber.profile_pic){
                                      barberCutPic = "https://digicut-public.s3.amazonaws.com/"+barber.profile_pic;
                                    }else{
                                      barberCutPic = "https://s3-us-west-2.amazonaws.com/digicut-public/logo.png";
                                    }
                                  }
                                  res.render("../public/views/users/barbers/profile", {
                                    barberCutPic: barberCutPic,
                                    description:"Filnu is the easiest way to find a barber",
                                    keywords: "barber, barbers, barbershop, barbershops, haircut, black barber, black barbers, black barbershop,"+barber.username+","+barber.address.details,
                                    link:config.globals.siteurl+'barber/'+barber.username,
                                    popupmessage: req.flash('popupmessage'),
                                    isBarber:true,
                                    barber:barber,
                                    cutpics:barbercutpics,
                                    appointments:count,
                                    reviews: reviewCount,
                                    countReviews:reviewCount.length
                                  });
                                  return;
                                })

                            })
                        })
                    }
                })
            }
        })
    }
    res.locals.title = "Barber Profile";
});

router.get('/edit', auth.isLoggedIn, auth.isClient, function (req, res) {
    res.render("../public/views/users/edit_profile", {user: req.user, popupmessage: req.flash('popupmessage'), error: req.flash('error')});
});

router.get("/change-password", auth.isLoggedIn, auth.isClient, function (req, res) {
    res.render("../public/views/users/change-password", {user: req.user, error: req.flash('error'), popupmessage: req.flash('popupmessage')});
})
router.post("/change-password", function (req, res) {
    if(req.body.newpassword !== req.body.repeatnewpassword){
        req.flash('error', true);
        req.flash('popupmessage', "'New password' and 'Confirm new password' are not the same");
        res.redirect("/profile/change-password/");
        return;
    }else{
        var usr = new User();
        if(!reg_password.test(req.body.newpassword)){
          req.flash('error', true);
          req.flash('popupmessage', "'New password' should contain at least 5 characters, it can't contain special characters: -/:;()$&@.,?!+='\"");
          res.redirect("/profile/change-password/");
          return;
        }else {
          User.findOne({'_id': req.body._id}, function (err, user) {
            if (!user.validPassword(req.body.password)) {
              req.flash('error', true);
              req.flash('popupmessage', "Invalid old password");
              res.redirect("/profile/change-password/");
              return;
            } else {
              if (err) {
                error = err;
                popupmessage = "Unable to find user";
              } else if (user) {
                user.password = usr.generateHash(req.body.newpassword);
                user.isTemporaryPassword = false;
                user.save();
                error = false;
                popupmessage = "Password has been updated sucessfully";
              } else {
                popupmessage = "Unable to save new password";
                error = true;
              }
            }
            req.flash('error', error);
            req.flash('popupmessage', popupmessage);
            if (user.type == 1) return res.redirect("/profile");
            return res.redirect("/profile/edit");

          });
        }
    }
})
function findByUserId(id) {
    user = User.findOne({_id: id}, {aka: 1, first: true, last: true}, function (err, doc) {
        return  doc;
    });
    return user;
}

router.post("/update", auth.isLoggedIn, auth.isClient, multipartyMiddleware, function (req, res) {
    //check if user exists
    User.findOne({username: req.user.username}, function (err, user) {
        if (err || user == null) {
            req.flash('error', true);
            req.flash("popupmessage", "No User found");
            res.redirect("/profile/edit");
        }
        //check if pic is available
        if (req.files.profile_pic.name) {
            //rename pic
            picname = (new Date().getTime()) + req.files.profile_pic.name;
            user.profile_pic = picname;
            var fs = require('fs')
//read and upload to folder
            fs.readFile(req.files.profile_pic.path, function (err, data) {
                var newPath = "./uploads/users/" + picname;
                fs.writeFile(newPath, data, function (err) {
                });
            });
            user.save(function (err) {
                if (err) {
                    req.flash('error', true);
                    req.flash('popupmessage', err.errors.first.message);
                    res.redirect("/profile/edit");
                } else {
                    req.flash('error', false);
                    req.flash('popupmessage', "User profile has been updated successfully");
                    expressmailer.mailer.mailer.send("../public/emails/profile_update", {
                        to: req.user.email,
                        subject: "Profile Updated",
                        data: req.user
                    }, function (err) {
                    });
                    req.user = user;
                    res.redirect("/profile/edit");
                }

            })
        }


    });

})

router.post("/update/address", auth.isLoggedIn, auth.isClient, multipartyMiddleware, function (req, res) {
    //check if user exists
    User.findOne({username: req.user.username}, function (err, user) {
        if (err || user == null) {
            req.flash('error', true);
            req.flash("popupmessage", "No User found");
            res.redirect("/profile/edit");
        }
        geo = req.body.barber.address.geo;
        console.log(geo);
        user.barber.address = req.body.barber.address;
        user.barber.address.geo = [parseFloat(geo[0]), parseFloat(geo[1])];
        user.save(function (err) {
            if (err) {
                console.log(err);
                req.flash('error', true);
                req.flash('popupmessage', err.errors.first.message);
                res.redirect("/profile/edit");
            } else {
                req.flash('error', false);
                req.flash('popupmessage', "User profile has been updated successfully");
                expressmailer.mailer.mailer.send("../public/emails/profile_update", {
                    to: req.user.email,
                    subject: "Profile Updated",
                    data: req.user,
                    siteurl: config.globals.siteurl,
                }, function (err) {
                });
                req.user = user;
                res.redirect("/profile/edit");
            }
        })
    });
})

router.post("/update/password", auth.isLoggedIn, multipartyMiddleware, function (req, res) {
    //check if user exists
    var usr = new User(),
      reg_password = /^[^\s]{5,}$/;

    if(!reg_password.test(req.body.new_password)){
      req.flash('error', true);
      req.flash("popupmessage", "New password must contain at least 5 charactersPassword should be at least 5 characters (without spaces)");
      res.redirect("/profile/edit");
      return;
    }else {
      User.findOne({username: req.user.username}, function (err, user) {
        console.log();
        if (err || user == null || !user.validPassword(req.body.password)) {
          req.flash('error', true);
          req.flash("popupmessage", "Invalid Password");
          res.redirect("/profile/edit");
          return;
        }
        user.password = usr.generateHash(req.body.new_password);
        user.save(function (err) {
          if (err) {
            req.flash('error', true);
            req.flash('popupmessage', err.errors.first.message);
            res.redirect("/profile/edit");
            return;
          } else {
            req.flash('error', false);
            req.flash('popupmessage', "Your password has been updated successfully");
            req.user = user;
            //send confirmation mail
            expressmailer.mailer.mailer.send("../public/emails/password_update", {
              to: req.user.email,
              subject: "Password Changed",
              siteurl: config.globals.siteurl,
              data: req.user
            }, function (err) {
            });
            res.redirect("/profile/edit");
            return;
          }
        })
      });
    }
})

router.post("/edit",auth.isLoggedIn, auth.isClient, function(req,res){
    var body = req.body.user;
  //if(req.user.username.toString() !== req.body.user.username.toString){
  //  req.flash('error', true);
  //  //req.flash("popupmessage","Unable to update profile");
  //  req.flash("popupmessage","You can't change username");
  //  res.redirect("/profile/edit");
  //}else{
    User.findOne({ $and: [ { username: { $ne: req.user.username } }, {$or: [{'email': req.body.user.email}, {'phone': req.body.user.phone}]}]}, function (err, user) {
      // if there are any errors, return the error
      //if (err){ return done(err);}

      // check to see if theres already a user with that email
      if (user) {
        req.flash('error', true);
        if (user.email == req.body.user.email) {
          message = req.body.user.email + " is already taken";
        } else if (user.phone == req.body.user.phone) {
          message = "Phone "+req.body.user.phone+" is already taken";
        } else {
          message = req.body.user.username + " is already taken";
        }
        req.flash('error', true);
        //req.flash("popupmessage","Unable to update profile");
        req.flash("popupmessage",message);
        res.redirect("/profile/edit");
      } else {


        User.findOne({_id: req.user._id}, function (err, doc) {
          if (err || doc == null) {
            req.flash("error", true);
            req.flash("popupmessage", 'unable to find user');
            return;
          }
          //if(body.address.geo == ""){
          var request = require('request');
          request("https://maps.googleapis.com/maps/api/geocode/json?address=" +  body.address.address + ", " + body.address.city + ", " + body.address.state + ", " + body.address.zipcode, function (err, response, rBody) {
            responseBody = JSON.parse(rBody);
            try {
              results = responseBody.results;
              doc.address.details = body.address.address + ", " + body.address.city + ", " + body.address.state + ", " + body.address.zipcode;
              var address_addr = '';
              for (addr in results[0].address_components) {
                address = results[0].address_components[addr];
                //console.log(results[0].address_components[addr],addr);
                console.log(address.types);
                if (address.types.indexOf('street_number') >= 0) {
                  address_addr += (address_addr !== '')?", "+address.short_name:address.short_name;
                }
                if (address.types.indexOf('route') >= 0) {
                  address_addr +=  (address_addr !== '')?", "+address.short_name:address.short_name;
                }
                if (address.types.indexOf('postal_code') >= 0) {
                  doc.address.zipcode = (address.long_name)
                }
                if (address.types.indexOf('locality') >= 0) {
                  doc.address.city = (address.long_name)
                }
                if (address.types.indexOf('administrative_area_level_1') >= 0) {
                  doc.address.state = (address.long_name)
                }
                if (address.types.indexOf('country') >= 0) {
                  doc.address.country = (address.long_name)
                }
              }
              if (results.length > 0) {
                doc.address.address = address_addr;
                lat = results[0].geometry.location.lat;
                lng = results[0].geometry.location.lng;
                doc.address.geo = [Number(lat), Number(lng)];
              }
            } catch (exp) {
              console.log(exp);
            }
            if (doc.type === 1) {
              if (doc.barber.paypalemail !== body.barber.paypalemail) {
                doc.updated = moment();

                //if ! friday payments > datetime > Thursday
                var currentTursday = moment().day(4).hour(17).minutes(00).seconds(00),
                  currentFriday = moment().day(5).hour(9).minutes(00).seconds(00),
                  current = moment();

                if (!current.isBetween(currentTursday, currentFriday)) {
                  doc.barber.paypalemail = body.barber.paypalemail;
                  doc.barber.newpaypalemail = null;
                  //if ! friday payments > datetime > Thursday
                } else {
                  doc.barber.newpaypalemail = body.barber.paypalemail;
                }
              }
            }
            doc.first = body.first;
            doc.last = body.last;
            doc.email = body.email;
            doc.aka = body.aka;
            doc.phone = body.phone;
            doc.birthday = body.birthday;

            console.log('DOC>>>', doc);
            doc.save(function (err) {
              if (!err) {
                req.flash('error', false);
                req.flash("popupmessage", "Profile has been updated successfully");
                res.redirect("/profile/edit");
                return;
              } else {
                console.log('>>>>>>>>>>>>>>>>>>')
                console.log('>>>>>>>>>>>>>>>>>>')
                console.log('router.post("/edit")', err)
                console.log('>>>>>>>>>>>>>>>>>>')
                console.log('>>>>>>>>>>>>>>>>>>')
                req.flash('error', true);
                //req.flash("popupmessage","Unable to update profile");
                req.flash("popupmessage", err.errors.first.message);
                res.redirect("/profile/edit");
              }
            })
          })
        })
        console.log(req.body);
      }
    })
  //}


})
router.post("/update/email", auth.isLoggedIn, auth.isClient, multipartyMiddleware, function (req, res) {
    //check if user exists
    User = require('../models/user');
    if (typeof req.body.email != 'undefined') {
        var which = 'email',
            whatnew = 'new_email',
            whatTemporary = 'isTemporaryEmail';
    } else if (typeof req.body.username != 'undefined') {
        var which = 'username',
            whatnew = 'new_username',
            whatTemporary = 'isTemporaryUsername';
    }
    if (req.user[which] == req.body[whatnew]) {
        req.flash('error', true);
        req.flash("popupmessage", "New " + which + " must be different then the current one");
        res.redirect("/profile/edit");
        return;
    }
    var query = {};
    query[which] = req.body[which];
    User.findOne(query, function (err, user) {
        if (err || user == null) {
            req.flash('error', true);
            req.flash("popupmessage", "No User found");
            res.redirect("/profile/edit");
            return;
        }
        user[which] = req.body[whatnew];
        user[whatTemporary] = true;
        user.save(function (err) {
            if (err) {
                req.flash('error', true);
                req.flash('popupmessage', err.errors.first.message);
                res.redirect("/profile/edit");
                return;
            } else {
                req.flash('error', false);
                req.flash('popupmessage', "User " + which + " has been updated successfully");
                req.user = user;
                //send confirmation mail
                expressmailer.mailer.mailer.send("../public/emails/" + which + "_update", {
                    to: req.user.email,
                    subject: which + " Updated and Verification",
                    siteurl: config.globals.siteurl,
                    data: req.user
                }, function (err) {
                    console.log(err);
                });
                res.redirect("/profile/edit");
                return;
            }
        })
    });
});

module.exports = router;
