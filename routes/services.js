
var express = require('express'),
  router = express.Router(),
  User = require('../models/user'),
  Availability = require("../models/availability"),
  Appointment = require("../models/appointments"),
  Reviews = require("../models/reviews"),
  auth = require("../config/auth"),
  subscriber = require("../config/subscription"),
  formidable = require('formidable'),
  service_regExp = /^[a-zA-Z\+\s_-]{3,11}$/,
  expressmailer = require('../config/expressmailer'),
  config = require("../config/config"),
  _ = require('lodash');

router.get('/',auth.isLoggedIn, subscriber.isBarber, function (req, res) {
    req.flash('error', true);
    req.flash("popupmessage", '');
    res.locals.title = 'Services Offered';
    res.render("../public/views/users/barbers/services", {
      user: req.user,
      error: req.flash('error'),
      popupmessage: req.flash('popupmessage')
    });
});
router.get('/select/:barber',auth.isLoggedIn, subscriber.isCustomer, function (req, res) {
  User.findOne({username:req.params.barber, 'barber.working_hours':true}, function(err, barber){
    if(!err && barber){
      req.flash('error', true);
      req.flash("popupmessage", '');
      res.locals.title = 'Select Service';
      res.render("../public/views/users/pay_as_you_go", {
        barber: barber,
        error: req.flash('error'),
        popupmessage: req.flash('popupmessage')
      });
    }else{
      req.flash('error', true);
      req.flash('popupmessage', 'Unable to locate barber');
      res.redirect("/barber/search");
      return;
    }
  })
});

router.post('/',auth.isLoggedIn, subscriber.isBarber, getReqBody, function (req, res) {
  if(req.body.price<1){
    return res.send(400, 'Input correct service price');
  }else if(!(service_regExp.test(req.body.service))){
    return res.send(400, "Service name should be 3-10 characters and it can't contain digits");
  }else{
    if(_.where(req.user.services, {service:req.body.service.toUpperCase()}).length == 0){
      req.user.services.push({service:req.body.service.toUpperCase(), price: parseInt(req.body.price)});
      User.update({_id:req.user._id}, {$set:{services:req.user.services}}, function(err, user){
        console.log(' req.user.save(function(err, user){', err, user);
        return res.send(200,'Service successfully created');
      });
    }else{
      return res.send(400, 'Service "'+req.body.service.toUpperCase()+'" already exist');
    };
  };
});

router.put('/',auth.isLoggedIn, subscriber.isBarber, getReqBody, function (req, res) {
  if(req.body.new_price<1){
    return res.send(400, 'Input correct service price');
  }else if(!(service_regExp.test(req.body.new_service))){
    return res.send(400, "Service name should be 3-10 characters and it can't contain digits");
  }else{
    var services = updateServices(req);
    if(!services.error){
      User.update({_id:req.user._id}, {$set:{services:req.user.services}}, function(err, user){
        console.log(' req.user.save(function(err, user){', err, user);
        return res.send(200,'Service successfully updated');
      });
    }else{
      return res.send(400, services.error);
    };
  }
});

router.delete('/',auth.isLoggedIn, subscriber.isBarber, getReqBody, function (req, res) {
    var services = _.filter(req.user.services, function(obj){return obj.service !== req.body.service.toUpperCase()});
      User.update({_id:req.user._id}, {$set:{services: services}}, function(err, user){
        console.log(' req.user.save(function(err, user){', err, user);
        return res.send(200,'Service successfully deleted');
      });
});

function getReqBody(req, res, next){
  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    req.body = fields;
    next();
  });
};

function updateService(services, body, name){
  for(var i = 0; i<services.length; i++){
    if(services[i].service == body.old_service.toUpperCase()){
      services[i].service = body.new_service.toUpperCase();
      services[i].price = parseInt(body.new_price);
      break;
    }
    if(i==services.length-1) break;
  }
  return services;
};

function updateServices(req){
  if((req.body.new_service.toUpperCase() === req.body.old_service.toUpperCase()) &&(req.body.new_price === req.body.old_price)){
    return {error:false};
  }else if((req.body.new_service.toUpperCase() === req.body.old_service.toUpperCase())){
    req.user.services = updateService(req.user.services, req.body);
    return {error:false};
  }else{
    if(_.where(req.user.services, {service:req.body.new_service.toUpperCase()}).length == 0){
      req.user.services = updateService(req.user.services, req.body);
      return {error:false};
    }else{
      return {error:'Service "'+req.body.new_service.toUpperCase()+'" already exist'};
    }
  }
};


module.exports = router;
