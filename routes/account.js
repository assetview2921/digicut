var express = require('express'),
    router = express.Router(),
    auth = require("../config/auth"),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    expressmailer = require('../config/expressmailer'),
    subscriber = require("../config/subscription"),
    config = require("../config/config"),
    moment = require('moment'),
    formidable = require('formidable'),
    PayPal = require('paypal-rest-sdk');
PayPal.configure({
    'mode': config.paypal.mode, //sandbox or live
    'client_id': config.paypal.client_id,
    'client_secret': config.paypal.client_secret
})

/***
 * get user subscription state (there is no page for the same)
 * */
router.get('/subscription', auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, function (req, res) {
    if (req.user.type == 1) {
        which = 'barber';
    } else if (req.user.type == 0) {
        which = 'customer';
    }
  if(req.session.subscription){
    PayPal.billing_agreement.get(req.session.subscription.subscription_id, function (err, info) {
      res.send(info);
    })
  }else{
    res.send('');
  }

});
/***
 * for account deactivation
 * upon successfull deactivation it will redirect to dashboard
 * */
router.get('/subscription/deactivate', auth.isLoggedIn, auth.isClient, function (req, res) {
    if (req.user.type == 1) {
        which = 'barber';
    } else if (req.user.type == 0) {
        which = 'customer';
    }
    var suspend_note = {
        "note": "Suspending the agreement"
    };
    PayPal.billing_agreement.cancel(req.session.subscription.subscription_id, suspend_note, function (err, response) {
        if (!err) {
            User.findOne({_id: req.user._id}, function (uError, user) {
                user.active = false;
                req.session.subscription = {};
                user.save(function (usr) {
                    req.user = usr;
                    req.flash("error", false);
                    req.flash('popupmessage', "Your account has been deactivate successfully");
                    res.redirect("/dashboard");
                    return;
                })
            })
        }
        req.flash("error", true);
        req.flash('message', "Unable to deactivate plan");
        res.redirect("/account/subscription");
        return;
    })
});

/***
 * not in use
 * */
router.get('/edit', auth.isLoggedIn, auth.isClient, function (req, res) {
    res.render("../public/views/account/edit", {user: req.user});
});
/***
 * payment page
 * */
router.get("/subscribe", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, subscriber.checkSubscription, function (req, res) {
    if (!req.notSubscribed) {
        res.locals.title = "Current credit card"
        res.render("../public/views/account/creditCardInfo", {
            error: req.flash('error', false),
            user: req.user,
            popupmessage: req.flash('popupmessage','')
        });
    }else{
        res.locals.title = "Subscribe";
        res.render("../public/views/account/subscribe", {
            error: req.flash('error'),
            user: req.user,
            popupmessage: req.flash('popupmessage')
        });
    }
})

router.get("/subscription_option", auth.isLoggedIn, auth.isClient, subscriber.isCustomer,subscriber.checkSubscription, function (req, res) {
    if(req.notSubscribed){
        res.locals.title = "Subscription Option";
      var backURL = (req.query.service)?"/order/info/"+req.query.date+"/"+req.query.barber+"/?service="+req.query.service+"&time="+req.query.selectedTime:'/services/select/'+req.query.barber
        res.render("../public/views/account/subscription_option", {
            error: req.flash('error', false),
            user: req.user,
            barber:req.query.barber,
            date:req.query.date,
            datetime:req.query.selectedTime,
            service:req.query.service,
            backURL:backURL,
            popupmessage: req.flash('popupmessage','')
        });
    }else{
        res.locals.title = "Find A Barber";
        res.redirect("/barber/search");
    }
})

router.get("/change_card",auth.isLoggedIn, auth.isClient, function(req, res, next) {
    if (req.user.active == 1) {
        res.locals.title = "Change credit card"
        res.render("../public/views/account/changeCreditCard", {
            error: req.flash('error', false),
            user: req.user,
            popupmessage: req.flash('popupmessage','')
        });
        return;
    }else{
        res.locals.title = "Subscribe";
        res.render("../public/views/account/subscribe", {
            error: req.flash('error'),
            user: req.user,
            popupmessage: req.flash('popupmessage')
        });
        return;
    }
})

router.post("/change_card",auth.isLoggedIn, auth.isClient, getBillingAgreement, changeCreditCard)

//router.post("/subscription_option",auth.isLoggedIn, auth.isClient, getBillingAgreement, changeCreditCard)
/***
 *
 * handle credit card info
 * */
router.post("/subscribe", auth.isLoggedIn, auth.isClient, function (req, res) {
    //do payments if useris saved successfull
    console.log('req.body credit card', req.body.credit_card)
    //create credit card
    PayPal.credit_card.create(req.body.credit_card, function (error, credit_card) {
        console.log('error card ==>>',error, credit_card)
        if (error) {
            console.log(error);
            req.flash('error', true);
            errors = [];
            for (i in error.response.details) {
                field = error.response.details[i].field.replace("_", " ");
                if (error.response.details[i].field == 'number') {
                    field = "credit card";
                }
                errors.push(field + " " + error.response.details[i].issue);
            }
            console.log(errors);
            req.flash("popupmessage", errors.join(","));

          if(req.body.barber && req.body.datetime){
            res.render("../public/views/account/subscription_option", {
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage'),
              user: req.user,
              barber: req.body.barber,
              datetime: req.body.datetime,
              service: req.body.service,
              date: req.body.date,
              backURL:'/order/info/'+req.body.date+'/'+req.body.barber+'/?service='+req.body.service+'&time='+req.body.datetime

            });
            return;
          }else if(req.body.barber){
            res.render("../public/views/account/subscription_option", {
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage'),
              user: req.user,
              barber: req.body.barber,
              backURL: '/services/select/'+req.body.barber
            });
            return;
          }else{

            res.render("../public/views/account/subscribe", {
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage'),
              user: req.user
            });
            return;
          }
        }
        var now = new Date();
        now.setMinutes(now.getMinutes() + 5);
        now = now.toISOString();
        var dt = now.split(".")[0] + ("Z");
        if (req.user.type == "1") {
            var which = "barber";
        } else {
          switch (req.body.cuts){
            case 4: {
              var which = "customer_changed_card";
              break;
            }
            case 2: {
              var which = "customer_changed_card_2_cuts";
              break;
            }
            default: {
              var which = "customer_changed_card";
              break;
            }
          }

        }
        if (req.user.type == 1) {
            var lookup = require('country-data').lookup;
            var country = lookup.countries({name: req.user.address.country})[0];
        }

        //create billing agreement data
        billing_agreement_info = {
            name: 'Filnu ' + which + ' Subscription',
            description: 'Filnu ' + which + ' Subscription',
            start_date: dt,
            plan: {
                "id": config.paypal.billingPlans[which]
            },
            payer: {
                payment_method: 'credit_card',
                funding_instruments: [
                    {
                        "credit_card": {
                            number: req.body.credit_card.number,
                            type: req.body.credit_card.type,
                            expire_month: req.body.credit_card.expire_month,
                            expire_year: req.body.credit_card.expire_year,
                            cvv2: req.body.credit_card.cvv2,
                            first_name: req.body.credit_card.first_name,
                            last_name: req.body.credit_card.last_name
                        }
                    }
                ]
            }
        }

        PayPal.billing_agreement.create(billing_agreement_info, function (err, info) {
                console.log('errr, info >>>>>>>>', err, info)
            if (err) {
                console.log(err);
                req.flash('error', true);
                errors = [];
                for (i in err.response.details) {
                    field = err.response.details[i].field.replace("_", " ");
                    errors.push(field + " " + err.response.details[i].issue);
                }
                console.log(errors);
                req.flash("popupmessage", errors.join(","));
              if(req.body.barber && req.body.datetime){
                res.render("../public/views/account/subscription_option", {
                  error: req.flash('error'),
                  popupmessage: req.flash('popupmessage'),
                  user: req.user,
                  barber: req.body.barber,
                  datetime: req.body.datetime,
                  service: req.body.service,
                  date: req.body.date,
                  backURL:'/order/info/'+req.body.date+'/'+req.body.barber+'/?service='+req.body.service+'&time='+req.body.datetime

                });
                return;
              }else if(req.body.barber){
                res.render("../public/views/account/subscription_option", {
                  error: req.flash('error'),
                  popupmessage: req.flash('popupmessage'),
                  user: req.user,
                  barber: req.body.barber,
                  backURL: '/services/select/'+req.body.barber
                });
                return;
              }else{

                res.render("../public/views/account/subscribe", {
                  error: req.flash('error'),
                  popupmessage: req.flash('popupmessage'),
                  user: req.user
                });
                return;
              }
            } else {
                var Subscriptions = require("../models/subscriptions");
                var User = require('../models/user');

                var subscription = new Subscriptions();
                subscription.user_id = req.user._id;
                subscription.status = true;
                subscription.subscription_id = info.id;
                subscription.save(function (err, sub) {
                    console.log(sub);
                    req.session.subscription = sub;
                    User.findOne({_id: req.user._id}, function (user_err, doc) {
                        doc.active = 1;
                        if(req.user.type == 0){
                          doc.cuts = req.body.cuts||4;
                        }
                        var card_digits = '';
                        req.body.credit_card.number = req.body.credit_card.number.toString();
                        for(var i = 1; i <= Math.ceil(req.body.credit_card.number.length/4); i++){
                            if(Math.ceil(req.body.credit_card.number.length/4)==i){
                                card_digits += req.body.credit_card.number.substr(-4);
                            } else {
                                if(req.body.credit_card.number.length!==16 && i == 1){
                                    card_digits += new Array(5-(Math.ceil(req.body.credit_card.number.length/4)*4-req.body.credit_card.number.length)).join( '*' );
                                    card_digits += " ";
                                }else{
                                    card_digits += "**** ";
                                }
                            }
                        }
                        doc.card.digits = card_digits;
                        doc.card.first = req.body.credit_card.first_name;
                        doc.card.last = req.body.credit_card.last_name;
                        doc.save(function (response) {
                            console.log(response);
                            if (response) {
                                req.flash('error', true);
                                errors = [];
                                for (i in err.response.details) {
                                    errors.push(error.response.details[i].issue);
                                }
                                req.flash("popupmessage", errors.join(","));
                              if(req.body.barber && req.body.datetime){
                                res.render("../public/views/account/subscription_option", {
                                  error: req.flash('error'),
                                  popupmessage: req.flash('popupmessage'),
                                  user: req.user,
                                  barber: req.body.barber,
                                  datetime: req.body.datetime,
                                  service: req.body.service,
                                  date: req.body.date,
                                  backURL:'/order/info/'+req.body.date+'/'+req.body.barber+'/?service='+req.body.service+'&time='+req.body.datetime
                                });
                                return;
                              }else if(req.body.barber){
                                res.render("../public/views/account/subscription_option", {
                                  error: req.flash('error'),
                                  popupmessage: req.flash('popupmessage'),
                                  user: req.user,
                                  barber: req.body.barber,
                                  backURL: '/services/select/'+req.body.barber
                                });
                                return;
                              }else{

                                res.render("../public/views/account/subscribe", {
                                  error: req.flash('error'),
                                  popupmessage: req.flash('popupmessage'),
                                  user: req.user
                                });
                                return;
                              }
                            }

                            //send confirmation mail
                            var expressmailer = require('../config/expressmailer');
                            expressmailer.mailer.mailer.send("../public/emails/subscribed", {
                                to: doc.email,
                                subject: "User Subscritption",
                                data: doc,
                                siteurl: config.globals.siteurl
                            }, function (err) {
                                if (err) {
                                    req.flash('error', true);
                                    req.flash("popupmessage", err);
                                    res.render("../public/views/account/subscribe", {
                                        error: req.flash('error'),
                                        popupmessage: req.flash('popupmessage')
                                    });
                                } else {
                                  //if we should redirect users to appointment details after subscription
                                  if(req.body.barber && req.body.datetime){
                                    console.log('subscription after popup')
                                    createAppointment(req, res);
                                    //create new app and redirect "appointment/app._id"
                                  }else if(req.body.barber){
                                    req.flash('error', false);
                                    req.flash("popupmessage", "Subscription status has been updated");
                                    req.user = doc;
                                    res.redirect("/select-date/"+req.body.barber);
                                    return;
                                  }else{
                                    req.flash('error', false);
                                    req.flash("popupmessage", "Subscription status has been updated");
                                    req.user = doc;
                                    res.redirect("/dashboard");
                                    return;
                                  }
                                }
                            })
                        })
                    });
                });
            }
        })
    });
})

function getBillingAgreement(req,  res, next){
    PayPal.billing_agreement.get(req.session.subscription.subscription_id, function(err, data){
        req.billingAgreement = data;
        next()
    })
}
function changeCreditCard(req, res){
    var suspend_note = {
        "note": "Suspending the agreement"
    },
    reactivate_note = {
        "note": "Reactivating the agreement"
    };
    PayPal.credit_card.create(req.body.credit_card,
    function (error, credit_card) {
        console.log('error card ==>>', error, credit_card)
        if (error) {
            console.log(error);
            req.flash('error', true);
            errors = [];
            for (i in error.response.details) {
                field = error.response.details[i].field.replace("_", " ");
                if (error.response.details[i].field == 'number') {
                    field = "credit card";
                }
                errors.push(field + " " + error.response.details[i].issue);
            }
            console.log(errors);
            req.flash("popupmessage", errors.join(","));
            res.locals.title = "Change credit card"
            res.render("../public/views/account/changeCreditCard", {
                error: req.flash('error'),
                popupmessage: req.flash('popupmessage'),
                user: req.user
            });
            return;
        }else{
            PayPal.billing_agreement.suspend(req.session.subscription.subscription_id, suspend_note, function (err, response) {
            //PayPal.billing_agreement.cancel(req.session.subscription.subscription_id, suspend_note, function (err, response) {
                if (!err) {

                    var now = new Date();
                    now.setMinutes(now.getMinutes() + 1);
                    now = now.toISOString();
                    var dt = now.split(".")[0] + ("Z");
                    if (req.user.type == "1") {
                        var which = "barber_changed_card";
                    } else {
                      switch (req.user.cuts){
                        case 4: {
                          var which = "customer_changed_card";
                          break;
                        }
                        case 2: {
                          var which = "customer_changed_card_2_cuts";
                          break;
                        }
                              // ? what for "Pay as you go" ?
                        default: {
                          var which = "customer_changed_card";
                          break;
                        }
                      }
                    }
                    //var which = 'customer_changed_card';
                    billing_agreement_info = {
                        name: 'Filnu ' + which + ' Subscription',
                        description: 'Filnu ' + which + ' Subscription',
                        start_date: req.billingAgreement.agreement_details.next_billing_date,
                        plan: {
                            "id": config.paypal.billingPlans[which]
                        },
                        payer: {
                            payment_method: 'credit_card',
                            funding_instruments: [
                                {
                                    "credit_card": {
                                        number: req.body.credit_card.number,
                                        type: req.body.credit_card.type,
                                        expire_month: req.body.credit_card.expire_month,
                                        expire_year: req.body.credit_card.expire_year,
                                        cvv2: req.body.credit_card.cvv2,
                                        first_name: req.body.credit_card.first_name,
                                        last_name: req.body.credit_card.last_name
                                    }
                                }
                            ]
                        }
                    }


                    PayPal.billing_agreement.create(billing_agreement_info, function (err, info) {
                        console.log('errr, info >>>>>>>>', err, info)
                        if (err) {
                            console.log('=================');
                            console.log('=================');
                            console.log(" can't create billing_agreement after suspending previouse  ",req.user, err, info);
                            console.log('=================');
                            console.log('=================');

                            PayPal.billing_agreement.reactivate(req.session.subscription.subscription_id, reactivate_note, function (error, response) {
                                if(error){
                                    console.log('=================');
                                    console.log('=================');
                                    console.log(" can't reactive billing_agreement after suspending previouse  ",req.session.subscription.subscription_id, req.user, err, info);
                                    console.log('=================');
                                    console.log('=================');
                                    req.flash("popupmessage", 'Cann`t update payment info');
                                    res.render("../public/views/account/changeCreditCard", {
                                        error: req.flash('error'),
                                        popupmessage: req.flash('popupmessage'),
                                        user: req.user
                                    });
                                    return;
                                }else{
                                    console.log('=================');
                                    console.log('reactivated successfully');
                                    console.log('=================');
                                    req.flash('error', true);
                                    errors = [];
                                    for (i in response.details) {
                                        field = response.details[i].field.replace("_", " ");
                                        errors.push(field + " " + response.details[i].issue);
                                    }
                                    console.log(errors);
                                    req.flash("popupmessage", errors.join(","));
                                    res.render("../public/views/account/changeCreditCard", {
                                        error: req.flash('error'),
                                        popupmessage: req.flash('popupmessage'),
                                        user: req.user
                                    });
                                    return;
                                }
                            })
                        } else {
                            PayPal.billing_agreement.cancel(req.session.subscription.subscription_id, suspend_note, function (err_c, response_c) {
                                if(err_c){
                                    console.log('=================');
                                    console.log('=================');
                                    console.log(" can't cancel previouse billing_agreement after subscribtion to new!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",req.session.subscription.subscription_id, req.user, err, info);
                                    console.log('=================');
                                    console.log('=================');
                                    req.flash("popupmessage", 'Cann`t update payment info');
                                    res.render("../public/views/account/changeCreditCard", {
                                        error: req.flash('error'),
                                        popupmessage: req.flash('popupmessage'),
                                        user: req.user
                                    });
                                    return;
                                }else{
                                    var Subscriptions = require("../models/subscriptions");
                                    var User = require('../models/user');
                                    var subscription = new Subscriptions();
                                    subscription.user_id = req.user._id;
                                    subscription.status = true;
                                    subscription.subscription_id = info.id;
                                    subscription.created = moment(new Date(req.session.subscription.created)).add(1, 'seconds')
                                    subscription.save(function (err, sub) {
                                        console.log('=================');
                                        console.log(sub, req.user, req.session.subscription.subscription_id);
                                        console.log('=================');
                                        req.session.subscription = sub;
                                        User.findOne({_id: req.user._id}, function (user_err, doc) {
                                            doc.active = 1;
                                            var card_digits = '';
                                            req.body.credit_card.number = req.body.credit_card.number.toString();
                                            for(var i = 1; i <= Math.ceil(req.body.credit_card.number.length/4); i++){
                                                 if(Math.ceil(req.body.credit_card.number.length/4)==i){
                                                     card_digits += req.body.credit_card.number.substr(-4);
                                                 } else {
                                                     if(req.body.credit_card.number.length!==16 && i == 1){
                                                         card_digits += new Array(5-(Math.ceil(req.body.credit_card.number.length/4)*4-req.body.credit_card.number.length)).join( '*' );
                                                         card_digits += " ";
                                                     }else{
                                                         card_digits += "**** ";
                                                     }
                                                 }
                                             }
                                            doc.card.digits = card_digits;
                                            doc.card.first = req.body.credit_card.first_name;
                                            doc.card.last = req.body.credit_card.last_name;
                                            doc.save(function (response) {
                                                console.log(response);
                                                if (response) {
                                                    req.flash('error', true);
                                                    errors = [];
                                                    for (i in err.response.details) {
                                                        errors.push(error.response.details[i].issue);
                                                    }
                                                    req.flash("popupmessage", errors.join(","));
                                                    console.log('=================');
                                                    console.log('=================');
                                                    console.log(" can't save user after changing credit card", req.user, response);
                                                    console.log('=================');
                                                    console.log('=================');
                                                    res.render("../public/views/account/changeCreditCard", {
                                                        error: req.flash('error'),
                                                        popupmessage: req.flash('popupmessage')
                                                    });
                                                    return;
                                                } else {
                                                    //send confirmation mail
                                                    var expressmailer = require('../config/expressmailer');
///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                                    expressmailer.mailer.mailer.send("../public/emails/payment_info", {
                                                        to: doc.email,
                                                        subject: "Payment info",
                                                        data: doc,
                                                        siteurl: config.globals.siteurl
                                                    }, function (err) {
                                                        if (err) {
                                                            req.flash('error', true);
                                                            req.flash("popupmessage", err);
                                                            res.render("../public/views/account/changeCreditCard", {
                                                                error: req.flash('error'),
                                                                popupmessage: req.flash('popupmessage')
                                                            });
                                                            return;
                                                        } else {
                                                            req.flash('error', false);
                                                            req.flash("popupmessage", "Payment info successfully updated");
                                                            req.user = doc;
                                                            res.redirect("/dashboard");
                                                            return;
                                                        }
                                                    })
                                                }


                                            })
                                        });
                                    });
                                }
                            })
                        }
                    })
                }else{
                    console.log('====================================')
                    console.log('====================================')
                    console.log("can't suspend billing agreement to change card((",req.user, err, response)
                    console.log('====================================')
                    console.log('====================================')
                    req.flash("error", true);
                    req.flash('message', "Unable to change credit card");
                    res.redirect("/dashboard");
                    return;
                }
            })
        }
    })
}
function createAppointment(req, res){
  User.findOne({username: req.body.barber}, function (err, doc) {
    if (doc == null) {
      req.flash('error', true);
      req.flash('popupmessage', 'Unable to locate barber');
      res.redirect("/barber/search");
      return;
    }
    console.log("Time is: ", req.body.datetime);
    var selectedTime = moment(new Date(req.body.datetime));
    console.log("Selected moment time is: ", selectedTime);
    var current = moment();
    //check if user is trying to book in back date/time
    if (selectedTime.unix() < current.unix()) {
      req.flash('error', true);
      req.flash('popupmessage', "You can't book in earlier dates");
      res.send(200,"/select-date/" + req.body.barber);
      return;
    }
    var day = selectedTime.format("dddd").substr(0, 3).toLowerCase(),
      hour = selectedTime.format("H") - 9;
    query = {};
    query["availability.0." + day + "." + hour] = "true";
    query['user_id'] = doc._id;
    console.log("Selected time and query is given below");
    console.log(query, selectedTime.toDate());
    Availability.findOne(query, function (err, ava) {
      if (ava == null) {
        console.log("You selected " + hour + " for " + day + "\nSystem date time is " + new Date().toString() + " UTC time is " + current.toDate());
        req.flash('error', true);
        req.flash('popupmessage', "oop's barber is not available at that time");
        res.redirect("/select-date/" + req.body.barber);
        return;
      }
      var barber = doc;
      //if(barber.service){
      //  if(barber.service.length){
      //    var service = _.where(barber.services, {service: req.query.service.toUpperCase()})
      //    if(service.length){
      //      var price = service[0].price;
      //    }
      //  }
      //}
      //if(!price){
      //  var price = _.where(config.services, {service: req.body.service.toUpperCase()})[0].price;
      //}
      var u = new User(),
        newApp = new Appointment();
      newApp.user_id = req.user._id;
      newApp.barber_id = doc._id;
      newApp.service = req.body.service;
      newApp.date = req.body.datetime;
      newApp.confirmation_code = u.generatePassword(6);
      //newApp.price = price;
      newApp.save(function (err, appo) {
        console.log(err);
        if (err == null) {
          User.findOne({username: req.body.barber}, {
            phone: 1,
            email: 1,
            aka: 1,
            first: 1,
            last: 1,
            barber: 1,
            'address.details': 1,
            'username': 1
          }, function (e, barber) {

            // Your accountSid and authToken from twilio.com/user/account
            //notify to barber
            expressmailer.mailer.mailer.send("../public/emails/barber_appointment", {
              to: doc.email,
              subject: "New appointment",
              user: barber,
              data: req.user,
              service: req.body.service,
              siteurl: config.globals.siteurl,
              date: moment(new Date(selectedTime).toISOString()).format('ddd MMM DD YYYY hh:mm A')
            }, function (err) {
            });
            //notify not admin

            //notify to customer
            expressmailer.mailer.mailer.send("../public/emails/customer_appointment", {
              to: req.user.email,
              subject: "Appointment confirmation",
              data: barber,
              date: moment(new Date(selectedTime).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
              user: req.user,
              code: newApp.confirmation_code,
              siteurl: config.globals.siteurl
            }, function (err) {
              console.log(err);
            });

            var accountSid = config.twillio.accountID,
              authToken = config.twillio.authToken,
              client = require('twilio')(accountSid, authToken),
            //notify to barber on sms
              temp1 = barber.phone.split(/[()\s\t-]/);
            barber.phone = temp1.join("");
            client.messages.create({
              body: "Dear " + barber.username + ", you have received a new appointment from " + req.user.username + " at " + moment(new Date(selectedTime.toISOString())).format('ddd MMM DD YYYY hh:mm A') + " for " + req.body.service + ". Please go to your profile and under alerts, enter the code that the client will give you to complete the transaction (in the same screen where you enter the code, you can also click \"cancel\" if you cannot cut at this time).",
              to: "+1" + (barber.phone),
              from: config.twillio.number
            }, function (err, message) {
              console.log(err, message);
              req.flash('error', false);
              req.flash('popupmessage', "Your appointment confirmation has been sent to barber for approval/rejection");
              res.redirect("/appointments/" + appo._id);
              return;
            });
          })
        }
        })
      });
    })
}
module.exports = router;
