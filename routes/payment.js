/**
 * Created by abhishekgarg on 02/08/15.
 */
var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    mongoose = require('mongoose'),
    async = require('async'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    ObjectID = mongoose.Types.ObjectId,
    config = require("../config/config"),
    moment = require('moment'),
    //checkingDate = moment().add(-1,'days').hour(17).minutes(00).seconds(00),
    PayPal = require('paypal-rest-sdk'),
    configuration = [
      {$match: {
        payed_to_barber: false,
        confirmation_status: true,
        status:true,
        manually:{$ne:true},
        price:{$exists: false}
      }},
      {$group: {
        '_id': '$barber_id',
        total: {$sum: config.globals.transaction_price},
        count: {$sum: 1}}
      }],
    configuration_pay_as_you_go = [
      {$match: {
        payed_to_barber: false,
        confirmation_status: true,
        status:true,
        manually:{$ne:true},
        price:{$exists: true}
      }},
      {$group: {
        '_id': '$barber_id',
        total: {$sum: '$price'},
        count: {$sum: 1}}
      }];
PayPal.configure({
    'mode': config.paypal.mode, //sandbox or live
    'client_id': config.paypal.client_id,
    'client_secret': config.paypal.client_secret
});

router.get("/payments/", function (req, res) {
    if (req.socket.remoteAddress.indexOf('127.0.0.1') < 0) {
        res.send({
            error: true,
            message: 'unauthorized access'
        });
        return false;
    }
    var sender_batch_id = Math.random().toString(36).substring(9);

    async.parallel({
      standard: function(callback){
        getAggregated(getConfiguration(), callback);
      },
      pay_as_you_go: function(callback){
        getAggregated(getConfiguration_pay_as_you_go(), callback);
      }
    }, function(err, result) {
      getFullResult(result.standard, result.pay_as_you_go, function (doc) {
        if (doc == null) {
          res.send({
            error: false,
            results: doc,
            message: 'No barbers found'
          });
        } else {
          inusers = [];
          for (i in doc) {
            inusers.push(doc[i]._id);
          }
          i = null;
          var items = [];


          User.find({_id: {$in: inusers}}, {username: 1, first: 1, last: 1, email: 1, 'barber.paypalemail':1,'barber.newpaypalemail':1}, function (err, users) {
            for (i in users) {
              items.push({
                "recipient_type": "EMAIL",
                "amount": {
                  "value": doc[i].total,
                  "currency": "USD"
                },
                "receiver": (users[i].barber.newpaypalemail)?users[i].barber.newpaypalemail:users[i].barber.paypalemail,
                "note": "Thank you.",
                "sender_item_id": "item_" + users[i]._id
              })
            }
            var create_payout_json = {
              "sender_batch_header": {
                "sender_batch_id": sender_batch_id,
                "email_subject": "You have a payment"
              },
              "items": items
            };

            PayPal.payout.create(create_payout_json, function (error, payout) {
              if (error) {
                console.log(error.response);
              } else {
                console.log("Create Payout Response");
                console.log(payout);
              }
            });
            Appointment.update({
              barber_id: {$in: inusers},
              payed_to_barber: false,
              confirmation_status: true,
              status:true,
              manually:{$ne:true}
            },
            {
              $set: {
                payed_to_barber: true,
                payDate: new Date()
              }
            }, {multi: true}, function (error, result) {
              console.log(error, result);
              res.send(create_payout_json);
            })
          })
        }
      });
    })
})

router.get("/admin-mail", function (req, res) {
    if (req.socket.remoteAddress.indexOf('127.0.0.1') < 0) {
        res.send({
            error: true,
            message: 'unauthorized access'
        });
        return false;
    }


    async.parallel({
      standard: function(callback){
        getAggregated(getConfiguration(), callback);
      },
      pay_as_you_go: function(callback){
        getAggregated(getConfiguration_pay_as_you_go(), callback);
      }
    }, function(err, result){
      getFullResult(result.standard,result.pay_as_you_go, function(doc){

        console.log('>>>>>>>>>>>>>>>');
        console.log('full_result', doc);

        if (doc == null) {
          res.send({
            error: false,
            results: doc,
            message: 'No barbers found'
          });
        } else {
          inusers = [];
          for (i in doc) {
            var val = mongoose.Types.ObjectId(doc[i]._id);
            inusers.push(val);
          }
          i = null;
          console.log('inusers', inusers);


          User.find({_id: {$in: inusers}}, {username: 1, first: 1, last: 1, email: 1}, function (err, users) {
            console.log('users',users);
            for (i in users) {
              doc[i].user = users[i];
            }
            expressmailer.mailer.mailer.send("../public/emails/admin_payments", {
              to: config.globals.adminmain,
              subject: "Weekly Payments Mail",
              data: doc,
              currency: config.globals.currency,
              siteurl: config.globals.siteurl,
              transaction_price: config.globals.transaction_price
            }, function (err1) {
              console.log('err1',err1);
              res.send({
                error: false,
                results: doc
              });
            });
          })
        }
      })
    })
});

router.get("/update-emails"), function(req, res){
  User.find({type:1, "barber.newpaypalemail":{$ne: null}}, function(err, barbers){
    async.eachSeries(barbers, function iterator(user, callback) {
      user.barber.paypalemail = user.barber.newpaypalemail;
      user.barber.newpaypalemail = null;
      user.save(function(err, barber){
        console.log(err, barber.username, barber.barber.paypalemail, barber.barber.newpaypalemail)
        callback();
      })
    }, function done() {
      console.log('barbers each series callback');
      return res.send(200);
    });
  });
}
function getConfiguration_pay_as_you_go(){
  return [
    {$match: {
      payed_to_barber: false,
      confirmation_status: true,
      status:true,
      manually:{$ne:true},
      completedAt:{$lte:moment().day(4).hour(17).minutes(00).seconds(00)},
      price:{$exists: true}
    }},
    {$group: {
      '_id': '$barber_id',
      total: {$sum: '$price'},
      count: {$sum: 1}}
    }]
}
function getConfiguration(){
  return [
    {$match: {
      payed_to_barber: false,
      confirmation_status: true,
      status:true,
      manually:{$ne:true},
      completedAt:{$lte:moment().day(4).hour(17).minutes(00).seconds(00)},
      price:{$exists: false}
    }},
    {$group: {
      '_id': '$barber_id',
      total: {$sum: config.globals.transaction_price},
      count: {$sum: 1}}
    }]
}
function getAggregated(aggregate_configuration, callback){
  Appointment.aggregate(aggregate_configuration, function (err, doc) {
    if (doc == null) {
      callback(null, []);
    }else{
      callback(null, doc)
    }
  })
};
function getFullResult(res1, res2, callback){
  var result = [],
    union = _.union(res1, res2)
    length = union.length;

  union.forEach(function(obj, i){
    var existing = _.where(result, {_id:obj._id})
    if(existing.length){
      for(var j = 0; j<= result.length-1; j++){
        var ID = new ObjectID(result[j]._id);
        var ID2 = new ObjectID(obj._id);
        if(ID.equals(ID2)){

            result[j].total += obj.total;
            result[j].count += obj.count;

            break;
          }
      }
      //if(i == length-1){
      //  callback(result);
      //}

    }else{
      result.push(obj);
      //if(i == length-1){
      //  callback(result);
      //}
    }
  })
  callback(result)
}

module.exports = router;
