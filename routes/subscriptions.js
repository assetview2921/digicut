/**
 * Created by abhishekgarg on 16/08/15.
 */
/**
 * Created by abhishekgarg on 21/07/15.
 */
var express = require('express'),
    router = express.Router(),
    auth = require("../config/auth"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    PayPal = require('paypal-rest-sdk');
router.get('/customer', function (req, res) {
    PayPal.configure({
        'mode': 'live',
        'schema': 'https',
        'host': 'api.paypal.com',
        'port': '',
        'openid_connect_schema': 'https',
        'openid_connect_host': 'api.paypal.com',
        'openid_connect_port': '',
        'client_id': "AWVUVSGVGCd332bgIyZ83MXFr7GoPygBbJZmLyDY-yCRCdPNx_bbTX6UOZ2gj1d4RKwzHovyPWYBmoOt",
        'client_secret': "EPrKEHr69Yz5XYpf0rv1b4K8OZn0h8mdlI1NBuSOvdYTkB8sFSiBxHUHLm9frHwiQoio8F6NaVyKiaYs",
        'authorize_url': 'https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/authorize',
        'logout_url': 'https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/endsession'
    });
    console.log(PayPal);
    var billingPlanAttributes = {
        "name": "Customer Plan",
        "description": "Customer billing plan.",
        "type": "fixed",
        "payment_definitions": [
            {
                "name": "Regular Payments",
                "type": "REGULAR",
                "frequency": "MONTH",
                "frequency_interval": "1",
                "amount": {
                    "value": "0.01",
                    "currency": "USD"
                },
                "cycles": "12",
                "charge_models": [
                    {
                        "type": "SHIPPING",
                        "amount": {
                            "value": "0",
                            "currency": "USD"
                        }
                    },
                    {
                        "type": "TAX",
                        "amount": {
                            "value": "0",
                            "currency": "USD"
                        }
                    }
                ]
            }
        ],
        "merchant_preferences": {
            "setup_fee": {
                "value": "0.02",
                "currency": "USD"
            },
            "return_url": "http://www.return.com",
            "cancel_url": "http://www.cancel.com",
            "auto_bill_amount": "YES",
            "initial_fail_amount_action": "CONTINUE",
            "max_fail_attempts": "0"
        }
    };
    PayPal.billing_plan.create(billingPlanAttributes, function (error, billingPlan) {
        if (error) {
            console.log(error);
        } else {
            console.log("Create Billing Plan Response");
            console.log(billingPlan);
        }
    });
});

module.exports = router;