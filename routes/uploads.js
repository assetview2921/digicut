/**
 * Created by abhishekgarg on 23/07/15.
 */
var express = require('express'),
    router = express.Router(),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    BarberCutPics = require("../models/barber_cutpics"),
    User = require("../models/user"),
    config = require("../config/config"),
    knox = require("knox").createClient({
        key: config.aws.key,
        secret: config.aws.secretKey,
        bucket: config.aws.bucket
    }),
    fs = require('fs'),
    multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
    formidable = require('formidable');
console.log('CONFIG   >>>>>>>>>>>>>>>>>>>>>', config)

router.get('/', auth.isLoggedIn, auth.isClient, function (req, res) {
    res.redirect("/dashboard");
});
router.get("/profile-pic", auth.isLoggedIn, auth.isClient, function (req, res) {
    if(req.query.profile === 1 ||req.query.profile === '1'){
      req.session.backUrl = '/profile/edit';
      console.log('REQ>QUERY>PROFILE', req.query.profile, req.session);
    }
    if ((req.user.profile_pic == '')||(!req.user.profile_pic)) {
        image = "/img/no_image.png";
    } else {
        image = "https://digicut-public.s3.amazonaws.com/" + req.user.profile_pic;
    }
    res.render("../public/views/users/upload-profile-pic", {
        isProfilePic: true,
        user: req.user,
        popupmessage: req.flash('popupmessage'),
        error: req.flash('error')
    });
})
router.get("/barber-cut-pic/", auth.isLoggedIn, auth.isClient, function (req, res) {
    if (typeof req.params.image == 'undefined') {
        image = "/img/no_image.png";
    } else {
        image = "https://digicut-public.s3.amazonaws.com/" + req.params.image;
    }
    res.render("../public/views/users/upload-profile-pic", {
        isProfilePic: false,
        user: req.user,
        image: image,
        main_pic:false,
        popupmessage: req.flash('popupmessage'),
        error: req.flash('error')
    });
})
router.get("/barber-cut-pic/:image/:id", auth.isLoggedIn, auth.isClient, function (req, res) {
    BarberCutPics.findOne({_id:req.params.id}, function(err, picture){
        var main_pic = false;
        if (typeof req.params.image == 'undefined') {
            image = "/img/no_image.png";
        } else {
            if(!err && picture){
                main_pic = picture.default;
            }
            image = "https://digicut-public.s3.amazonaws.com/" + req.params.image;
        }
        res.render("../public/views/users/upload-profile-pic", {
            isProfilePic: false,
            user: req.user,
            image: image,
            id: req.params.id,
            main_pic:main_pic,
            popupmessage: req.flash('popupmessage'),
            error: req.flash('error')
        });
    })
})

router.get("/process", auth.isClient, function (req, res) {
    res.redirect("/dashboard");
})

router.get("/delete/barbercutpic/:id/", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    if (typeof req.params.id == "undefined") {
        req.flash("error", true);
        req.flash("popupmessage", "Invalid picture");
        //req.user.profile_pic=name;
        backURL = req.header('Referer') || '/';
        // do your thang
        res.redirect(backURL);
        return;
    }
    BarberCutPics.find({_id: req.params.id, user_id: req.user._id}, function(err, picture){
        BarberCutPics.remove({_id: req.params.id, user_id: req.user._id}, function (err, result) {
            try {
                knox.del(picture[0].pic).on('response', function(res){
                    console.log('del!!!!',res.statusCode);
                    console.log(res.headers);
                }).end();
            } catch (ex) {
                console.log(ex);
            }
            req.flash("error", false);
            req.flash("popupmessage", "Image delete successfully");
            //req.user.profile_pic=name;
            backURL = req.header('Referer') || '/';
            // do your thang
            res.redirect("/profile");
            return;
        })
    })
})

router.post("/process-test",auth.isLoggedIn, auth.isClient, function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        res.writeHead(200, {'content-type': 'text/plain'});
        res.write('received upload:\n\n');
        res.end(util.inspect({fields: fields, files: files}));
    });

})

router.post("/process", auth.isLoggedIn, auth.isClient, function (req, res) {
    console.log('REQ> BODY >>>>>', req.body)
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if ( fields.profile_pic !== 'undefined') {
            if (files.croppedImage.name.length == 0) {
                req.flash("error", true);
                req.flash("popupmessage", "Please select picture");
                //req.user.profile_pic=name;
                backURL = req.header('Referer') || '/';
                // do your thang
                res.send(200,backURL);
                return;
            }
            try {
                var name = config.aws.folder + (new Date().getTime()) + files.croppedImage.name;
                name = name.split(" ").join("-");

                var file = files.croppedImage,
                    stream = fs.createReadStream(file.path),
                    awsResult;
                awsResult = knox.putStream(stream, name,
                    {
                        'Content-Type': file.type,
                        'x-amz-acl': 'public-read',
                        'Content-Length': file.size
                    },
                    function(err, result) {
                    }
                );
                awsResult.on('response', function(awsResponse) {
                    if (awsResponse.statusCode == 200) {
                        if (fields.isProfilePic == "true") {
                          var backUrl = req.session.backUrl||'/dashboard';
                          delete req.session['backUrl'];
                            User.findOne({_id: req.user._id}, function (err, user) {
                                var toBeDeleted = user.profile_pic;
                                //user.profile_pic = name;
                                user.profile_pic =  name;
                                user.save(function (errr) {
                                    if (!errr) {
                                        try {
                                            knox.del(toBeDeleted).on('response', function(res){
                                                console.log('del!!!!',res.statusCode);
                                                console.log(res.headers);
                                            }).end();
                                        } catch (ex) {
                                            console.log(ex);
                                        }
                                        req.flash("error", false);
                                        req.flash("popupmessage", "Profile Picture has been updated successfully");
                                        req.user.profile_pic = name;
                                        res.send(200,backUrl);
                                        return;
                                    } else {
                                        req.flash("error", true);
                                        req.flash("popupmessage", "Unable to upload profile pic");
                                        res.redirect(backUrl);
                                        return;
                                    }
                                })
                            })
                        } else {
                            if ( fields.id !== 'undefined') {
                                BarberCutPics.findOne({_id:fields.id},function(err1,barberpic){
                                    if(err1 || barberpic==null){
                                        req.flash("error", true);
                                        req.flash("popupmessage", "No picture found");
                                        res.send(200,"/profile");
                                        return;
                                    }
                                    try {
                                        knox.del(barberpic.pic).on('response', function(res){
                                            console.log('del!!!!',res.statusCode);
                                            console.log(res.headers);
                                        }).end();
                                    } catch (ex) {
                                        console.log(ex);
                                    }
                                    barberpic.pic=name;
                                    barberpic.default = fields.default;
                                    if(fields.default){
                                        BarberCutPics.update({user_id:req.user._id},{$set:{default:false}},{multi:true}).exec(function(err, result){
                                            barberpic.save(function(err2){
                                                if(err2){
                                                    req.flash("popupmessage", "Unable to update picture");
                                                    res.send(200,"/profile");
                                                    return;
                                                }
                                                req.flash("popupmessage", "Picture has been updated successfully");
                                                res.send(200,"/profile");
                                                return;
                                            })
                                        })
                                    }else{
                                        barberpic.save(function(err2){
                                            if(err2){
                                                req.flash("popupmessage", "Unable to update picture");
                                                res.send(200,"/profile");
                                                return;
                                            }
                                            req.flash("popupmessage", "Picture has been updated successfully");
                                            res.send(200,"/profile");
                                            return;
                                        })
                                    }

                                })
                            } else {
                                BarberCutPics.count({user_id: req.user._id}, function (error1, count) {
                                    if (count == 9) {
                                        req.flash("error", true);
                                        req.flash("popupmessage", "You can't upload more then 9 cut pics");
                                        res.redirect("/profile");
                                    } else {
                                        var barbercutpic = new BarberCutPics();
                                        barbercutpic.pic = name;
                                        barbercutpic.default = fields.default;
                                        barbercutpic.user_id = req.user._id;
                                        if(barbercutpic.default){
                                          BarberCutPics.update({user_id:req.user._id},{$set:{default:false}},{multi:true}).exec(function(err, result){
                                                barbercutpic.save(function (err) {
                                                    if (err) {
                                                        req.flash("error", true);
                                                        req.flash("popupmessage", "Unable to upload pic");
                                                        res.send(200,"/profile");
                                                        return
                                                    } else {
                                                        req.flash("error", false);
                                                        req.flash("popupmessage", "Image has been upload successfully");
                                                        res.send(200,"/profile");
                                                        return
                                                    }
                                                })
                                            })
                                        }else{
                                            barbercutpic.save(function (err) {
                                                if (err) {
                                                    req.flash("error", true);
                                                    req.flash("popupmessage", "Unable to upload pic");
                                                    res.send(200,"/profile");
                                                    return
                                                } else {
                                                    req.flash("error", false);
                                                    req.flash("popupmessage", "Image has been upload successfully");
                                                    res.send(200,"/profile");
                                                    return
                                                }
                                            })
                                        }

                                    }
                                })
                            }
                        }
                    } else {
                        req.flash("error", true);
                        req.flash("popupmessage", "Unable to upload profile pic");
                        res.send(200,'/dashboard');
                        return
                    }

                })
            } catch (exc) {
                console.log(exc);
            }
        }else{
            if ( fields.id !== 'undefined') {
                BarberCutPics.findOne({_id: fields.id}, function(err, picture){
                    if(fields.default){
                        BarberCutPics.update({user_id:req.user._id,_id:{$ne:fields.id}},{$set:{default:false}},{multi:true})
                            .exec(function(err, result){
                                BarberCutPics.update({_id: fields.id}, {$set:{default:fields.default}})
                                    .exec(function(err, main){
                                        req.flash("popupmessage", "Image has been updated successfully")
                                        backURL = req.header('Referer') || '/';
                                        // do your thang
                                        res.send(200,backURL);
                                        return;
                                    })
                            })
                    }else{
                        BarberCutPics.update({_id: fields.id}, {$set:{default:fields.default}})
                            .exec(function(err, main){
                                req.flash("popupmessage", "Image has been updated successfully")
                                backURL = req.header('Referer') || '/';
                                res.send(200,backURL);
                                return;
                            })
                    }
                })
            }else{
                req.flash("popupmessage", "Please select picture");
                backURL = req.header('Referer') || '/';
                res.send(200,backURL);
                return;
            }
        }
    });
})

module.exports = router;
