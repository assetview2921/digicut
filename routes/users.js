var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Subscription = require("../models/subscriptions"),
    BarberCutPic = require("../models/barber_cutpics"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config");

router.get("/alerts", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, function (req, res) {
    res.locals.title="Alerts";
    Appointment.find({review_status:false,user_id:req.user._id}).populate('barber_id').exec(function(err,appointments){
        res.render("../public/views/users/alerts", {
            user: req.user,
            appointments:appointments,
            popupmessage: req.flash('popupmessage'),
            error: req.flash('error')
        });
    })
})
router.get("/review/:appointment_id", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, subscriber.isSubscribed, function (req, res) {
    Appointment.find({
        _id: req.param.appointment_id,
        status: true
    }).populate('barber_id').exec(function (err, appointment) {
        if (err || appointment.length == 0) {
            res.redirect("/users/alerts", {
                popupmessage: 'Unauthorized access'
            });
            return;
        } else {
            res.render("../public/views/users/review", {user: req.user, appointment: appointment});
        }
    })
})
router.get('/reset-password', function (req, res) {
    res.render("../public/views/users/reset-password", {message: req.flash("message"), error: req.flash("error"),popupmessage: req.flash('popupmessage')});
});
router.get('/manual-verification', function (req, res) {
    res.render("../public/views/users/manual-verification", {message: req.flash("message"), error: req.flash("error")});
});
router.get("/joinnow", auth.isLoggedIn, auth.isClient, function (req, res) {
    res.locals.title="Join Filnu";
    res.render("../public/views/users/joinnow", {count: req.session.count, user: req.user,popupmessage:''});
})
router.get("/invitenow", auth.isLoggedIn, auth.isClient, function (req, res) {
    res.locals.title="Request Filnu";
    res.render("../public/views/users/invitenow", {user: req.user, popupmessage:''});
});

router.post("/invitenow", auth.isClient, function (req, res) {
    expressmailer.mailer.mailer.send("../public/emails/request-barbers", {
        to: config.globals.adminmain,
        subject: "New Barber Request in " + (req.body.zipcode),
        zip: req.body.zipcode,
        user: req.user,
        siteurl: config.globals.siteurl
    }, function (err) {
        console.log(err);
        if (err) {
            req.flash("error", true);
            req.flash("popupmessage", "Unable to request new barbers at the moment");
            res.send({
                error: true,
                message: "Unable to request new barbers at the moment"
            })
        } else {
            req.flash("error", false);
            req.flash("popupmessage", "Thank you for your request! We’re working hard to expand our reach and we’ll send you an email once we’re in your area");

            res.send({
                error: false,
                message: "Thank you for your request! We’re working hard to expand our reach and we’ll send you an email once we’re in your area"
            })
        }
    })
});

//verify user
router.get("/verify", function (req, res) {
    res.locals.title="Login";
    if (typeof req.query.user === 'undefined' || typeof req.query.code === 'undefined') {
        res.render("../public/views/users/login", {popupmessage: "Invalid request", 'error': true});
    } else {
        User.findOne({"_id": req.query.user, verification_code: req.query.code}, function (err, user) {
            if (err || !user) {
                res.render("../public/views/users/login/", {popupmessage: "Invalid User", 'error': true});
            } else {
                user.emailVerified = 1;
                user.active = 1;
                user.save(function (er) {
                    if (er) {
                        res.render("../public/views/users/login", {
                            popupmessage: "Unable to activate user, try again later",
                            'error': 'true'
                        });
                    } else {
                        res.render("../public/views/users/login", {
                            popupmessage: "User has been activated successfully",
                            'error': false
                        });
                    }
                });
            }
        });
    }
})
router.get('/resend_verification', function (req, res) {
    if (typeof req.session.vemail !== 'undefined') {
        User.findOne({$or: [{'username': req.session.vemail}, {'email': req.session.vemail}]}).exec(function (err, user) {
            if (user.length == 0 || err) {
                res.render("../public/views/users/login", {popupmessage: "Invalid request", 'error': true});
            } else {
                var config = require("../config/config");
//send sms notification

                //send confirmation mail
                expressmailer.mailer.mailer.send("../public/emails/verification", {
                    to: user.email,
                    subject: "User Registration",
                    data: user,
                    siteurl: config.globals.siteurl,
                    vcode: user.verification_code
                }, function (err) {
                    if (err) {
                        res.render("../public/views/users/login", {
                            popupmessage: "Invalid request",
                            'error': true
                        });
                    } else {
                        res.render("../public/views/users/login", {
                            popupmessage: "Verification mail has beens sent successfully",
                            'error': true
                        });
                    }
                })
            }
        })
    } else {
        res.render("../public/views/users/login", {popupmessage: "Invalid request", 'error': true});
    }
})

router.post("/verify", function (req, res) {
    if (typeof req.body.username === 'undefined' || typeof req.body.verification_code === 'undefined') {
        res.render("../public/views/users/login", {popupmessage: "Invalid request", 'error': true});
    } else {
        User.findOne({
            "username": req.body.username,
            verification_code: req.body.verification_code
        }, function (err, user) {
            if (err || !user) {
                res.render("../public/views/users/login/", {popupmessage: "Invalid User", 'error': true});
            } else {
                user.emailVerified = 1;
                user.save(function (er) {
                    if (er) {
                        res.render("../public/views/users/login", {
                            popupmessage: "Unable to activate user, try again later",
                            'error': 'true'
                        });
                    } else {
                        res.render("../public/views/users/login", {
                            popupmessage: "User has been activated successfully",
                            'error': false
                        });
                    }
                });
            }
        });
    }
})
//confirmupdate

router.get("/confirmupdate", function (req, res) {
    if (typeof req.query.user === 'undefined') {
        res.render(__dirname + "/public/views/users/login", {popupmessage: "Invalid request", 'error': true});
    } else {
        User.findOne({"_id": req.query.user}, function (err, user) {
            if (err || !user) {
                res.render("../public/views/users/login", {popupmessage: "Invalid User", 'error': true});
            } else {
                user.isTemporaryEmail = 1;
                user.isTemporaryPassword = 1;
                user.isTemporaryUsername = 1;
                user.save(function (er) {
                    if (er) {
                        res.render("../public/views/users/login", {
                            popupmessage: "Unable to confirm update, try again later",
                            'error': 'true'
                        });
                    } else {
                        res.render("../public/views/users/login", {
                            popupmessage: "User has been updated successfully",
                            'error': false
                        });
                    }
                });
            }
        });
    }
})


router.post("/unique", function (req, res) {
  if(req.body.username){
    if(!/^[a-zA-Z0-9_]{3,14}$/.test(req.body.username)){
      res.send({error: false});
    }else{
      User.findOne({username:new RegExp('^'+req.body.username+'$', 'i')}, function (err, user) {
        var error = {};
        if (err) {
          error = err;
        } else if (user) {
          error = true;
        } else {
          error = false;
        }
        res.send({error: error});
      });
    }
  }else {
    User.findOne(req.body, function (err, user) {
      var error = {};
      if (err) {
        error = err;
      } else if (user) {
        error = true;
      } else {
        error = false;
      }
      res.send({error: error});
    });
  }
});

router.get("/change-password", function (req, res) {
    if (typeof req.query.user === 'undefined') {
        res.render(__dirname + "/public/views/users/login", {popupmessage: "Invalid request", 'error': true});
    } else {
        User.findOne({"_id": req.query.user, isTemporaryPassword: true}, function (err, user) {
            if (err || !user) {
                res.render("../public/views/users/login", {popupmessage: "Invalid User", 'error': true});
            } else {
                res.render("../public/views/users/change-password-unauth", {
                    username: user.username,
                    _id: req.query.user,
                    error: req.flash('error'),
                    popupmessage: req.flash('popupmessage')
                });
            }
        });
    }
})

router.post("/change-password", function (req, res) {
    var usr = new User();
    if (req.body.password !== req.body.confirm_password) {
        req.flash('error', true);
        req.flash('popupmessage', "Both Password must match");
        res.redirect("/users/change-password/?user=" + req.body._id);
        return;
    }
    User.findOne({'username': req.body.username}, function (err, user) {
        var error = {};
        if (err) {
            error = err;
            popupmessage = "Unable to find user";
        } else if (user) {
            user.password = usr.generateHash(req.body.password);
            user.isTemporaryPassword = false;
            user.save();
            error = false;
            popupmessage = "Password has been updated sucessfully";
        } else {
            popupmessage = "Unable to save new password";
            error = true;
        }
        req.flash('error', error);
        req.flash('popupmessage', popupmessage);
        res.redirect("/login");
    });
})


router.post("/reset-password", function (req, res) {
    User.findOne({'email': req.body.email}, function (err, user) {
        var error = {};
        if (err) {
            error = err;
        } else if (user) {
            error = false;

            user.isTemporaryPassword = true;
            user.save(function(err, user){
              console.log('RESET_ PASSWORD >>>> SAVE', err, user);
              expressmailer.mailer.mailer.send("../public/emails/change-password", {
                to: user.email,
                subject: "Forgot Password",
                data: user,
                siteurl: config.globals.siteurl
              }, function (err) {
                console.log(err);
                if (err) {
                  req.flash('error', true);
                } else {
                  req.flash('error', false);

                  req.flash("popupmessage", "Forgot password email sent successfully!");
                }
              })
            })
        } else {
            error = true;
        }
        if (error == true)
            res.render("../public/views/users/reset-password", {
                error: error,
                'popupmessage': 'User with this email address doesn\'t exist'
            });
        else
            res.render("../public/views/users/login", {
                error: error,
                'popupmessage': 'Password Reset mail has been sent to your email account'
            });
    });
})

router.get("/resend-verification-email", function (req, res) {
    res.render("../public/views/users/resend-verification-mail");
})


router.post("/resend-verification-email", function (req, res) {
    User.findOne({$or: [{'username': req.body.username}, {'email': req.body.username}]}, function (err, user) {
        var error = {};
        if (err) {
            error = err;
        } else if (user) {
            error = false;
            expressmailer.mailer.mailer.send("../public/emails/re-verify", {
                to: user.email,
                subject: "User Re-Verification",
                data: user,
                siteurl: config.globals.siteurl
            }, function (err) {
                console.log(err);
                if (err) {
                    req.flash('error', true);
                } else {
                    req.flash('error', false);
                    req.flash("popupmessage", "Verification email sent successfully!");
                }
            })
        } else {
            error = true;
        }
        if (error == true)
            res.render("../public/views/users/resend-verification-mail", {
                error: error,
                'popupmessage': 'Unable to send verification email'
            });
        else
            res.render("../public/views/users/login", {
                error: error,
                'popupmessage': 'Verification mail sent successfully'
            });
    });
})

module.exports = router;
