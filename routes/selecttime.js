/**
 * Created by abhishekgarg on 24/07/15.
 */
var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    mongoose = require('mongoose'),
    moment = require('moment');

router.get("/",auth.isLoggedIn, auth.isClient, function (req, res) {
    req.flash('error', true);
    req.flash('popupmessage', 'Invalid request');
    res.redirect("/dashboard");
})

router.get("/:username",auth.isLoggedInToBook, auth.isClient, subscriber.isCustomer,subscriber.isSubscribed, subscriber.checkSubscription, function (req, res) {
    res.locals.title='Select Date';
  if(req.notSubscribed){
    if(req.query.service){
      res.render("../public/views/users/calendar", {
        username: req.params.username,
        service:req.query.service,
        error: req.flash('error'),
        popupmessage: req.flash('popupmessage')
      });
    }else{
      return res.redirect('/services/select/'+req.params.username);
    }

  }else{
    var subscriptionStartDate = moment(new Date(req.session.subscription.start)),
      //subscriptionEndDate = moment(new Date(req.session.subscription.start)).add(1, 'month'),
      subscriptionEndDate = moment(new Date(req.session.subscription.created))
        .add(Math.ceil(moment().diff(moment(new Date(req.session.subscription.created)), 'months', true)), 'month'),
      val = mongoose.Types.ObjectId(req.user._id);
    Appointment.aggregate([
      {
        $match: {
          user_id: val,
          created: {$gte: subscriptionStartDate.toDate(), $lte: subscriptionEndDate.toDate()},
          confirmation_status: true
        }
      },
      {
        $group: {
          '_id': '$status',
          total: {$sum: 1}
        }
      }
    ]).exec(function (err, docs) {
      console.log("---Aggregate---");
      console.log(docs, docs[0])
      var errorFlag = false,
        errorMessage = '',
        total_cuts = req.user.cuts||4;
      if (docs.length > 0) {

        if((typeof docs[0] != 'undefined')&&(typeof docs[1] != 'undefined')){
          if( (docs[0].total + docs[1].total) == total_cuts ){
            errorFlag = true;
            errorMessage = "You have no transactions remaining this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD')+". Remember that you can cancel your pending appointment (go to your profile and click on alerts) and book a new one now";
          }
        }else{
          if (typeof docs[0] != 'undefined') {
            if (docs[0]._id == true && docs[0].total == total_cuts) {
              errorFlag = true;
              errorMessage = "You have used your "+total_cuts+" transactions this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD');
            }
            if (docs[0]._id == false && docs[0].total == total_cuts) {
              errorFlag = true;
              errorMessage = "You have no transactions remaining this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD')+". Remember that you can cancel your pending appointment (go to your profile and click on alerts) and book a new one now";
            }
          }
          if (typeof docs[1] != 'undefined') {
            if (docs[0]._id == true && docs[1].total == total_cuts) {
              errorFlag = true;
              errorMessage = "You have used your "+total_cuts+" transactions this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD');
            }
            if (docs[0]._id == false && docs[1].total == total_cuts) {
              errorFlag = true;
              errorMessage = "You have no transactions remaining this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD')+". Remember that you can cancel your pending appointment (go to your profile and click on alerts) and book a new one now";
            }
          }
        }

      }
      if (errorFlag == true) {
        req.flash('error', true);
        req.flash('popupmessage', errorMessage);
        res.redirect("/dashboard");
        return;
      } else {
        User.findOne({username:req.params.username, emailVerified : 1,
          active : 1}, function(err, barber){
          if(!err && barber){
            return res.render("../public/views/users/calendar", {
              username: req.params.username,
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage')
            });
          }else{
            res.locals.title='Find A Barber';
            req.flash('error', true);
            req.flash('popupmessage', 'Unable to locate barber');
            return res.redirect("/barber/search");

          }
        })

      }
    })
  }

})

module.exports = router;
