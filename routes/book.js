/**
 * Created by abhishekgarg on 21/07/15.
 */
var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Action = require("../models/action"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    moment = require('moment'),
    mongoose = require('mongoose'),
    formidable = require('formidable'),
    _ = require('lodash'),
    days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'],
    PayPal = require('paypal-rest-sdk');
PayPal.configure({
  'mode': config.paypal.mode, //sandbox or live
  'client_id': config.paypal.client_id,
  'client_secret': config.paypal.client_secret
});

router.get("/", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.isCustomer, function (req, res) {
    req.flash('error', true);
    req.flash('popupmessage', 'Unable to locate barber');
    res.redirect("/barber/search");
})
router.get("/:time/:barber", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.isCustomer, subscriber.checkSubscription, function (req, res) {
    res.locals.title = 'Select Time';
    if (typeof req.params.barber !== 'undefined') {
        User.find({username: req.params.barber, "barber.working_hours": 1, active:1}, {barber: 1}, function (err, doc) {
            if (err || (!doc.length)) {
                req.flash('error', true);
                req.flash('popupmessage', 'Unable to locate barber');
                return res.redirect("/barber/search");
            }
            res.locals.time = (new Date(req.params.time)).getTime();
            var day = moment(req.params.time, "YYYY-MM-DD");
            var time =day.toDate().getTime();
          if(req.query.service){
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            res.render("../public/views/users/barbers/booking_pay_as_you_go", {
              user: req.user,
              moment: moment,
              barber: req.params.barber,
              time:time,
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage')
            });
          }else{
            res.render("../public/views/users/barbers/booking", {
              user: req.user,
              moment: moment,
              barber: req.params.barber,
              time:time,
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage')
            });
          }

        })
    }

})
router.post('/getAvailability/:barber/:time', auth.isLoggedIn, auth.isClient, function (req, res) {
    var day = moment(Number(req.params.time)),
        tomorrow = moment(day).add(1, 'days');
    console.log(day.toDate(), tomorrow);
    User.findOne({username: req.params.barber, active:1}, {}, function (err, barber) {
        Availability.findOne({"user_id": barber._id}, function (err1, ava) {
            Appointment.find({
                date: {$gte: day.toDate(), $lt: tomorrow.toDate()},
                barber_id: barber._id,
                client_cancel:{$ne:true}
            }, {date: 1, user_id:1, service:1, status:1, confirmation_status:1, client_name:1, manually:1}, function (error, appointments) {
                console.log(appointments);
                res.send({
                    error: err1,
                    availability: ava,
                    appointments: appointments
                });
            }).populate('user_id')
        })
    })
})
router.post("/", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.checkSubscription, getReqBody, function (req, res) {
    console.log("Booking Post");
    if (typeof req.body.barber !== 'undefined') {
        if(req.user.type == 0) {
            User.findOne({username: req.body.barber, emailVerified : 1,
            active : 1}, function (err, doc) {
                if (doc == null) {
                    req.flash('error', true);
                    req.flash('popupmessage', 'Unable to locate barber');
                    res.send(200,"/barber/search");
                    return;
                }
                console.log("Time is: ", req.body.datetime);
                var selectedTime = moment(new Date(req.body.datetime));
                console.log("Selected moment time is: ", selectedTime);
                var current = moment();
                //check if user is trying to book in back date/time
                if (selectedTime.unix() < current.unix()) {
                    req.flash('error', true);
                    req.flash('popupmessage', "You can't book in earlier dates");
                    res.send(200,"/select-date/" + req.body.barber);
                    return;
                }
                var day = selectedTime.format("dddd").substr(0, 3).toLowerCase(),
                    hour = selectedTime.format("H") - 9;
                    query = {};
                query["availability.0." + day + "." + hour] = "true";
                query['user_id'] = doc._id;
                console.log("Selected time and query is given below");
                console.log(query, selectedTime.toDate());
                Availability.findOne(query, function (err, ava) {
                    if (ava == null) {
                        console.log("You selected " + hour + " for " + day + "\nSystem date time is " + new Date().toString() + " UTC time is " + current.toDate());
                        req.flash('error', true);
                        req.flash('popupmessage', "oop's barber is not available at that time");
                        res.send(200,"/select-date/" + req.body.barber);
                        return;
                    }
                  if(req.notSubscribed){
                    bookingPayAsYouGo(req, res, doc, selectedTime);
                  }else{
                    bookingWithSubscribtion(req, res, doc, selectedTime);
                  }

                })
            })
        }else{
            manuallyBookService(req, res);
        }
    }
})
function getReqBody(req, res, next){
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        req.body = fields;
        next();
    })
}
function manuallyBookService(req, res){
    if(req.user.username == req.body.barber){
        if(req.body.client_name !== ''){
            User.findOne({username: req.body.barber, "barber.working_hours":true, emailVerified : 1,
              active : 1}, function (err, doc) {
                if (doc == null) {
                    req.flash('error', true);
                    req.flash('popupmessage', 'Unable to locate barber');
                    res.send(200,"/barber/calendar");
                    return;
                }
                //var tz = require('moment-timezone');
                console.log("Time is: ", req.body.datetime);
                var selectedTime = moment(new Date(req.body.datetime)),
                    current = moment();
                console.log("Selected moment time is: ", selectedTime);

                //check if user is trying to book in back date/time
                if (selectedTime.unix() < current.unix()) {
                    req.flash('error', true);
                    req.flash('popupmessage', "You can't book in earlier dates");
                    res.send(200,"/barber/calendar");
                    return;
                }
                var day = selectedTime.format("dddd").substr(0, 3).toLowerCase(),
                    hour = selectedTime.format("H") - 9,
                    query = {};
                query["availability.0." + day + "." + hour] = "true";
                query['user_id'] = doc._id;
                console.log("Selected time and query is given below");
                console.log(query, selectedTime.toDate());
                Availability.findOne(query, function (err, ava) {
                    //console.log(ava.availability[0],days[selectedTime.day()],selectedTime.day())
                    //var days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
                    if (ava == null) {
                        console.log("You selected " + hour + " for " + day + "\nSystem date time is " + new Date().toString() + " UTC time is " + current.toDate());
                        req.flash('error', true);
                        req.flash('popupmessage', "oop's you are not available at that time");
                        res.send(200,"/barber/calendar");
                        return;
                    }
                    var u = new User(),
                        newApp = new Appointment();
                    newApp.user_id = req.user._id;
                    newApp.barber_id = doc._id;
                    newApp.service = req.body.service;
                    newApp.date = req.body.datetime;
                    newApp.manually = true;
                    newApp.client_name = req.body.client_name;
                    newApp.confirmation_code = u.generatePassword(6);
                    newApp.save(function (err, appo) {
                        console.log(err);
                        if (err == null) {
                            User.findOne({username: req.body.barber}, {
                                phone: 1,
                                email: 1,
                                aka: 1,
                                first: 1,
                                last: 1,
                                barber: 1,
                                'address.details': 1,
                                'username': 1
                            }, function (e, barber) {
                                req.flash('error', false);
                                req.flash('popupmessage', "New appointment created");
                                res.send(200,"/appointments/" + appo._id);
                                return;
                             })
                        }
                    });
                })
            })
        }else{
            req.flash('error', true);
            req.flash('popupmessage', "You didn't enter client name");
            // if they aren't redirect them to the home page
            res.send(200,'/barber/calendar');
        }
    }else{
        req.flash('error', true);
        req.flash('popupmessage', "You don't have access to this area");
        // if they aren't redirect them to the home page
        res.send(200,'/dashboard');
    }
}

function bookingPayAsYouGo(req, res, doc, selectedTime) {
  //User.findOne({username: req.body.barber}, function (e, barber) {
  Action.findOne({_id: req.body.action, active:true}, function(err, action){
    Appointment.findOne({action:req.body.action, user_id: req.user._id, confirmation_status:true}, function(err, app){
      var barber = doc,
        standard = ((req.body.service.toUpperCase()=="CUT"||req.body.service.toUpperCase()=="LINE-UP"||req.body.service.toUpperCase()=="SHAVE")&&(!app));
      //if(barber.service){
      //  if(barber.service.length){
      var service = _.where(doc.services, {service: req.body.service.toUpperCase()})[0];
      if (!service) {
        req.flash('error', false);
        req.flash('popupmessage', '"' + req.body.service.toUpperCase() + '" is not available any more, please choose another');
        return res.redirect('/services/select/' + req.body.barber);
      }else{

        var price = service.price;
        var u = new User(),
          newApp = new Appointment();
        newApp.user_id = req.user._id;
        newApp.barber_id = doc._id;
        newApp.service = req.body.service;
        newApp.date = req.body.datetime;
        if(standard && action){
          newApp.action = action._id;
        }
        newApp.confirmation_code = u.generatePassword(6);
        newApp.price = (standard && action)?action.price:parseInt(price);
        newApp.save(function (err, appo) {
          var total = (standard && action)?action.price.toString():(price + 2).toString();
          var options = createPaymentOptions(req.body, total);
          PayPal.payment.create(options, function (err, data) {
            console.log('payment create >', err, data);
            console.log(err);
            if ((err == null) && (data.state == 'approved')) {
              // Your accountSid and authToken from twilio.com/user/account
              //notify to barber
              expressmailer.mailer.mailer.send("../public/emails/barber_appointment", {
                to: doc.email,
                subject: "New appointment",
                user: barber,
                data: req.user,
                service: req.body.service,
                siteurl: config.globals.siteurl,
                date: moment(new Date(selectedTime).toISOString()).format('ddd MMM DD YYYY hh:mm A')
              }, function (err) {
              });
              //notify not admin

              //notify to customer
              expressmailer.mailer.mailer.send("../public/emails/customer_appointment", {
                to: req.user.email,
                subject: "Appointment confirmation",
                data: barber,
                date: moment(new Date(selectedTime).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
                user: req.user,
                code: newApp.confirmation_code,
                siteurl: config.globals.siteurl
              }, function (err) {
                console.log(err);
              });

              var accountSid = config.twillio.accountID,
                authToken = config.twillio.authToken,
                client = require('twilio')(accountSid, authToken),
              //notify to barber on sms
                temp1 = barber.phone.split(/[()\s\t-]/);
              barber.phone = temp1.join("");
              client.messages.create({
                body: "Dear " + barber.username + ", you have received a new appointment from " + req.user.username + " at " + moment(new Date(selectedTime.toISOString())).format('ddd MMM DD YYYY hh:mm A') + " for " + req.body.service + ". Please go to your profile and under alerts, enter the code that the client will give you to complete the transaction (in the same screen where you enter the code, you can also click \"cancel\" if you cannot cut at this time).",
                to: "+1" + (barber.phone),
                from: config.twillio.number
              }, function (err, message) {
                console.log(err, message);
                req.flash('error', false);
                req.flash('popupmessage', "Your appointment confirmation has been sent to barber for approval/rejection");
                res.send(200, "/appointments/" + appo._id);
                return;
              });
            } else {
              Appointment.remove({_id: appo._id}, function (error, data) {
                if (!error) {
                  console.log('APPOINTMENT REMOVE');
                }
                return res.send(400, 'Invalid credit card information. Please check it and try again later')
              })
            }
          })
        });
      }
    })
  })

}

function bookingWithSubscribtion(req, res, doc, selectedTime){

  var subscriptionStartDate = moment(new Date(req.session.subscription.start)),
    subscriptionEndDate = moment(new Date(req.session.subscription.start)).add(1, 'month'),
    val = mongoose.Types.ObjectId(req.user._id);
  Appointment.aggregate([
    {
      $match: {
        user_id: val,
        created: {$gte: subscriptionStartDate.toDate(), $lte: subscriptionEndDate.toDate()},
        confirmation_status: true
      }
    },
    {
      $group: {
        '_id': '$status',
        total: {$sum: 1}
      }
    }
  ]).exec(function (err, docs) {
    console.log("---Aggregate---");
    console.log(docs, docs[0])
    var errorFlag = false,
      errorMessage = '',
      total_cuts = req.user.cuts||4;
    if (docs.length > 0) {
    //  if (typeof docs[0] != 'undefined') {
    //    if (docs[0]._id == true && docs[0].total == total_cuts) {
    //      errorFlag = true;
    //      errorMessage = "You have used your "+total_cuts+" transactions this cycle. You will get "+total_cuts+" more in the next monthly cycle."
    //    }
    //    else if (docs[0]._id == false && docs[0].total > 0) {// if summ == total cuts && )(docs[0,1]._id = false => > 0)
    //      errorFlag = true;
    //      errorMessage = "You must complete pending transaction before booking new appointment"
    //    }
    //  }
    //  if (typeof docs[1] != 'undefined') {
    //    if (docs[1]._id == true && docs[1].total == total_cuts) {
    //      errorFlag = true;
    //      errorMessage = "You have used your "+total_cuts+" transactions this cycle. You will get 4 more in the next monthly cycle."
    //    }
    //    if (docs[1]._id == false && docs[1].total > 0) {
    //      errorFlag = true;
    //      errorMessage = "You must complete pending transaction before booking new appointment"
    //    }
    //  }


      if((typeof docs[0] != 'undefined')&&(typeof docs[1] != 'undefined')){
        if( (docs[0].total + docs[1].total) == total_cuts ){
          errorFlag = true;
          errorMessage = "You have no transactions remaining this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD')+". Remember that you can cancel your pending appointment (go to your profile and click on history) and book a new one now";
        }
      }else{
        if (typeof docs[0] != 'undefined') {
          if (docs[0]._id == true && docs[0].total == total_cuts) {
            errorFlag = true;
            errorMessage = "You have used your "+total_cuts+" transactions this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD');
          }
          if (docs[0]._id == false && docs[0].total == total_cuts) {
            errorFlag = true;
            errorMessage = "You have no transactions remaining this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD')+". Remember that you can cancel your pending appointment (go to your profile and click on history) and book a new one now";
          }
        }
        if (typeof docs[1] != 'undefined') {
          if (docs[0]._id == true && docs[1].total == total_cuts) {
            errorFlag = true;
            errorMessage = "You have used your "+total_cuts+" transactions this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD');
          }
          if (docs[0]._id == false && docs[1].total == total_cuts) {
            errorFlag = true;
            errorMessage = "You have no transactions remaining this cycle, your new cycle begins on "+subscriptionEndDate.format('MMM DD')+". Remember that you can cancel your pending appointment (go to your profile and click on alerts) and book a new one now";
          }
        }
      }


    }

    if (errorFlag == true) {
      req.flash('error', true);
      req.flash('popupmessage', errorMessage);
      res.send(200,"/dashboard");
      return;
    } else {
      var u = new User(),
        newApp = new Appointment();
      newApp.user_id = req.user._id;
      newApp.barber_id = doc._id;
      newApp.service = req.body.service;
      newApp.date = req.body.datetime;
      newApp.confirmation_code = u.generatePassword(6);
      newApp.save(function (err, appo) {
        console.log(err);
        if (err == null) {
          User.findOne({username: req.body.barber}, {
            phone: 1,
            email: 1,
            aka: 1,
            first: 1,
            last: 1,
            barber: 1,
            'address.details': 1,
            'username': 1
          }, function (e, barber) {

            // Your accountSid and authToken from twilio.com/user/account
            //notify to barber
            expressmailer.mailer.mailer.send("../public/emails/barber_appointment", {
              to: doc.email,
              subject: "New appointment",
              user: barber,
              data: req.user,
              service: req.body.service,
              siteurl: config.globals.siteurl,
              date: moment(new Date(selectedTime).toISOString()).format('ddd MMM DD YYYY hh:mm A')
            }, function (err) {
            });
            //notify not admin

            //notify to customer
            expressmailer.mailer.mailer.send("../public/emails/customer_appointment", {
              to: req.user.email,
              subject: "Appointment confirmation",
              data: barber,
              date: moment(new Date(selectedTime).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
              user: req.user,
              code: newApp.confirmation_code,
              siteurl: config.globals.siteurl
            }, function (err) {
              console.log(err);
            });

            var accountSid = config.twillio.accountID,
              authToken = config.twillio.authToken,
              client = require('twilio')(accountSid, authToken),
            //notify to barber on sms
              temp1 = barber.phone.split(/[()\s\t-]/);
            barber.phone = temp1.join("");
            client.messages.create({
              body: "Dear " + barber.username + ", you have received a new appointment from " + req.user.username + " at " + moment(new Date(selectedTime.toISOString())).format('ddd MMM DD YYYY hh:mm A') + " for " + req.body.service + ". Please go to your profile and under alerts, enter the code that the client will give you to complete the transaction (in the same screen where you enter the code, you can also click \"cancel\" if you cannot cut at this time).",
              to: "+1" + (barber.phone),
              from: config.twillio.number
            }, function (err, message) {
              console.log(err, message);
              req.flash('error', false);
              req.flash('popupmessage', "Your appointment confirmation has been sent to barber for approval/rejection");
              res.send(200,"/appointments/" + appo._id);
              return;
            });
          })
        }
      });
    }
  });
}

function createPaymentOptions(card, total){
  return {
    "intent":"sale",
    "payer":{
    "payment_method":"credit_card",
    "funding_instruments":[
        {
          "credit_card":{
            "number": card.number,
            "type": card.type,
            "expire_month": card.expire_month,
            "expire_year": card.expire_year,
            "cvv2": card.cvv2,
            "first_name": card.first_name,
            "last_name": card.last_name
          }
        }
      ]
    },
    "transactions":[
      {
        "amount":{
          "total": total,
          "currency":"USD",
          "details":{
            "subtotal": total
          }
        },
        "description":"Booking appointment 'Pay as you go'."
      }
    ]
  };
}

module.exports = router;
