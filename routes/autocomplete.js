/**
 * Created by abhishekgarg on 02/08/15.
 */
var express = require('express'),
  router = express.Router(),
  User = require('../models/user'),
  Availability = require("../models/availability"),
  Appointment = require("../models/appointments"),
  Reviews = require("../models/reviews"),
  auth = require("../config/auth"),
  subscriber = require("../config/subscription"),
  moment = require('moment'),
  expressmailer = require('../config/expressmailer'),
  mongoose = require('mongoose'),
  async = require('async'),
  _ = require('lodash'),
  mongoose = require('mongoose'),
  ObjectID = mongoose.Types.ObjectId,
  config = require("../config/config")
  accountSid = config.twillio.accountID,
  authToken = config.twillio.authToken,
  twillio_client = require('twilio')(accountSid, authToken);

router.get("/", function (req, res) {
  Appointment.find({confirmation_status:true, status:false,manually:false,date:{$lte:moment().add( -3,'hours')}}, function(err, appointments){
    if(!err){
      async.eachSeries(appointments, function iterator(app, main_callback) {
        app.confirmation_status = false;
        app.status = false;
        app.save(function(err, saved_app){
          async.parallel({
            client:function(callback){
console.log('client')
              //notify to barber on sms
                temp1 = app.user_id.phone.split(/[()\s\t-]/);
              app.user_id.phone = temp1.join("");
              twillio_client.messages.create({
                body: "Dear " + app.user_id.username + ", your "+ moment(new Date(app.date).toISOString()).format(' hh:mm A')+" appointment for "
                  + moment(new Date(app.date).toISOString()).format('dddd DD') + " was cancelled because it went unfulfilled. \n"
                  +"You can go to filnu.com and schedule a new appointment. Sorry for the inconvenience.\n\nTeam FILNU LLC.",
                to: "+1" + (app.user_id.phone),
                from: config.twillio.number
              }, function (err, message) {
                console.log('AUTOCOMPLETE ROUTE SMS to client:',err, message);
                sendEmail("appointment_autocanceled", app.user_id.email, app.user_id, app, function(){callback();});
              });
            },
            barber:function(callback){console.log('barber')

              sendEmail("appointment_autocanceled", app.barber_id.email, app.barber_id, app, function(){callback();});
            },
            admin:function(callback){
              console.log('admin')
              sendEmail("admin_appointment_autocanceled", config.globals.adminmain, null, app, function(){callback();});
            }
          }, function(err, results) {
            console.log('parallel result')
              // results is now equals to: {one: 1, two: 2}
            main_callback();
          });
        });
      }, function done() {
        console.log('each series callback')
          return res.send(200);
      });
    }else{
      console.log('AUTOCOMPLETE ROUTE ERROR', err)
      return res.send(400);
    }
  })
    .populate('barber_id', {'email': true, 'username': true, 'first': true, 'last': true, 'phone':true, address:true})
    .populate('user_id', {'email': true, 'username': true, 'first': true, 'last': true, 'phone':true})
    .populate('action');
})

function sendEmail(template, reciver, user, app, callback){
  expressmailer.mailer.mailer.send("../public/emails/"+template, {
    to: reciver,
    subject: "Unfulfilled appointment",
    user: user,
    siteurl: config.globals.siteurl,
    customer: app.user_id,
    barber: app.barber_id,
    price: app.price,
    campaign:app.action,
    time: moment(new Date(app.date).toISOString()).format(' hh:mm A'),
    date: moment(new Date(app.date).toISOString()).format('dddd DD'),
    date: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
    datetime: moment(new Date(app.date).toISOString()).format('ddd MMM DD YYYY hh:mm A'),
    client_name: app.client_name,
    client_cancel: app.client_cancel
  }, function (err) {
    console.log('\n =>', template, reciver,err )
    callback();
  });
}
module.exports = router;
