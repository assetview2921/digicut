
var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config");
router.get('/:barber_id', function (req, res) {
    res.locals.title = 'Reviews'
  if(req.isAuthenticated()){
    if(req.user.admin === true){
      getReviews(req, res);
    } else {
      if (req.user.type == 1) {
        Reviews.find({barber_id: req.user._id}).populate('user_id', {
          profile_pic: 1,
          username: 1,
          fbid: 1
        }).exec(function (err, reviews) {
          res.render("../public/views/users/barbers/reviews_list", {
            //user: req.user,
            reviews: reviews,
            popupmessage: req.flash('popupmessage'),
            error: req.flash('error'),
            backUrl: '/profile'
          });
        })
      } else if (req.user.type == 0) {
        getReviews(req, res);
      }
    }
  }else{
    getReviews(req, res);
  }

});

function getReviews(req, res){
  Reviews.find({barber_id:req.params.barber_id}).populate('user_id',{profile_pic:1,username:1, fbid:1}).exec(function(err,reviews){
    User.findOne({_id:req.params.barber_id}, function(err, user){
      res.render("../public/views/users/reviews_list", {
        //user: req.user,
        reviews: reviews,
        popupmessage: req.flash('popupmessage'),
        error: req.flash('error'),
        backUrl:'/barber/'+user.username
      });
    })
  })
}
module.exports = router;
