/**
 * Created by abhishekgarg on 23/07/15.
 */
var express = require('express'),
    router = express.Router(),
    auth = require("../config/auth");

router.get("/:barber/:image",  function (req, res) {
    res.locals.title='Barber Profile Image';
    if (typeof req.params.image == 'undefined') {
        image = "/img/no_image.png";
    } else {
        image = "https://digicut-public.s3.amazonaws.com/" + req.params.image;
    }
    req.flash('popupmessage', '')
    res.render("../public/views/users/viewImage", {
        popupmessage:'',
        imgSrc: image,
        backUrl: "barber/"+req.params.barber
    });
})

module.exports = router;
