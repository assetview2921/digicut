/**
 * Created by abhishekgarg on 21/07/15.
 */
var express = require('express'),
    router = express.Router(),
    auth = require("../config/auth"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config");

router.get('/',auth.isLoggedIn, function (req, res) {
    if(req.user.admin == true){
        res.redirect('/admin');
    } else {
        res.render("../public/views/users/dashboard", {user: req.user, error: false,popupmessage:req.flash('popupmessage')});
    }


});

module.exports = router;