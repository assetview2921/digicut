
var express = require('express'),
  router = express.Router(),
  User = require('../models/user'),
  Availability = require("../models/availability"),
  Appointment = require("../models/appointments"),
  Reviews = require("../models/reviews"),
  Action = require('../models/action'),
  async = require('async'),
  auth = require("../config/auth"),
  subscriber = require("../config/subscription"),
  _ = require('lodash'),
  expressmailer = require('../config/expressmailer'),
  config = require("../config/config"),
  mongoose = require('mongoose'),
  moment = require('moment');

//var action = new Action({
  //active: true,
  //description:"Redeem your $5 cut",
  //button:'Redeem your $5 cut',
  //price:10,
  //type:'pay as you go'
//})
//action.save();




router.get("/:time/:barber", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.isCustomer, subscriber.checkSubscription, function (req, res) {
  res.locals.title = 'Select Time';
  if (typeof req.params.barber !== 'undefined') {
    User.find({username: req.params.barber, "barber.working_hours": 1}, {barber: 1, services:1}, function (err, doc) {
      if (err || (!doc.length)) {
        req.flash('error', true);
        req.flash('popupmessage', 'Unable to locate barber');
        return res.redirect("/barber/search");
      }
      res.locals.time = (new Date(req.params.time)).getTime();
      var day = moment(req.params.time, "YYYY-MM-DD");
      var time =day.toDate().getTime();
      if(req.query.service){
        var service =  _.where(doc[0].services, {service: req.query.service.toUpperCase()})[0];
        if(!service){
          req.flash('error', true);
          req.flash('popupmessage', '"'+req.query.service.toUpperCase()+'" is not available any more, please choose another');
          return res.redirect('/services/select/'+req.params.barber);
        }
        res.render("../public/views/users/barbers/booking_pay_as_you_go", {
          user: req.user,
          moment: moment,
          barber: req.params.barber,
          service:service,
          time:time,
          error: req.flash('error'),
          popupmessage: req.flash('popupmessage')
        });
      }else{
        res.render("../public/views/users/barbers/booking", {
          user: req.user,
          moment: moment,
          barber: req.params.barber,
          time:time,
          error: req.flash('error'),
          popupmessage: req.flash('popupmessage')
        });
      }

    })
  }

})

router.get("/info/:time/:barber", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.isCustomer, subscriber.checkSubscription, function (req, res) {
  res.locals.title = 'Order Information';
  if (typeof req.params.barber !== 'undefined') {
    User.findOne({username: req.params.barber}, function (err, doc) {
      if (doc == null) {
        req.flash('error', true);
        req.flash('popupmessage', 'Unable to locate barber');
        res.send(200,"/barber/search");
        return;
      }
      console.log("Time is: ", req.query.time);
      var selectedTime = moment(new Date(req.query.time));
      console.log("Selected moment time is: ", selectedTime);
      var current = moment();
      //check if user is trying to book in back date/time
      if (selectedTime.unix() < current.unix()) {
        req.flash('error', true);
        req.flash('popupmessage', "You can't book in earlier dates");
        res.send(200,"/select-date/" + req.body.barber);
        return;
      }
      var day = selectedTime.format("dddd").substr(0, 3).toLowerCase(),
        hour = selectedTime.format("H") - 9;
      query = {};
      query["availability.0." + day + "." + hour] = "true";
      query['user_id'] = doc._id;
      console.log("Selected time and query is given below");
      console.log(query, selectedTime.toDate());
      Availability.findOne(query, function (err, ava) {
        if (ava == null) {
          console.log("You selected " + hour + " for " + day + "\nSystem date time is " + new Date().toString() + " UTC time is " + current.toDate());
          req.flash('error', true);
          req.flash('popupmessage', "oop's barber is not available at that time");
          res.send(200,"/select-date/" + req.body.barber);
          return;
        }
        res.locals.time = (new Date(req.params.time)).getTime();
        var day = moment(req.params.time, "YYYY-MM-DD");
        var time =day.toDate().getTime();
        if(req.query.service && req.notSubscribed){
          var service =  _.where(doc.services, {service: req.query.service.toUpperCase()})[0];
          //if(!service){this service doesn't available more -> choose another please}
          if(!service){
            req.flash('error', false);
            req.flash('popupmessage', '"'+req.query.service.toUpperCase()+'" is not available any more, please choose another');
            return res.redirect('/services/select/'+req.params.barber);
          }
          getActions('pay as you go', req.user._id, function(actions){
            res.render("../public/views/users/barbers/order_info", {
              user: req.user,
              moment: moment,
              barber: req.params.barber,
              service:service,
              date: req.params.time,
              time:time,
              actions:actions,
              selectedTime: req.query.time,
              error: req.flash('error'),
              popupmessage: req.flash('popupmessage')
            });
          })

          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        }else{
          res.render("../public/views/users/barbers/booking", {
            user: req.user,
            moment: moment,
            barber: req.params.barber,
            time:time,
            error: req.flash('error'),
            popupmessage: req.flash('popupmessage')
          });
        }
      })
    })
  }
})

function getActions(type, user_id, actions_callback){
  Action.find({type:type, active:true}, function(err, actions){
    var result = [];
    async.each(actions,
      function(action, callback){
        Appointment.findOne({confirmation_status:true, user_id:user_id, action:action._id}, function(err, appointment){
          if((!err) && (!appointment)){
            result.push(action);
            callback();
          }else{
            callback();
          }
        })
      },
      function(err){
        console.log("get Actions -> err", err);
        actions_callback(result);
      }
    );
  })
}

module.exports = router;
