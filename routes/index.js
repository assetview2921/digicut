/**
 * Created by abhishekgarg on 03/09/15.
 */
var express = require('express'),
    router = express.Router(),
    formidable = require('formidable'),
    auth = require("../config/auth"),
    subs = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    realm = require('express-http-auth').realm('Private Area'),
    User = require('../models/user'),
    passport = require('passport')//,
    sitemapCtrl = require('./sitemapCtrl');

var checkUser = function(req, res, next) {
    if (req.username == 'filnuadmin' && req.password == 'filnuadmin') {
        next();
    } else {
        res.send(401);
    }
}
//was check function on "signup" and "signup/barber"
var private = [realm, checkUser];
//landing page
router.get('/', function (req, res) {
    // render the page and pass in any flash data if it exists
    res.render("../public/views/index.ejs");
});

router.get('/sitemap.xml', function(req, res) {
  var sitemap = sitemapCtrl.getSitemap();
  if(!sitemap){
  return res.status(500).end();
}else{
  res.header('Content-Type', 'text/xml');
  res.send( sitemap );
}

});
//deactivate
router.get("/deactivate",auth.isLoggedIn, subs.isSubscribed,function (req, res) {
    console.log(req.session);
    res.render("../public/views/users/deactivate", {
        popupmessage: req.flash('popupmessage'),
        'error': req.flash('error'),
        user:req.user
    });
});
//deactivate post handler
router.post('/deactivate',auth.isLoggedIn, subs.isSubscribed,
    passport.authenticate('local-deactivate', {
        successRedirect: '/login',
        failureRedirect: '/dashboard',
        failureFlash: true
    })
);

// route for facebook authentication and login
router.get('/facebook/login', passport.authenticate('facebook', {scope: 'email'}));

// handle the callback after facebook has authenticated the user
router.get('/facebook/callback',
    passport.authenticate('facebook', {
        successRedirect: '/signup',
        failureRedirect: '/'
    }));
// =====================================
// LOGIN ===============================
// =====================================
// show the login form
router.get("/login", function (req, res) {
    res.locals.title="Login";
    if (req.session.usernameLogin) {
      var username = req.session.usernameLogin;
      delete req.session['usernameLogin']
    }
    res.render("../public/views/users/login", {
        username:username,
        show_verfication: req.flash('show_verfication'),
        popupmessage: req.flash("popupmessage"),
        'error': req.flash('error')
    });
});

router.get("/signup/barber", function (req, res) {
    res.render("../public/views/users/signup_barber", {
        popupmessage: req.flash('popupmessage'),
        'error': req.flash('error')
    });
});
router.get("/signup/main", function (req, res) {
    res.locals.title="Filnu";
    res.render("../public/views/users/sugnup_main", {
        popupmessage: req.flash('popupmessage'),
        'error': req.flash('error')
    });
});

router.get("/signup", function (req, res) {
    res.render("../public/views/users/signup_customer", {
        popupmessage: req.flash('popupmessage'),
        'show_verfication':req.flash('show_verfication'),
        'error': req.flash('error')
    });
});

router.post('/login',
    passport.authenticate('local-login', {
      failureRedirect: '/login',
      failureFlash: true}),
    function(req, res) {
      // successful auth, user is set at req.user.  redirect as necessary.
      if (req.session.barber) {
        var barber = req.session.barber;
        delete req.session['barber']
        return res.redirect('/barber/'+barber);
      }else{
        return res.redirect('/dashboard');
      }
    }
);


router.post("/tmplogin", passport.authenticate('local-tmp-login'), function (req, res) {
    res.send(req.user);
})

// process the login form
// router.post('/login', do all our passport stuff here);


router.post('/signup', getReqBody,passport.authenticate('local-signup', {
  //successRedirect: '/login', // redirect to the secure profile section
  failureRedirect: '/signup', // redirect back to the signup page if there is an error
  failureFlash: true // allow flash messages
}), function(req, res) {
  console.log('req.user', req.user);
  return res.send(200, 'QQQ');
});

router.post('/signup/barber', getReqBody, passport.authenticate('local-signup', {
    //successRedirect: '/login', // redirect to the secure profile section
    failureRedirect: '/signup/barber', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
}), function(req, res) {
  console.log('req.user', req.user);
  return res.send(200, 'QQQ');
  //var user = req.user;
  //var account = req.account;
  //
  //// Associate the Twitter account with the logged-in user.
  //account.userId = user.id;
  //account.save(function(err) {
  //  if (err) { return self.error(err); }
  //  self.redirect('/');
  //});
});

// process the signup form
// router.post('/signup', do all our passport stuff here);


// =====================================
// LOGOUT ==============================
// =====================================
router.get('/logout', function (req, res) {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    req.logout();
    req.flash('error', null);
    req.flash('popupmessage', null);
    res.redirect('/login');
});
// =====================================
// LOGOUT ==============================
// =====================================

router.get("/user/profile", function (req, res) {
    console.log(req.user);
    res.send(req.user)
});
router.post("/user/profile", function (req, res) {
    User.findOne({"local.username": req.user.local.username}, function (err, user) {
        for (i in req.body) {
            if (i == 'email' || i == 'username')
                continue;
            user.local[i] = req.body[i];

        }
        user.save();
    })
})
router.get('/test/paypal', function (req, res) {
    var usr = new User();
    usr.createCC({})
})

router.get("/update/user", function (req, res) {
    User.findOne({'local.username': '3dvirtuallabs'}, function (err, user) {
        if (err)
            console.log("error - " + err);
        user.local.active = 100;
        user.save(function (e) {
            console.log(e);
        });
    })
    res.send(req.user);
})
//==================================================================
// route to test if the user is logged in or not
router.get('/loggedin', function (req, res) {
    res.send(req.isAuthenticated() ? req.user.first : '0');
});
router.get("/terms",function(req,res){
    res.render("../public/views/static/terms");
});
router.get("/privacy",function(req,res){
    res.render("../public/views/static/privacy");
});
router.get("/notifications",function(req,res){
    res.render("../public/views/static/notification");
})

function getReqBody(req, res, next){

  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    req.body = fields;
    req.files = files;
    next();
  })
}

module.exports = router;
