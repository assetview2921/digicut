/**
 * Created by abhishekgarg on 03/09/15.
 */
var express = require('express'),
    router = express.Router(),
    auth = require("../../config/auth"),
    subscriber = require("../../config/subscription"),
    config = require("../../config/config"),
    realm = require('express-http-auth').realm('Admin Area'),
    paypal = require('paypal-rest-sdk');
paypal.configure({
    'mode': config.paypal.mode, //sandbox or live
    'client_id': config.paypal.client_id,
    'client_secret': config.paypal.client_secret
})

var checkUser = function (req, res, next) {
    if (req.username == 'filnucustomadmin' && req.password == 'filnucustomadmin') {
        next();
    } else {
        res.send(401);
    }
}
var private = [realm, checkUser];
//list all billing plans in json format
router.get("/", auth.isAdmin, function (req, res) {
    var list_billing_plan = {
        'page_size': 20,
        'page': 1,
        'total_required': 'yes'
    };
    paypal.billing_plan.list(list_billing_plan, function (err, plansList) {
        res.send({
            error: err,
            result: plansList
        });
    })
})
//get individual billing plan
router.get("/:id", auth.isAdmin, function (req, res) {
    paypal.billing_plan.get(req.params.id, function (err, plansList) {
        res.send({
            error: err,
            result: plansList
        });
    })
})
/***
 * activate billing plan
 * */
router.get("/activate/:which", auth.isAdmin, function (req,res) {
    var billingPlanId = config.paypal.billingPlans[req.params.which],
        billing_plan_update_attributes = [
        {
            "op": "replace",
            "path": "/",
            "value": {
                "state": "ACTIVE"
            }
        }
    ];
    paypal.billing_plan.get(billingPlanId, function (error, billingPlan) {
        if (error) {
            res.send({
                error:true,
                result:error
            });
        } else {
            paypal.billing_plan.update(billingPlanId, billing_plan_update_attributes, function (error, response) {
                if (error) {
                    res.send({
                        error:true,
                        result:error
                    });
                } else {
                    paypal.billing_plan.get(billingPlanId, function (error, billingPlan) {
                        if (error) {
                            res.send({
                                error:true,
                                result:error
                            });
                        } else {
                            res.send({
                                error:true,
                                result:billingPlan
                            });
                        }
                    });
                }
            });
        }
    });
})

/**
 * create billing plan
 * **/
router.get("/create/:type", auth.isAdmin, function (req, res) {
    switch (req.params.type){
      case "customer": {
        var type = "customer";
        break;
      }
      case "customer_2_cuts": {
        var type ="customer_2_cuts";
        break;
      }
      case "customer_changed_card": {
        var type = "customer_changed_card";
        break;
      }
      case "customer_changed_card_2_cuts": {
        var type = "customer_changed_card_2_cuts";
        break;
      }
      case "barber": {
        var type = "barber";
        break;
      }
      case "barber_changed_card": {
        var type = "barber_changed_card";
        break;
      }
      default : {
        return res.send({
          error: true,
          message: "Only customer or barber type is supported"
        })
        break;
      }
    }
    var billingPlanAttributes = {
        "description": "Filnu Plan for "+type,
        "merchant_preferences": {
            "auto_bill_amount": "yes",
            "cancel_url": config.globals.siteurl+"account/payment/canceled",
            "initial_fail_amount_action": "continue",
            "max_fail_attempts": "1",
            "return_url": config.globals.siteurl+"account/payment/processed",
            "setup_fee": {
                "currency": "USD",
                "value": config.globals.fee[type].setup
            }
        },
        "name": "Filnu "+type+" Monthly subscription plan",
        "payment_definitions": [
            {
                "amount": {
                    "currency": "USD",
                    "value": config.globals.fee[type].monthly
                },
                "charge_models": [
                    {
                        "amount": {
                            "currency": "USD",
                            "value": "0"
                        },
                        "type": "SHIPPING"
                    },
                    {
                        "amount": {
                            "currency": "USD",
                            "value": "0"
                        },
                        "type": "TAX"
                    }
                ],
                "cycles": "0",
                "frequency": "MONTH",
                "frequency_interval": "1",
                "name": "Regular 1",
                "type": "REGULAR"
            },
        ],
        "type": "INFINITE"
    };
console.log('billingPlanAttributes',billingPlanAttributes)
    paypal.billing_plan.create(billingPlanAttributes, function (error, billingPlan) {
        if (error) {
            res.send({
                error:true,
                result:error
            });
        } else {
            res.send({
                error:true,
                result:billingPlan
            });
        }
    });
})

module.exports = router;
