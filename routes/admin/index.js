var express = require('express'),
    router = express.Router(),
    auth = require("../../config/auth"),
    subscriber = require("../../config/subscription"),
    config = require("../../config/config"),
    User = require('../../models/user'),
    Appointments = require('../../models/appointments'),
    Action = require('../../models/action'),
    async = require('async'),
    moment = require('moment'),
    formidable = require('formidable'),
    request = require('request'),
    mongoose = require('mongoose'),
    ObjectID = mongoose.Types.ObjectId,
    realm = require('express-http-auth').realm('Admin Area');

var checkUser = function(req, res, next) {
    if (req.username == 'filnucustomadmin' && req.password == 'filnucustomadmin') {
        next();
    } else {
        res.send(401);
    }
}
var private = [realm, checkUser];


router.get("/", auth.isLoggedIn, auth.isAdmin, getDashboardData)
router.get("/settings", auth.isLoggedIn, auth.isAdmin, getSettingsData)
router.get("/appointments", auth.isLoggedIn, auth.isAdmin, getAppointments)
router.get("/campaigns", auth.isLoggedIn, auth.isAdmin, getActions)
router.get("/:type", auth.isLoggedIn, auth.isAdmin, getDashboardData)
router.post("/edit", auth.isLoggedIn, auth.isAdmin, getReqBody, editUser)
router.post("/changePassword", auth.isLoggedIn, auth.isAdmin, getReqBody, changePassword)
router.post("/changeUsername", auth.isLoggedIn, auth.isAdmin, getReqBody, changeUsername)
router.post("/campaigns", auth.isLoggedIn, auth.isAdmin, getReqBody, updateAction)

function getDashboardData(req, res){
    res.locals.title='Admin dashboard';
    req.flash('popupmessage', '');
    req.flash('error', false);
    var type = req.params.type||"all";
    async.parallel({
        clientsCount: function(callback){
            getUsersCountByType(0,callback)
        },
        barbersCount: function(callback){
            getUsersCountByType(1,callback)
        },
        users: function(callback){
            getUsers(type, callback)
        }
    }, function(err, results){
        console.log('ERR, RESULTS', err, results)
        res.render("../public/views/users/admin/index", {
            users:results.clientsCount+results.barbersCount,
            usr:results.users,
            active:type,
            clients:results.clientsCount,
            barbers:results.barbersCount,
            moment:moment,
            popupmessage: req.flash('popupmessage')
        });
        return;
    })
}
function getAppointments(req, res){
    res.locals.title='Admin dashboard';
    req.flash('popupmessage', '');
    req.flash('error', false);
    var type = req.params.type||"all";
    async.parallel({
        clientsCount: function(callback){
            getUsersCountByType(0,callback)
        },
        barbersCount: function(callback){
            getUsersCountByType(1,callback)
        },
        appointments:function(callback){
            Appointments.find({}, function(err, appointments){
                callback(err, appointments)
            }).populate('user_id').populate("barber_id");
        }
    }, function(err, results){
        console.log('ERR, RESULTS', err, results)
        res.render("../public/views/users/admin/appointments", {
            users:results.clientsCount+results.barbersCount,
            appointments:results.appointments,
            active:'appointments',
            clients:results.clientsCount,
            barbers:results.barbersCount,
            moment:moment,
            popupmessage: req.flash('popupmessage')
        });
        return;
    })

}
function editUser(req, res, next){
    //var body = req.body.user;
    async.parallel({
        address: function(callback){
            request("https://maps.googleapis.com/maps/api/geocode/json?address=" + req.body.address, function (err, response, rBody) {
                var responseBody = JSON.parse(rBody);
                var newAddress = {};
                try {
                    var results = responseBody.results;
                    newAddress.details = req.body.address;
                    for (addr in results[0].address_components) {
                        var address = results[0].address_components[addr];
                        //console.log(results[0].address_components[addr],addr);
                        console.log(address.types);
                        if (address.types.indexOf('postal_code') >= 0) {
                            newAddress.zipcode = (address.long_name)
                        }
                        if (address.types.indexOf('locality') >= 0) {
                            newAddress.city = (address.long_name)
                        }
                        if (address.types.indexOf('administrative_area_level_1') >= 0) {
                            newAddress.state = (address.long_name)
                        }
                        if (address.types.indexOf('country') >= 0) {
                            newAddress.country = (address.long_name)
                        }
                    }
                    if (results.length > 0) {
                        var lat = results[0].geometry.location.lat;
                        var lng = results[0].geometry.location.lng;
                        newAddress.geo = [Number(lat), Number(lng)];
                    }
                } catch (exp) {
                    console.log(exp);
                }
                callback(err, newAddress);
            })
        },
        email: function(callback){
            User.count({_id:{$ne: req.body.id}, email:req.body.email})
                .exec(function(err, count){
                    if(!err && count==0){
                        callback(null, true)
                    }else{
                        callback(true)
                    }
                })
        },
        phone: function(callback){
            User.count({_id:{$ne: req.body.id}, phone:req.body.phone})
                .exec(function(err, count){
                    if(!err && count==0){
                        callback(null, true)
                    }else{
                        callback(true)
                    }
                })
        }
    }, function(err, results){
        if(results.address.geo && results.email && results.phone){
            User.update({_id: req.body.id},
                {$set:{address:results.address, phone: req.body.phone, email: req.body.email}})
                .exec(function(err, result){
                    console.log('err, result >>>', err, result);
                    if(!err && result){
                        res.send(200);
                        return;
                    } else {
                        res.send(400, "Cann't update user");
                    }
                })
        }else{
            if(!results.email){
                return  res.send(400, req.body.email +" is already taken");
            } else if(!results.phone){
                return  res.send(400, req.body.phone +" is already taken");
            } else if(!results.address.geo){
                return res.send(400, "Cann't find address: \""+results.address.details+"\"");
            } else {
                return res.send(400, "Cann't update user !");
            }

        }
    })
}
function getSettingsData(req, res){
    res.locals.title='Admin settings';
    req.flash('popupmessage', '');
    req.flash('error', false);
    async.parallel({
        clientsCount: function(callback){
            getUsersCountByType(0,callback)
        },
        barbersCount: function(callback){
            getUsersCountByType(1,callback)
        }
    }, function(err, results){
        console.log('ERR, RESULTS', err, results)
        res.render("../public/views/users/admin/settings", {
            users:results.clientsCount+results.barbersCount,
            clients:results.clientsCount,
            barbers:results.barbersCount,
            user:req.user,
            moment:moment,
            popupmessage: req.flash('popupmessage')
        });
        return;
    })
}
function changePassword(req, res, next){
    var ID = new ObjectID(req.user._id),
        ID2 = new ObjectID(req.body.id);
    if(!(ID.equals(ID2))) {
        return res.send(400, "Cann't update password");
    } else {
        if (!req.user.validPassword(req.body.old)) {
            return res.send(400, "Invalid old password")
        } else {
            if(req.body.new !== req.body.confirm){
                return res.send(400, "New password and Confirm password doesn't match")
            }
            else{
                var usr = new User();
                var password = usr.generateHash(req.body.new);
                User.update({_id:req.user._id}, {$set:{password:password}})
                    .exec(function(err, user){
                        if(!err && user){
                            return res.send(200)
                        }else{
                            return res.send(400, "Cann't update password")
                        }
                    })
            }


        }


    }
}
function changeUsername(req, res, next){
    var ID = new ObjectID(req.user._id),
        ID2 = new ObjectID(req.body.id);
    if(!(ID.equals(ID2))) {
        return res.send(400, "Cann't update username");
    } else {
        if(req.body.username == '' || req.body.username.length<6){
            return res.send(400, "Incorrect username");
        }else{
            User.count({username:req.body.username, _id:{$ne:req.body.id}})
                .exec(function(err, count){
                    if(!err && count == 0){
                        User.update({_id:req.body.id}, {$set:{username:req.body.username}})
                            .exec(function(err, user){
                                if(!err && user){
                                    return res.send(200)
                                }else{
                                    return res.send(400, "Cann't update username")
                                }
                            })
                    }else{
                        return res.send(400, "Username \""+req.body.username+"\" is already taken");
                    }
                })
        }

    }
}
function getActions(req, res, next){
  res.locals.title='Admin dashboard';
  req.flash('popupmessage', '');
  req.flash('error', false);
  var type = req.params.type||"all";
  async.parallel({
    clientsCount: function(callback){
      getUsersCountByType(0,callback)
    },
    barbersCount: function(callback){
      getUsersCountByType(1,callback)
    },
    appointments:function(callback){
      Appointments.find({}, function(err, appointments){
        callback(err, appointments)
      }).populate('user_id').populate("barber_id");
    },
    campaigns:function(callback){
      Action.find({}, function(err, actions){
        callback(err, actions)
      });
    }
  }, function(err, results){
    console.log('ERR, RESULTS', err, results)
    res.render("../public/views/users/admin/actions", {
      users:results.clientsCount+results.barbersCount,
      appointments:results.appointments,
      active:'appointments',
      clients:results.clientsCount,
      barbers:results.barbersCount,
      campaigns:results.campaigns,
      moment:moment,
      popupmessage: req.flash('popupmessage')
    });
    return;
  })
}
function updateAction(req, res, next){
  Action.findOne({_id:req.body._id}, function(err, action){
    if(action){
      action.active = !action.active;
      action.save(function(err, result){
        if(!err && result){
          console.log(err, result);
          return res.send(200, result.active);
        }else{
         return res.send(400)
        }
      })
    }else{
      return res.send(400);
    }

  })
}
function getUsersCountByType(type,callback){
    User.count({type:type, admin:{$ne: true}})
        .exec(function(err, count){
            callback(err, count);
        });
}
function getUsers(type, callback){
    var query = {admin:{$ne: true}};
    if(type == 'clients') query.type = 0;
    if(type == 'barbers') query.type = 1;
    User.find(query)
        .exec(function(err, users){
            callback(err, users)
        });
}
function getReqBody(req, res, next){
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        req.body = fields;
        next();
    })
}
module.exports = router;
