var express = require('express'),
  router = express.Router(),
  User = require('../models/user'),
  Availability = require("../models/availability"),
  Appointment = require("../models/appointments"),
  Subscription = require("../models/subscriptions"),
  BarberCutPic = require("../models/barber_cutpics"),
  Reviews = require("../models/reviews"),
  auth = require("../config/auth"),
  subscriber = require("../config/subscription"),
  expressmailer = require('../config/expressmailer'),
  config = require("../config/config"),
  async = require("async"),
  _ = require('lodash'),
  sm = require('sitemap'),
  fs = require('fs'),
  path = require('path'),
  request = require('request'),
  days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'],
  all_url,
  urls,
  sitemap;


function createSitemap(cb){
  getAllBarbersUrls(function(results){
    sitemap = sm.createSitemap({
      hostname: config.globals.siteurl,
      cacheTime: 600000,  //600 sec (10 min) cache purge period
      urls: results
    });
    cb();
  })
}
function setSitemap(){
  createSitemap(function(){
    fs.writeFileSync(path.resolve(__dirname, "../sitemap.xml"), sitemap.toString());
  });
}

function getSitemap(){
  return sitemap.toXML();
}

function getAllBarbersUrls(mainCB){
  all_url = [];
  all_url.push(
    { url: '/', links:[{url: '/'}]  },
    { url: '/login ', links:[{url: '/'}]  },
    { url: '/signup', links:[{url: '/'}]  },
    { url: '/signup/barber', links:[{url: '/'}]  }
  );
  User.find({type:1},{username:1})
    .exec(function(err, barbers){
      if(!err && barbers){
        async.map(_.pluck(barbers, 'username'), function(username, callback){
          all_url.push({ url: '/barber/'+username, links:[{url: '/'+username}]  });
          callback(null, null);
        }, function(err, results){
          mainCB(all_url)
        });
      }else{
        mainCB(all_url);
      }
    })
}

function addToSitemap(username){
  setSitemap()
}

exports.addToSitemap = addToSitemap
exports.setSitemap = setSitemap
exports.getSitemap = getSitemap
