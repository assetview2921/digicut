var express = require('express'),
    router = express.Router(),
    User = require('../models/user'),
    Availability = require("../models/availability"),
    Appointment = require("../models/appointments"),
    Subscription = require("../models/subscriptions"),
    BarberCutPic = require("../models/barber_cutpics"),
    Reviews = require("../models/reviews"),
    auth = require("../config/auth"),
    subscriber = require("../config/subscription"),
    expressmailer = require('../config/expressmailer'),
    config = require("../config/config"),
    async = require("async"),
    _ = require('lodash'),
    request = require('request'),
    days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
router.get("/", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    res.redirect("/dashboard");
})

router.get("/search", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.isCustomer, function (req, res) {
    res.locals.title='Find A Barber';
    if (typeof req.query.location !== 'undefined') {
        res.redirect("/barber/search/" + req.query.location);
    } else if(typeof req.query.username !== 'undefined'){
        res.redirect("/barber/search/username/" + req.query.username);
    } else {
        res.render("../public/views/users/search", {
            user: req.user,
            error: req.flash('error'),
            popupmessage: req.flash('popupmessage')
        });
    }

})
router.get("/alerts", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {


    Appointment.find({"barber_id": req.user._id, status: false}).populate('user_id').exec(function (err, appointments) {
        if (err || appointments == null) {
            req.flash('popupmessage', 'No appointments found');
            res.redirect('/dashboard');
            return;
        }
        res.locals.title = "Alerts";
        res.render("../public/views/users/barbers/alerts", {
            user: req.user,
            appointments: appointments,
            popupmessage: req.flash('popupmessage'),
            error: req.flash('error')
        });
    });

})
router.get("/setup/:type", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    switch (req.params.type) {
        case 'working_hours':
            Availability.count({user_id: req.user._id}, function (err, count) {
                res.send({error: err, count: count});
            });
            break;
    }
})
router.get("/calendar", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    res.locals.title = "Calendar";
    res.render("../public/views/users/barbers/calendar", {
        user: req.user,
        error: req.flash('error'),
        popupmessage: req.flash('popupmessage')
    });
})

router.get("/gallery", auth.isLoggedIn, auth.isClient, subscriber.isSubscribed, subscriber.isBarber, function (req, res) {
    res.render("../public/views/users/barbers/gallery", {
        user: req.user,
        error: req.flash('error'),
        popupmessage: req.flash('popupmessage')
    });
})
//get request working-hours
router.get("/working-hours", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    res.locals.title='Working Hours'
    Availability.findOne({user_id: req.user._id}, function (err, docs) {
        res.render("../public/views/users/barbers/working-hours", {
            availability: docs,
            error: req.flash('error'),
            popupmessage: req.flash('popupmessage')
        });
    })
})
router.get("/:username", function (req, res) {
    res.locals.title='Barber Profile';
    if (typeof req.params.username !== 'undefined') {

        User.findOne({
            username: req.params.username, emailVerified:1
        }).exec(function (err, barber) {
            if (err == null && barber) {
                getBarberProfile(req, res, barber)
                //BarberCutPics = require("../models/barber_cutpics");
                //BarberCutPics.find({
                //    user_id: barber._id
                //}, function (err, barbercutpics) {
                //    if (!err) {
                //        Appointment.count({barber_id: barber._id, status: true}, function (err, count) {
                //            Reviews.find({barber_id: barber._id}, function (cerr, reviews) {
                //                res.render("../public/views/users/barbers/profile", {
                //                    popupmessage: req.flash('popupmessage'),
                //                    barber: barber,
                //                    cutpics: barbercutpics,
                //                    appointments: count,
                //                    reviews: reviews,
                //                    countReviews:reviews.length
                //                });
                //            })
                //
                //        })
                //
                //    }
                //})
            }else{
              //tell to user that this barber doesn't exist or redirect to login/search???
              if (req.isAuthenticated()) {
               return res.redirect('/barber/search');
              }else{
                return res.redirect('/barber/search');
              }
            }
        })
    }
})
//post request working-hours usually when form is submitted via HTML or ajax
router.post("/working-hours", auth.isLoggedIn, auth.isClient, subscriber.isBarber, function (req, res) {
    Availability.findOne({user_id: req.user._id}, function (err, docs) {

        if (docs == null) {
            availability = new Availability();
            availability.user_id = req.user._id;
            availability.availability = req.body;
            availability.save(function (error) {
                if (error == null) {
                    req.flash('error', false);
                    req.flash('popupmessage', 'Availability has been updated successfully');
                    User.findOne({_id: req.user._id}, function (err1, doc) {
                        doc.barber.working_hours = true;
                        doc.save(function (error) {
                            console.log(error);
                            res.redirect("/dashboard");
                            return;
                        })

                    })
                } else {
                    req.flash('popupmessage', 'Unable to update availability');
                    req.flash('error', true);
                    User.findOne({_id: req.user._id}, function (err1, doc) {
                        doc.barber.working_hours = true;
                        doc.save(function (error) {
                            console.log(error);
                            res.redirect("/dashboard");
                        })

                    })


                }

            })
        } else {
            docs.availability = req.body;
            docs.save(function (error) {
                if (error == null) {
                    req.flash('error', false);
                    req.flash('popupmessage', 'Availability has been updated successfully');
                } else {
                    req.flash('error', true);
                    req.flash('popupmessage', 'Unable to update availability');
                }
                User.findOne({_id: req.user._id}, function (err1, doc) {
                    doc.barber.working_hours = true;
                    doc.save(function (error) {
                        console.log(error);
                        res.redirect("/dashboard");
                    })

                })
            })
        }
    })
})

router.get("/search/username/:data", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, function (req, res) {
    User.findOne({username:req.params.data, type:1, emailVerified : 1,
    active : 1}, function(err, barber){
        if(!err && barber){
            getBarberProfile(req, res, barber);
        }else{
            User.find({username: new RegExp(req.params.data, 'i'), type: 1, "barber.working_hours": true, emailVerified : 1,
            active : 1}, function (err, doc) {
                var lat = req.user.address.geo[0],
                    lng = req.user.address.geo[1];
                returnSearchedBarbers(req, res, doc, null, lat, lng, null);
            });
        };
    });
})

router.get("/search/:location", auth.isLoggedIn, auth.isClient, subscriber.isCustomer, function (req, res) {
    if (typeof req.params.location != 'undefined') {
        request("https://maps.googleapis.com/maps/api/geocode/json?address=" + req.params.location, function (err, response, body) {
            responseBody = JSON.parse(body);
            results = responseBody.results;
            if (results.length > 0) {
                lat = results[0].geometry.location.lat;
                lng = results[0].geometry.location.lng;
                Appointment.count({_id: req.user._id}, function (appCountErr, earlierApptsCount) {
                    console.log(earlierApptsCount);
                });
                //  maxDistance: 0.29039
              async.parallel({
                searchedBarbers:function(callback){
                  User.geoSearch({type: 1, "barber.working_hours": true, emailVerified : 1,
                    active : 1}, {
                      near: [parseFloat(lat), parseFloat(lng)],
                      maxDistance: 0.07246
                    }, function (err, doc, stats) {
                    callback(null, {doc:doc, stats:stats});
                  });
                },
                allBarbers:function(callback){
                  User.find({type: 1, "barber.working_hours": true, emailVerified : 1,
                    active : 1},function(err, barbers){
                    callback(null, {barbers:barbers});
                  });
                }
              },function(err, results) {
                returnSearchedBarbers(req, res, results.searchedBarbers.doc, results.searchedBarbers.stats, lat, lng, results.allBarbers.barbers);
              });
            }else{
                res.locals.title='Invite Now'
                viewtorender = "/users/invitenow";
                res.redirect(viewtorender);
                return;
            };
        });
    };
})
router.get("/myappointments", auth.isClient, function (req, res) {
    Subscription.find({user_id: ""});
})

module.exports = router;

function getActive(doc, callback){
    var newDoc = [];
    doc.forEach(function(barber){
      if( barber.barber.working_hours == true)
          newDoc.push(barber);
    });
    return callback(null, newDoc);
}

function getBarberProfile(req, res, barber){
    BarberCutPic.find({
        user_id: barber._id
    }, function (err, barbercutpics) {
        if (!err) {
            Appointment.count({barber_id: barber._id, status: true}, function (err, count) {
                Reviews.find({barber_id: barber._id}, function (cerr, reviews) {
                  //if (req.isAuthenticated()) {
                  //  if(req.user.username.toString()===barber.username.toString){
                      var link = config.globals.siteurl+'barber/'+barber.username;
                  //  }
                  //}
              async.parallel({
                    main_pic: function(callback){
                      BarberCutPic.find({user_id:barber._id, default:true}, function(err, cutPic){
                        if(!err && cutPic){
                          callback(err, cutPic[0])
                        }else{
                          callback(err, null)
                        }

                      }).sort({created:-1})
                    },
                    cut_pic: function(callback){
                      BarberCutPic.findOne({user_id:barber._id}, function(err, cutPic){
                        callback(err, cutPic)
                      })
                    }
                  },
                  function(err, results) {
                    var barberCutPic = (results.main_pic)?results.main_pic:results.cut_pic;
                    if(barberCutPic){
                      barberCutPic = "https://digicut-public.s3.amazonaws.com/"+barberCutPic.pic;
                    }else{
                      if(barber.profile_pic){
                        barberCutPic = "https://digicut-public.s3.amazonaws.com/"+barber.profile_pic;
                      }else{
                        barberCutPic = "https://s3-us-west-2.amazonaws.com/digicut-public/logo.png";
                      }
                    }

                    return res.render("../public/views/users/barbers/profile", {
                      barberCutPic: barberCutPic,
                      description:"Filnu is the easiest way to find a barber",
                      keywords: "barber, barbers, barbershop, barbershops, haircut, black barber, black barbers, black barbershop,"+barber.username+","+barber.address.details,
                      link:(link)?link:false,
                      popupmessage: req.flash('popupmessage'),
                      barber: barber,
                      cutpics: barbercutpics,
                      appointments: count,
                      reviews: reviews,
                      countReviews:reviews.length
                    });
                  })

                });
            });
        }else{
          return res.redirect('/barber/search');
        };
    });
}

function returnSearchedBarbers(req, res, doc, stats, lat, lng, allBarbers){
    getActive(doc,function(err, newDoc){
        console.log(err,doc,stats);
        var count = newDoc.length;
        req.session.count = count;
        if (count == 0) {
            if(typeof req.params.location != 'undefined'){
                req.session.count = count;
                if (count == 0) {
                    res.locals.title='Invite Now'
                    viewtorender = "/users/invitenow";
                }
                res.redirect(viewtorender);
                return;
                }else{
                    res.locals.title="Search a Barber";
                    req.flash('popupmessage', 'There are no barbers with similar username that you enter');
                    //viewtorender = "../public/views/users/search";
                    res.redirect("/barber/search")
                    req.flash('popupmessage', '');
                    return;
            }

        }
        //if (req.user.active == 0) {
        //    res.locals.title='Join Now'
        //    viewtorender = "/users/joinnow";
        //
        //    res.redirect(viewtorender);
        //} else {
            var docs = [];
            async.eachSeries(newDoc, function(item, callback){
                async.parallel({
                        main_pic: function(callback){
                            BarberCutPic.find({user_id:item._id, default:true}, function(err, cutPic){
                                if(!err && cutPic){
                                    callback(err, cutPic[0])
                                }else{
                                    callback(err, null)
                                }

                            }).sort({created:-1})
                        },
                        cut_pic: function(callback){
                            BarberCutPic.findOne({user_id:item._id}, function(err, cutPic){
                                callback(err, cutPic)
                            })
                        }
                    },
                    function(err, results) {
                        Reviews.find({barber_id: item._id}, function (err, reviews) {
                            item._doc.reviews = reviews;
                            item._doc.countReviews = reviews.length
                            item._doc.barberCutPic = (results.main_pic)?results.main_pic:results.cut_pic;
                            docs.push(item._doc);
                            callback(null);
                        })
                    })

            }, function done(err){
                res.locals.title='Select Barber'
                viewtorender = "../public/views/users/search_results";
                res.render(viewtorender, {
                    lat: lat,
                    lng: lng,
                    error: err,
                    maps: docs,
                    stats: stats,
                    allBarbers:allBarbers,
                    popupmessage: req.flash('popupmessage')
                })
            })


        //}
        return;
    })
}
